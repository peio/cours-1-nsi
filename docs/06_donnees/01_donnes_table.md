---
author: Nicolas Revéret, Pierre Marquestaut
title: Les données structurées
---
# Les données structurées

!!! abstract "Données en table"
    Une **table** est une collection de données organisées sous forme d'un tableau où les colonnes correspondent à des **catégories d'information**, et les lignes à des **enregistrements**. 

    Un **champ** est la valeur d’une catégorie pour une enregistrement donné.

???+ question "Exemple"
    |nom   |âge|ville   |passion      |
    |------|---|--------|-------------|
    |Jean  |26 |Paris   |VTT          |
    |Marion|28 |Lyon    |badminton    |
    |Paul  |33 |Toulouse|Tai-chi-chuan|
    |Elorri|15 |Cambo   |photo        |
    |Sohra |25 |Agen    |vélo         |

    Combien cette table contient-elle d'enregistrements ? de catégories ? de champs ?

??? soluce "Solution"

    La table contient $5$ enregistrements décrits par $4$ catégories.

    Cela représente $20$ champs au total.


!!! abstract "Descripteurs"

    Les **descripteur** sont les noms de chaque catégories d'une table.

???+ question "Exemple"
    Quels sont les descripteurs de la table précédente ?

??? soluce "Solution"
    Les descripteurs de l'exemple précédents sont : *nom, âge, ville* et *passion*.


## Structure de données

Comment stocker les données dans un programme informatique?

### Les tuples

!!! abstract "p-uplet et enregistrement"
    Il est possible de représenter un enregistrement au travers d'un p-uplet.

    Dans ce cas, chaque catégorie correspond à une position à l'intérieur de p-uplet.

!!! example "Exemple"
    Le premier enregistrement de la table précédente peut se représenter ainsi :
    
    ```python
    enregistrement_1 = ("Jean", 26, "Paris","VTT")
    ```

    Dans ce cas, la valeur "Jean" correspond à la catégorie *nom* car situé à  la première position.



!!! abstract "p-uplet et table"
    Une table peut donc être repésentée par un tableau de p-uplets, chaque élément du tableau correspondant à un enregistrement.

    Il est important que l'ordre des catégories soit respecté dans chaque p-uplet.

!!! example "Exemple"
    La table peut se représenter ainsi :
    
    ```python
    personnes = [("Jean", 26, "Paris","VTT"),
                 ("Marion", 28, "Lyon","badminton"),
                 ("Paul",33,"Toulouse","Tai-chi-chuan"),
                 ("Elorri",15,"Cambo","photo"),        
                 ("Sohra",25,"Agen","vélo")]
    ```

    Il est très important que les catégories soient dans le même ordre pour chaque p-uplet.

???+ question "Tableau de p-uplets"

    On considère la table décrite ci-dessus.

    Cochez la ou les bonnes réponses.

    === "Propositions"
        
        - [ ] On peut accéder à la ville de la première personne en faisant `#!py personnes[0][3]`
        - [ ] `#!py personnes[4]` n'existe pas car il n'y a que quatre descripteurs
        - [ ] `#!py personnes[3][2]` désigne la ville d'Elorri
        - [ ] `#!py len(personnes[0])` permet de récupérer le nombre de personnes

    === "Solution"
        
        - :x: `#!py personnes[0][3]` permet de récupérer le loisir de Jean.
        - :x: `#!py personnes[4]` renvoie toutes les informations décrivant Sohra.
        - :white_check_mark:  personnes[3][2]` désigne la ville d'Elorri
        - :x: `#!py len(personnes[0])` permet de récupérer le nombre de descripteurs

!!! tips "Tableau de tableaux"
    Dans le cas où les valeurs sont toutes de même type, il est également possible d'utiliser un tableau de tableaux.

### Les dictionnaires

!!! abstract "dictionnaires et enregistrement"
    Il est possible de représenter un enregistrement au travers d'un dictionnaire.

    Dans ce cas, les descripteurs constituent les clés du dictionnaire.

!!! example "Exemple"
    Le premier enregistrement de la table précédente peut se représenter ainsi :
    
    ```python
    enregistrement_1 = {"nom" : "Jean", "age" : 26, "ville" : "Paris", "loisir" : "VTT"}
    ```




!!! abstract "p-uplet et table"
    Une table peut donc être repésentée par un tableau de dictionnaires, chaque élément du tableau correspondant à un enregistrement.

 

!!! example "Exemple"
    La table peut se représenter ainsi :
    
    ```python
    personnes = [{"nom" : "Jean", "age" : 26, "ville" : "Paris", "loisir" : "VTT"},
            {"nom" : "Marion", "age" : 28, "ville" : "Lyon", "loisir" : "badminton"}]
    ```
   Il est important que tous les enregistrements possèdent les mêmes clés.


???+ question "Tableau de dictionnaires"

    On considère la table décrite ci-dessus.

    === "Propositions"
        
        - [ ] On peut accéder à la ville rouge de la première personne en faisant `#!py personnes[0][ville]`
        - [ ] `#!py personnes[4][2]` renvoie l'âge de Mélanie
        - [ ] `#!py personnes["Jean"]["loisir"]` désigne le loisir de Jean.
        - [ ] `#!py len(personnes)` désigne le nombre de personnes dans la table.

    === "Solution"
        
        - :x: `#!py "ville"` est une chaîne de caractères, les guillemets sont indispensables
        - :x: Il faut faire `#!py personnes[1]["âge"]`
        - :x: Les caractéristiques de Jean sont à l'indice `#!py 5` du tableau
        - :white_check_mark: `#!py len(personnes)` désigne le nombre de personnes dans la table.