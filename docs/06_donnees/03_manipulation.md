---
author: Nicolas Revéret
title: Manipulations de données
---
# 🏁 Manipulations des données

# Extraire des données

!!! abstract "Extraire des données"
    La construction par compréhension permet de sélectionner d'une structure de données :

    * une ou plusieurs catégories,
    * les enregistrements répondant à une condition donnée.

!!! example "extraction"
    Si on reprend les données suivantes :
    ```python
    personnes = [("Jean", 26, "Paris","VTT"),
                 ("Marion", 28, "Lyon","badminton"),
                 ("Paul",33,"Toulouse","Tai-chi-chuan"),
                 ("Elorri",15,"Cambo","photo"),        
                 ("Sohra",25,"Agen","vélo")]
    ```
    Le code suivant permet d'extraire les loisirs
    ```python
    loisirs = [une_personne[3] for une_personne in personnes]
    ```

    Le code suivant en revanche permet de sélectionner uniquement les enregistrements des personnes âgés de moins de 18 ans.
    ```python
    personnes_mineurs = [une_personne for une_personne in personnes if une_personne[1] < 18]
    ```

## Exemples


Considérons le tableau de couleurs ci-dessous décrivant différentes couleurs à l'aide de quatre descripteurs :

* leur nom,
* leur composante rouge (de 0 à 255),
* leur composante verte (de 0 à 255),
* leur composante bleue (de 0 à 255).

<table>
    <theader>
        <tr>
            <td style="font-weight:bold">Couleur</td>
            <td style="font-weight:bold">Nom</td>
            <td style="font-weight:bold">Rouge</td>
            <td style="font-weight:bold">Vert</td>
            <td style="font-weight:bold">Bleu</td>
        </tr>
    </theader>
    <tbody>
    <tbody>
        <tr><td style="background-color:rgb(0,0,0)">&nbsp</td><td>Noir</td><td>0</td><td>0</td><td>0</td></tr>
        <tr><td style="background-color:rgb(255,255,255)">&nbsp</td><td>Blanc</td><td>255</td><td>255</td><td>255</td></tr>
        <tr><td style="background-color:rgb(255,0,0)">&nbsp</td><td>Rouge</td><td>255</td><td>0</td><td>0</td></tr>
        <tr><td style="background-color:rgb(0,255,0)">&nbsp</td><td>Vert</td><td>0</td><td>255</td><td>0</td></tr>
        <tr><td style="background-color:rgb(0,0,255)">&nbsp</td><td>Bleu</td><td>0</td><td>0</td><td>255</td></tr>
        <tr><td style="background-color:rgb(255,255,0)">&nbsp</td><td>Jaune</td><td>255</td><td>255</td><td>0</td></tr>
        <tr><td style="background-color:rgb(0,255,255)">&nbsp</td><td>Cyan</td><td>0</td><td>255</td><td>255</td></tr>
        <tr><td style="background-color:rgb(255,0,255)">&nbsp</td><td>Magenta</td><td>255</td><td>0</td><td>255</td></tr>
        <tr><td style="background-color:rgb(192,192,192)">&nbsp</td><td>Argent</td><td>192</td><td>192</td><td>192</td></tr>
        <tr><td style="background-color:rgb(128,128,128)">&nbsp</td><td>Gris</td><td>128</td><td>128</td><td>128</td></tr>
        <tr><td style="background-color:rgb(128,0,0)">&nbsp</td><td>Bordeaux</td><td>128</td><td>0</td><td>0</td></tr>
        <tr><td style="background-color:rgb(128,128,0)">&nbsp</td><td>Olive</td><td>128</td><td>128</td><td>0</td></tr>
        <tr><td style="background-color:rgb(128,0,128)">&nbsp</td><td>Violet</td><td>128</td><td>0</td><td>128</td></tr>
        <tr><td style="background-color:rgb(0,0,128)">&nbsp</td><td>Marine</td><td>0</td><td>0</td><td>128</td></tr>
    </tbody>
</table>

## Tableau de tableaux

Il est possible de représenter ces informations dans une liste Python. Chacun des éléments de cette liste est lui-même une liste décrivant une couleur :

```python
liste_couleurs = [
    ["Noir",       0,   0,   0],  # Couleur 0
    ["Blanc",    255, 255, 255],  # Couleur 1
    ["Rouge",    255,   0,   0],  # Couleur 2
    ["Vert",       0, 255,   0],  # Couleur 3
    ["Bleu",       0,   0, 255],  # Couleur 4
    ["Jaune",    255, 255,   0],  # Couleur 5
    ["Cyan",       0, 255, 255],  # Couleur 6
    ["Magenta",  255,   0, 255],  # Couleur 7
    ["Argent",   192, 192, 192],  # Couleur 8
    ["Gris",     128, 128, 128],  # Couleur 9
    ["Bordeaux", 128,   0,   0],  # Couleur 10
    ["Olive",    128, 128,   0],  # Couleur 11
    ["Violet",   128,   0, 128],  # Couleur 12
    ["Marine",     0,   0, 128]   # Couleur 13
]
```

!!! note "Remarque"
    
    On a ajouté des espaces afin de simplifier la lecture. Elles[^1] ne sont pas indispensables.

[^1]: En typographie, « espace » est un nom commun [féminin](https://fr.wikipedia.org/wiki/Espace_(typographie)#Genre_du_mot).

Les informations décrivant une couleur étant décrites dans des sous-listes, elles sont accessibles en utilisant les indices :

|     Indice      |   `#!py 0`   |    `#!py 1`    |   `#!py 2`    |   `#!py 3`    |
| :-------------: | :----------: | :------------: | :-----------: | :-----------: |
| **Descripteur** | `#!py "nom"` | `#!py "rouge"` | `#!py "vert"` | `#!py "bleu"` |

Il est donc possible de récupérer le nom de la troisième couleur de la liste en faisant `#!py liste_couleurs[2][0]` qui renvoie `#!py "rouge"`.


???+ question "Requêtes (tableau)"

    On travaille toujours avec le tableau de tableaux décrit ci-dessus.

    Compléter le script afin de d'effectuer les requêtes proposées :

    {{ IDE('scripts/couleurs_liste/exo', MAX=10, STD_KEY="bk4fg1") }}


???+ question "Nuances de gris (tableau)"

    On travaille toujours avec le tableau décrit ci-dessus.

    Il est possible de convertir une couleur en noir et blanc en donnant à chacun de ses canaux la même valeur obtenue par la formule suivante :

    $$Y = 0,299\times Rouge + 0,587 \times Vert + 0,114 \times Bleu$$

    Cette valeur $Y$ est appelée *niveau de gris*.

    Par exemple pour le rouge on obtient : $Y = 0,299 \times 255 + 0,587 \times 0 + 0,114 \times 0 \approx 76$.

    Compléter le code ci-dessous permettant d'obtenir la liste contenant le niveau de gris associé à chaque couleur de `liste_couleurs`.

    {{ IDE('scripts/gris/exo', MAX=10) }}



!!! faq "Exercice"
    [Requête dans une liste de listes](https://codex.forge.apps.education.fr/en_travaux/requetes/){ .md-button target="_blank" rel="noopener"}

## Tableau de dictionnaires

Une autre approche est possible : au lieu de représenter les données dans une tableau de tableaux, on utilise une tableau de dictionnaires. Chaque couleur est décrite par un dictionnaire dont les clés sont les noms des descripteurs et les valeurs la valeur associée.

```python
dico_couleurs = [
    {"nom": "Noir",     "rouge":   0, "vert":   0, "bleu":   0},
    {"nom": "Blanc",    "rouge": 255, "vert": 255, "bleu": 255},
    {"nom": "Rouge",    "rouge": 255, "vert":   0, "bleu":   0},
    {"nom": "Vert",     "rouge":   0, "vert": 255, "bleu":   0},
    {"nom": "Bleu",     "rouge":   0, "vert":   0, "bleu": 255},
    {"nom": "Jaune",    "rouge": 255, "vert": 255, "bleu":   0},
    {"nom": "Cyan",     "rouge":   0, "vert": 255, "bleu": 255},
    {"nom": "Magenta",  "rouge": 255, "vert":   0, "bleu": 255},
    {"nom": "Argent",   "rouge": 192, "vert": 192, "bleu": 192},
    {"nom": "Gris",     "rouge": 128, "vert": 128, "bleu": 128},
    {"nom": "Bordeaux", "rouge": 128, "vert":   0, "bleu":   0},
    {"nom": "Olive",    "rouge": 128, "vert": 128, "bleu":   0},
    {"nom": "Violet",   "rouge": 128, "vert":   0, "bleu": 128},
    {"nom": "Marine",   "rouge":   0, "vert":   0, "bleu": 128},
]
```

Au sein de chaque dictionnaire, les informations sont identifiées par les clés. Il est plus facile de récupérer chaque valeur (sans avoir à compter les indices !).

Il est possible de récupérer le nom de la troisième couleur de la liste en faisant `#!py liste_couleurs[2]["nom"]` qui renvoie là encore `#!py "rouge"`.


???+ question "Requêtes (dictionnaires)"

    On travaille toujours avec le tableau de dictionnaires décrite ci-dessus.

    Compléter le script afin de d'effectuer les requêtes proposées :

    {{ IDE('scripts/couleurs_dico/exo', MAX=10, STD_KEY="bk4fg1") }}

???+ question "Nuances de gris (dictionnaires)"

    On travaille toujours avec le tableau de dictionnaires ci-dessus.

    Reprendre l'exercice « Nuances de gris (listes) » avec cette structure de données.
    
    {{ IDE('scripts/gris_dico/exo', MAX=10) }}

!!! faq "Exercice"
    [Requête dans une liste de dictionnaires](https://codex.forge.apps.education.fr/exercices/requetes_dict/){ .md-button target="_blank" rel="noopener"}