---
author: Nicolas Revéret, Pierre Marquestaut
title: Les données dans les fichiers
---

# Les données dans les fichiers

Pour stocker un grand nombre de données, la solution la plus simple est d'utiliser un fichier.

## 📑 Les fichiers `csv` et `json`



!!! abstract "Les types de fichiers"
    Les fichiers correspondants sont souvent proposés aux formats 

    * `csv` pour _**C**omma **S**eparated **V**alues_,
    * `json` pour _**J**ava**S**cript **O**bject **N**otation_.

    Ces deux formats de fichiers permettent de **présenter des données textuelles**. 

!!! example "Exemple"
    Voici par exemple les mêmes informations présentées dans chacun des formats :

    * au format  `csv` (le fichier s'appelle `amis.csv`):

        ```title="📑 Données CSV"
        nom,âge,ville,passion
        Jean,26,Paris,VTT
        Marion,28,Lyon,badminton
        Paul,33,Toulouse,Tai-chi-chuan
        Elorri,15,Cambo,photo        
        Sohra,25,Agen,vélo        
        ```

    * au format `json` (le fichier s'appelle `amis.json`):

        ```json
        { "amis": [
            {"nom": "Jean","âge": 26,"ville": "Paris","passion": "VTT"},
            {"nom": "Marion","âge": 28,"ville": "Lyon","passion": "badminton"},
            {"nom": "Paul","âge": 33,"ville": "Toulouse","passion": "Tai-chi-chuan"},
            {"nom": "Elorri","âge": 15,"ville": "Cambo","passion": "photo"},
            {"nom": "Sohra","âge": 25,"ville": "Agen","passion": "vélo"}
                ]
        }
        ```

Nous travaillerons désormais avec les fichiers `csv`. 

!!! note "Remarque"
    L'exemple précédent permet de remarquer plusieurs choses :

    * un fichier `csv` contient des **données textuelles**,

    * les données sont organisées en lignes,

    * la première ligne regroupe le nom des **descripteurs** (il y en a quatre ici : `#!py nom`, `#!py âge`, `#!py ville` et `#!py passion`),

    * les autres lignes contiennent des **enregistrements** (il y en a deux ici : `#!py Jean,26,Paris,VTT` et `#!py Marion,28,Lyon,badminton`),

    * au sein de chaque ligne, les valeurs sont délimitées par un **séparateur** (ici le caractère `#!py ","`),

    * les données peuvent être de types différents. Ici le `#!py nom`, la `#!py ville` et la `#!py passion` sont des chaînes de caractères, l'`#!py âge` un entier.

!!! danger "Attention"

    La réalité n'est pas aussi simple :

    * il arrive que **la première ligne ne contienne pas les entêtes**. Ils peuvent être listés dans un fichier annexe ou... perdus !
    
    * on trouve parfois **une seconde ligne contenant les types des données** (entier, texte...).
    
    * le séparateur **n'est pas toujours une virgule**. Il est courant que l'on trouve des `#!py ";"` dans les fichiers français car la virgule est utilisée comme séparateur décimal.

???+ question "Premiers contacts"

    On considère les deux fichiers `csv` ci-dessous (on n'en donne que les trois première lignes) :

    * `petanque.csv` ([Localisation des terrains de pétanque dans l'agglomération de Tours](https://www.data.gouv.fr/fr/datasets/terrain-de-petanque-tours-metropole/)) :

    ```title="📑 Données CSV"
    geo_point_2d;nb_equipement;commune;cp
    (47.3392380011,0.7162219998);1;Chambray-lès-Tours;37170
    (47.3300100011,0.6120900019);5;Ballan-Miré;37510
    ```
    
    * `bac.csv` ([Résultats au baccalauréat par académie](https://www.data.gouv.fr/fr/datasets/le-baccalaureat-par-academie/)) :

    ```title="📑 Données CSV"
    session,academie,sexe,diplome_specialite,nombre_d_inscrits,nombre_d_admis_totaux
    INT,TEXT,TEXT,TEXT,INT,INT
    2021,AIX-MARSEILLE,FILLES,BAC PRO AG 21302 GEST MILIEUX NATURELS FAUNE,16,13
    ```

    Cochez la ou les bonnes réponses.

    === "Propositions"
        
        - [ ] Le séparateur du fichier `petanque.csv` est la virgule
        - [ ] Le fichier `petanque.csv` compte quatre descripteurs
        - [ ] Le séparateur du fichier `bac.csv` est la virgule
        - [ ] `INT` est un descripteur du fichier `bac.csv`

    === "Solution"
        
        - :x: Le séparateur du fichier `petanque.csv` est le point-virgule
        - :white_check_mark: Le fichier `petanque.csv` compte bien quatre descripteurs
        - :white_check_mark: Le séparateur du fichier `bac.csv` est bien la virgule
        - :x: `INT` est un type de données

???+ question "Problème !"

    On propose ci-dessous un extrait d'un fichier `csv`. Identifiez les trois problèmes présents :

    ```title="📑 Données CSV"
    nom,prenom,identifiant;mdp,derniere_connexion
    Clark,Sarah,sclark,k012345,20230105,
    Mapple,Marc,marc.mapple,20221231
    ```

    ??? success "Solution"

        1. Le séparateur n'est pas constant : il y a un point-virgule dans la première ligne
        2. Il y a une virgule en trop en fin de deuxième ligne
        3. Il manque un champs sur la troisième ligne

        On pourrait aussi noter le **gros** problème qui consiste à stocker les mots de passe des utilisateurs en clair dans un fichier !

???+ note "Autres formats..."

    Les fichiers `csv` et `json` ne sont pas les seuls formats permettant de conserver des données. 
    
    On peut aussi retenir le format `xml` pour _e**X**tensible **M**arkup **L**anguage_ qui utilise des balises au même titre que le `html` :


    ```xml title="📑 Données XML"
    <?xml version="1.0" encoding="UTF-8"?>
    <amis>
        <personne>
            <nom>Jean</nom>
            <âge>26</âge>
            <ville>Paris</ville>
            <passion>VTT</passion>
        </personne>
        <personne>
            <nom>Marion</nom>
            <âge>28</âge>
            <ville>Lyon</ville>
            <passion>badminton</passion>
        </personne>
    </amis>
    ```


## La fonction `#!py open`

Ouvrir un fichier avec Python est simple. On utilise la fonction `open` présente dans la [bibliothèque native](https://docs.python.org/fr/3/library/functions.html#open).

Cette fonction prend plusieurs paramètres. Retenons les trois suivants (déroulez pour avoir des détails):

??? info "`#!py file` : le nom du fichier"
    
    Le nom du fichier (avec son extension donc, au format `#!py str`).
    
    Attention, par « nom » on désigne **l'adresse** du fichier. Cette adresse peut-être :
    
    * *absolue*, depuis la racine du disque, `C:/home/nico/Documents/donnees_en_table/fables.txt` par exemple ;
    
    * ou *relative*, depuis le fichier Python qui ouvre le fichier, `fables.txt` par exemple si les deux fichiers sont dans le même dossier.

    On conseille **très fortement** d'utiliser des adresses relatives.

??? info "`#!py mode` : le mode d'ouverture"
    
    Le mode d'ouverture du fichier (au format `#!py str`).
    
    On peut retenir les modes suivants :

    | Caractère  | Signification                                                  |
    | :--------- | :------------------------------------------------------------- |
    | `#!py 'r'` | ouvre en lecture, en mode texte                                |
    | `#!py 'w'` | ouvre en écriture, en effaçant le contenu du fichier           |
    | `#!py 'a'` | ouvre en écriture, en ajoutant les données à la fin du fichier |
    | `#!py 'b'` | mode binaire                                                   |

    On notera que le mode `#!py 'w'` efface directement le contenu du fichier, il n'y a pas de message d'avertissement !

??? info "`#!py encoding` : l'encodage utilisé"
    
    Le type d'encodage (au format `#!py str`).
    
    L'encodage d'un fichier correspond à la façon dont le programme qui l'ouvre doit interpréter les données qu'il contient.
    
    Un fichier est stocké en machine sous forme d'une succession de bits. Dans la table ASCII initiale, 7 bits représentent un caractère. Dans la table ASCII étendue, il faut 8 bits pour représenter un caractère. L'encodage `#!py 'utf-8'` est plus subtil : les caractères « courants » (l'alphabet latin par exemple) sont codés sur 8 bits, les caractères moins « courants » sur 16 voire 24 bits.

    Changer l'encodage lors de l'ouverture d'un fichier ne modifie pas les données contenues dans le fichier mais **la façon de les lire**.
    
    Par exemple les bits `#!py 010100110110100101101101011100000110110001100101001000000111010001100101011110000111010001100101` lus :
    
    * avec l'encodage `#!py 'utf-8'` donnent `#!py 'Simple texte'`,
    
    * avec l'encodage `#!py 'utf-16 LE'` donnent `#!py '楓灭敬琠硥整'` !

    L'encodage le plus classique pour nous sera `#!py 'utf-8'`.


La fonction `#!py open` peut être utilisée de deux façons :

* utilisation classique, on ouvre **et** on ferme le fichier :

    ```python
    fichier = open(file="fichier.txt", mode="r", encoding="utf-8")
    # Traitement du fichier
    fichier.close()
    ```

    Techniquement, on devrait même utiliser un `#!py try ... except` au cas où le fichier est inaccessible :

    ```python
    try:
        fichier = open(file="fichier.txt", mode="r", encoding="utf-8")
        # Traitement du fichier
        fichier.close()
    except IOError:  # Le fichier est inaccessible
        print("Le fichier est inaccessible")
    ```

* utilisation avec `with`, il est inutile de fermer le fichier et de gérer les erreurs, c'est automatique :

    ```python
    with open(file="fichier.txt", mode="r", encoding="utf-8") as fichier:
        # Traitement du fichier
    ```

On utilisera la seconde méthode.

## Lire le fichier, des lignes, une ligne !

Une fois le fichier ouvert, on peut réaliser différentes actions. Si l'objet renvoyé par `#!py open` s'appelle `#!py fichier`, on peut :

* `#!py fichier.read()` : lit la totalité du fichier. Renvoie une unique chaîne de caractères.
* `#!py fichier.readlines()` : lit la totalité du fichier **ligne par ligne**. Renvoie la liste contenant les différentes lignes.
* `#!py fichier.readline()` : lit la prochaine ligne du fichier **ligne par ligne**. Renvoie une chaîne de caractères.

??? note "Écrire ?"

    Si le fichier est ouvert en mode *écriture*, on peut écrire en faisant `#!py fichier.write("texte à écrire")`.

    Attention toutefois à ne pas écraser le contenu du fichier !

Les deux scripts ci-dessous sont donc équivalents :

```python
with open(file="fichier.txt", mode="r", encoding="utf-8") as fichier:
    contenu = fichier.readlines()
```


## Importer des données structurées

La problématique est maintenant de pouvoir importer les données stockées dans des fichiers dans une structure de données exploitable en python.

La librairie `csv` permet d'importer facilement ces données en tableau de tableaux ou en tableau de dictionnaires.

!!! example "Tableau de tableaux"

    ```python
    from csv import reader

    with open("prenoms.csv", mode='r') as fichier_ouvert:
        prenoms = list(reader(fichier_ouvert,delimiter=","))
    ```

    Avec cet exemple, la variable `prenoms` contient les données sous la forme d'un tableau de tableaux.

!!! example "Tableau de dictionnaires"

    ```python
    from csv import DictReader

    with open("prenoms.csv", mode='r') as fichier_ouvert:
        prenoms = list(DictReader(fichier_ouvert,delimiter=","))
    ```

    Avec cet exemple, la variable `prenoms` contient les données sous la forme d'un tableau de dictionnaires.