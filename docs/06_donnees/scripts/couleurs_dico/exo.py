

# --------- PYODIDE:code --------- #
dico_couleurs = [
    {"nom": "Noir", "rouge": 0, "vert": 0, "bleu": 0},
    {"nom": "Blanc", "rouge": 255, "vert": 255, "bleu": 255},
    {"nom": "Rouge", "rouge": 255, "vert": 0, "bleu": 0},
    {"nom": "Vert", "rouge": 0, "vert": 255, "bleu": 0},
    {"nom": "Bleu", "rouge": 0, "vert": 0, "bleu": 255},
    {"nom": "Jaune", "rouge": 255, "vert": 255, "bleu": 0},
    {"nom": "Cyan", "rouge": 0, "vert": 255, "bleu": 255},
    {"nom": "Magenta", "rouge": 255, "vert": 0, "bleu": 255},
    {"nom": "Argent", "rouge": 192, "vert": 192, "bleu": 192},
    {"nom": "Gris", "rouge": 128, "vert": 128, "bleu": 128},
    {"nom": "Bordeaux", "rouge": 128, "vert": 0, "bleu": 0},
    {"nom": "Olive", "rouge": 128, "vert": 128, "bleu": 0},
    {"nom": "Violet", "rouge": 128, "vert": 0, "bleu": 128},
    {"nom": "Marine", "rouge": 0, "vert": 0, "bleu": 128},
]

# tableau_1 est la liste des noms des couleurs
tableau_1 = ...

# tableau_2 est la liste des composantes vertes des couleurs
tableau_2 = ...

# tableau_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
tableau_3 = ...

# tableau_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
tableau_4 = ...

# tableau_5 est la liste des noms des couleurs dont la composante bleue est plus grande que la verte et la rouge
tableau_5 = ...

# tableau_6 est la liste des triplets formés par les trois composantes de chaque couleur
tableau_6 = ...


# --------- PYODIDE:corr --------- #
# tableau_1 est la liste des noms des couleurs
tableau_1 = [c["nom"] for c in dico_couleurs]

# tableau_2 est la liste des composantes vertes des couleurs
tableau_2 = [c["bleu"] for c in dico_couleurs]

# tableau_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
tableau_3 = [c["nom"] for c in dico_couleurs if c["rouge"] >= 200]

# tableau_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
tableau_4 = [c["nom"] for c in dico_couleurs if c["rouge"] > c["bleu"]]

# tableau_5 est la liste des noms des couleurs dont la composante bleue est plus grande que la verte et la rouge
tableau_5 = [c["nom"] for c in dico_couleurs if c["bleu"] >= c["vert"] and c["bleu"] >= c["rouge"]]

# tableau_6 est la liste des triplets formés par les trois composantes de chaque couleur
tableau_6 = [(c["rouge"], c["vert"], c["bleu"]) for c in dico_couleurs]




# --------- PYODIDE:secrets --------- #

# tests
ok = True
tableaux = [tableau_1,tableau_2,tableau_3,tableau_4,tableau_5,tableau_6]

# tableau1 est la liste des noms des couleurs
attendu = [c["nom"] for c in dico_couleurs]

# tableau_2 est la liste des composantes vertes des couleurs
attendu = [c["bleu"] for c in dico_couleurs]

# tableau_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
attendu = [c["nom"] for c in dico_couleurs if c[1] >= 200]

# tableau_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
attendu = [c["nom"] for c in dico_couleurs if c["rouge"] > c["bleu"]]

# tableau_5 est la liste des noms des couleurs dont la composante bleue est plus grande que la verte et la rouge
attendu = [c["nom"] for c in dico_couleurs if c["bleu"] >= c["vert"] and c["bleu"] >= c["rouge"]]

# tableau_6 est la liste des triplets formés par les trois composantes de chaque couleur
attendu = [(c["rouge"], c["vert"], c["bleu"]) for c in dico_couleurs]

attendus = [attendu_1,attendu_2,attendu_3,attendu_4,attendu_5,attendu_6]


for i in range(len(attendus)):
    
    terminal_message("bk4fg1", f"Requête {i} : " , new_line=False)
    if tableaux[i] == ...:
        ok = False
        terminal_message("bk4fg1", "\u22EF non traitée", format="info")
    elif tableaux[i] != attendus[i]:
        ok = False
        terminal_message("bk4fg1", "\u274C échec", format="error")
    else:
        terminal_message("bk4fg1", "\u2705 succès", format="success")


assert ok is True, "Il y a des requêtes qui ne sont pas correctes"




