

# --------- PYODIDE:code --------- #
liste_couleurs = [
    ["Noir",       0,   0,   0],
    ["Blanc",    255, 255, 255],
    ["Rouge",    255,   0,   0],
    ["Vert",       0, 255,   0],
    ["Bleu",       0,   0, 255],
    ["Jaune",    255, 255,   0],
    ["Cyan",       0, 255, 255],
    ["Magenta",  255,   0, 255],
    ["Argent",   192, 192, 192],
    ["Gris",     128, 128, 128],
    ["Bordeaux", 128,   0,   0],
    ["Olive",    128, 128,   0],
    ["Violet",   128,   0, 128],
    ["Marine",     0,   0, 128]
]

# tableau_1 est la liste des noms des couleurs
tableau_1 = ...

# tableau_2 est la liste des composantes vertes des couleurs
tableau_2 = ...

# tableau_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
tableau_3 = ...

# tableau_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
tableau_4 = ...

# tableau_5 est la liste des noms des couleurs dont la composante bleue est plus grande que la verte et la rouge
tableau_5 = ...

# tableau_6 est la liste des triplets formés par les trois composantes de chaque couleur
tableau_6 = ...


# --------- PYODIDE:corr --------- #

# tableau_1 est la liste des noms des couleurs
tableau_1 = [c[0] for c in liste_couleurs]

# tableau_2 est la liste des composantes vertes des couleurs
tableau_2 = [c[2] for c in liste_couleurs]

# tableau_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
tableau_3 = [c[0] for c in liste_couleurs if c[1] >= 200]

# tableau_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
tableau_4 = [c[0] for c in liste_couleurs if c[1] >= c[3]]

# tableau_5 est la liste des noms des couleurs dont la composante bleue est plus grande que la verte et la rouge
tableau_5 = [c[0] for c in liste_couleurs if c[3] >= c[2] and c[3] >= c[1]]

# tableau_6 est la liste des triplets formés par les trois composantes de chaque couleur
tableau_6 = [(c[1], c[2], c[3]) for c in liste_couleurs]




# --------- PYODIDE:secrets --------- #

# tests
ok = True
tableaux = [tableau_1,tableau_2,tableau_3,tableau_4,tableau_5,tableau_6]

# tableau_1 est la liste des noms des couleurs
attendu_1 = [c[0] for c in liste_couleurs]


# tableau_2 est la liste des composantes vertes des couleurs
attendu_2 = [c[2] for c in liste_couleurs]


# tableau_3 est la liste des noms des couleurs dont la composante rouge est supérieure ou égale à 200
attendu_3 = [c[0] for c in liste_couleurs if c[1] >= 200]


# tableau_4 est la liste des noms des couleurs dont la composante rouge est strictement supérieure à la bleue
attendu_4 = [c[0] for c in liste_couleurs if c[1] > c[3]]


# tableau_5 est la liste des noms des couleurs dont la composante bleue est plus grande que la verte et la rouge
attendu_5 = [c[0] for c in liste_couleurs if c[3] >= c[2] and c[3] >= c[1]]


# tableau_6 est la liste des triplets formés par les trois composantes de chaque couleur
attendu_6 = [(c[1], c[2], c[3]) for c in liste_couleurs]

attendus = [attendu_1,attendu_2,attendu_3,attendu_4,attendu_5,attendu_6]


for i in range(len(attendus)):
    terminal_message("bk4fg1", f"Requête {i} : " , new_line=False)
    if tableaux[i] == ...:
        ok = False
        terminal_message("bk4fg1", "\u22EF non traitée", format="info")
    elif tableaux[i] != attendus[i]:
        ok = False
        terminal_message("bk4fg1", "\u274C échec", format="error")
    else:
        terminal_message("bk4fg1", "\u2705 succès", format="success")


assert ok is True, "Il y a des requêtes qui ne sont pas correctes"


