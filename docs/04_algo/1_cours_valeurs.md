---
author: Nicolas Revéret, Pierre Marquestaut
title: Parcours séquentiel d'un tableau par valeurs
---

# Parcours séquentiel d'un tableau par valeurs

Considérons le tableau des dix langues les plus parlées dans le monde (en nombre de locuteurs dont c'est la langue maternelle - source : [Wikipédia](https://fr.wikipedia.org/wiki/Liste_de_langues_par_nombre_total_de_locuteurs)) :

```python
plus_parlees = [
    "mandarin",
    "espagnol",
    "anglais",
    "hindi",
    "bengali",
    "portugais",
    "russe",
    "japonais",
    "allemand",
    "cantonais",
]
```

Le mandarin est le premier élément du tableau : c'est la langue maternelle la plus importante. La valeur `"mandarin"` est à l'indice `0` dans le tableau.

Il est très courant de parcourir des tableaux en utilisant une boucle « Pour », avec `for` de Python.

## Parcours par valeurs

Une première approche consiste à parcourir directement les valeurs contenues dans le tableau. Ainsi, le code ci-dessous affichera les mêmes lignes :

```pseudocode
POUR CHAQUE langue DE plus_parlees FAIRE
    AFFICHER langue
FIN POUR
```
Ce qui donne :

```python
for langue in plus_parlees:
    print("-", langue)
```

Comme on peut le voir, le code est allégé, plus facile à lire, car on n'utilise pas les indices pour accéder aux éléments.

!!! note "Rappels"
    L'ensemble des instructions répétées constitue un **bloc d'instructions**.

    Dans l'algorithme, le bloc débute avec le mot-clé `FAIRE` et se termine avec `FIN POUR`.

    En langage Python, le bloc débute après la signe de ponctuation `:` et est marqué par l'**indentation**. Le bloc est terminé lor du recul de l'indentation.



???+ question 
    **Reconstituer** le programme, puis **compléter** la structure itérative.
    <iframe src="https://www.codepuzzle.io/IPMQ5P" width="100%" height="250" frameborder="0"></iframe>



## Compteur

Un compteur permet de déterminer le nombre d'éléments d'un tableau qui respectent une condition donnée.

!!!note "A faire à 2"
    * L'**élève 1** choisit un nombre entre 0 et 5 puis propose à l'**élève 2** un vingtaine de chiffres toujours compris entre 0 et 5.
    * A la fin, l'**élève 2** doit donner le nombre de fois que le nombre choisi est apparu dans la liste des nombres donnés par l'**élève 1**.

    A partir de cette expérience, **proposer** un algorithme qui à partir d'un tableau donné, détermine le nombre d'apparitions (ou d'occurrence) d'une valeur cible.

???+ question 
    Quelle valeur contient la variable `n` à la fin de l'exécution de ce programme ? 

    ```python
    valeurs = [1, 2, 1, 3, 4, 6, 7, 2, 1, 2]
    n = 0
    cible = 1
    for element in valeurs :
        if element == cible :
            n = n + 1
    ```
    <iframe src="https://www.codepuzzle.io/IDY9CV" width="100%" height="300" frameborder="0"></iframe>

???+ question 
    **Reconstituer** le code de la fonction `trouver_lettre` qui comptabilise le nombre d'apparitions d'une lettre dans une chaine de caractères.

    Attention : certaines lignes sont en trop et ne servent pas !
    <iframe src="https://www.codepuzzle.io/IPY27B" width="100%" height="450" frameborder="0"></iframe>

???+ question 
    **Compléter** le script ci-dessous permettant de compter le nombre de pointures égales à 41 dans le tableau.
    {{ IDE('scripts/pointures') }}

!!! faq "Exercice"
    [Longueur d'une liste](https://codex.forge.apps.education.fr/exercices/longueur/){ .md-button target="_blank" rel="noopener"}

## Accumulateur

Pour calculer la somme d'une série de nombre, on utilise une variable en tant qu'**accumulateur**.

!!!note "A faire à 2"

    * L'**élève 1** propose à l'**élève 2** un dizaine de chiffres compris entre 0 et 5.
    * A la fin, l'**élève 2** doit donner la somme de ces chiffres.

    A partir de cette expérience, **proposer** un algorithme qui à partir d'un tableau de nombres donné, détermine la somme de ces nombres.

???+ question
    Reconstituer le programme qui permet de calculer la somme d'une liste d'entiers.

    On utilisera un **parcours par valeur**.

    **Attention : **certaines instructions sont inutiles !
    <iframe src="https://www.codepuzzle.io/IPUZ28" width="100%" height="450" frameborder="0"></iframe>


???+ question 
    Antoine a commis une erreur en créant une fonction pour calculer la somme d'une série de nombres.

    Saurais-tu la trouver ?
    {{ IDE('scripts/somme_erreur') }}




## Moyenne

Pour calculer la moyenne d'une série d'éléments (entiers ou réels), il faut dans un premier tempts calculer leur somme, puis la diviser par le nombre d'éléments.

???+ question 
    **Reconstituer** le code de la fonction `moyenne` qui calcule la moyenne d'une série d'entiers.

    Attention : certaines lignes sont en trop et ne servent pas !
    <iframe src="https://www.codepuzzle.io/IP9QWR" width="100%" height="600" frameborder="0"></iframe>
    
!!! faq "Exercice"
    [Moyenne simple](https://e-nsi.gitlab.io/pratique/N1/118-moyenne/sujet/){ .md-button }

## Extremum

La recherche d'un extremum consiste à rechercher la valeur minimale ou maximale d'une série de valeurs.

!!!note "A faire à 2"

    * L'**élève 1** propose à l'**élève 2** un vingtaine de nombres compris entre 0 et 1000.
    * A la fin, l'**élève 2** doit donner la valeur.

    A partir de cette expérience, **proposer** un algorithme qui à partir d'un tableau de nombres donné, détermine la valeur maximale de ces nombres.


???+ question 
    **Reconstituer** le code de la fonction `maximum` qui renvoie la valeur maximale d'une série d'entiers.

    <iframe src="https://www.codepuzzle.io/IPSZ52" width="100%" height="450" frameborder="0"></iframe>

???+ question
    Florence a commis deux erreurs en programmant une fonction qui renvoie la valeur minimum d'une série de nombres.

    Saurais-tu les retrouver ?
    {{ IDE('scripts/min_erreur_valeur') }}




