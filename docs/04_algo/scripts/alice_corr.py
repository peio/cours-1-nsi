def possibilites(montant):
    rendre = [[i, j, k] 
                for i in range(montant + 1) 
                for j in range(montant + 1)
                for k in range(montant + 1)
                if i*PIECES[0] + j*PIECES[1] + k*PIECES[2] == montant]
    return rendre

def monnaie_brute(montant):
    possibles = possibilites(montant)
    min = montant
    for liste in possibles:
        if sum(liste) < min:
            resultat = liste
            min = sum(liste)
    return (resultat, min)




