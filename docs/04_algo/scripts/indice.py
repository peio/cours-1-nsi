def recherche_valeur(valeurs, cible):
    ...

valeurs = [2, 3, 6, 7, 11, 14, 18, 19, 24]
assert recherche_valeur(valeurs, 14) == 5
assert recherche_valeur(valeurs, 25) == "non trouvé" 