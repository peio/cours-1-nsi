PIECES = [4, 3, 1]

def monnaie_Glouton(montant):
    a_rendre = montant
    nb_pieces = 0
    for piece in PIECES:
        while a_rendre >= piece:
            nb_pieces += 1
            a_rendre = a_rendre - piece
    return nb_pieces
