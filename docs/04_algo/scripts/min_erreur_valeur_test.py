assert trouver_minimum([1,1,1,1,1,1]) == 1, "Erreur quand toutes les valeurs strictement supérieures à 0"
assert trouver_minimum([5,-1,4,9,9,1]) == -1, "La fonction ne renvoie pas la plus petite valeur"

