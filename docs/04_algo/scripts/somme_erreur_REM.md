Dans le code intial, la première valeur était comptée deux fois : une à l'initialisation de la variable `somme` et l'autre lors du parcours du tableau. C'est pour cela qu'il est nécessaire d'initialiser cette variable à 0.

Un autre solution (moins pratique) de correction est la suivante :
```python
def somme(valeurs):
    resultat = valeurs[0]
    for i in range(1, len(valeurs)):
        resultat = resultat + valeur[i]
    return resultat
```
Dans ce cas, on initialise la variable `somme` avec la première valeur, puis on commence le parcours à la deuxième valeur pour ne pas additionner deux fois la première. Pour ce faire, on a été obligé d'utiliser un parcours par indice.