# --------- PYODIDE:code --------- #

def somme(valeurs):
    resultat = 0
    nb_valeurs = len(valeurs)
    for i in [0, nb_valeurs]:
        resultat = resultat + valeurs[0]
    return resultat

# --------- PYODIDE:corr --------- #

def somme(valeurs):
    resultat = 0
    nb_valeurs = len(valeurs)
    for i in range(0, nb_valeurs):
        resultat = resultat + valeurs[i]
    return resultat

# --------- PYODIDE:secrets --------- #

assert somme([0, 0, 0]) == 0
assert somme([2, 1, 3]) == 6, "La fonction ne prend pas en compte toutes les valeurs"
