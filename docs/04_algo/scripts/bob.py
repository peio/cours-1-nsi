PIECES = [4, 3, 1]

def monnaie_Glouton(montant):
    a_rendre = montant
    nb_pieces = 0
    for piece in PIECES:
        ...


# Test

assert monnaie_Glouton(6) == 3
assert monnaie_Glouton(10) == 4
