Si on initialise le minimum à $0$, on court le risque que toutes les valeurs lui soit supérieures, et donc de renvoyer $0$ au lieu du minimum.

Il faut bien faire attention avec les signes `<` et `>`. En se trompant de signe, on renvoie la valeur maximale au lieu de la minimale.

