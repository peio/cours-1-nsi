def trouver_minimum(valeurs):
    minimum = valeurs[0]
    nombres_valeurs = len(valeurs)
    for i in range(0, nombres_valeurs):
        if minimum > valeurs[i]:
            minimum = valeurs[i]
    return minimum
