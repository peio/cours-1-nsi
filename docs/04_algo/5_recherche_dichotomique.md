---
author: Gilles Lassus, Pierre Marquestaut
title: Recherche dichotomique
---

# Dichotomie

*ou comment rechercher efficacement dans une liste triée ?*

!!! abstact
    «dichotomie» se dit en anglais *binary search*.

<!--
# Découpages

Le « *Plus grand, plus petit* » est un classique des jeux d'enfants :

- le premier joueur pense à un nombre entier compris dans un certain intervalle,
- le second essaie de le deviner,
- à chaque proposition, le premier joueur répond par « *Mon nombre est plus grand* », « *Mon nombre est plus petit* » ou « *Tu as trouvé !* ».

Mettons-nous à la place du second joueur : quelle méthode adopter ?

Il est possible de tester tous les nombres de l'intervalle dans l'ordre croissant mais cela risque d'être long si le nombre secret est à la fin... Cette méthode est de coût linéaire.

Une autre approche est de débuter au milieu de l'intervalle et de modifier la zone de recherche en fonction de la réponse du premier joueur :

* s'il répond « *Mon nombre est plus petit* », on cherche désormais dans la partie gauche de la zone,
* s'il répond « *Mon nombre est plus grand* », on cherche désormais dans la partie droite de la zone.

Exprimée en Python, cette méthode devient :

```python
reponse = ""
while reponse != "Tu as trouvé !" :
    milieu = (fin + debut) // 2
    reponse = input(f"Je propose {milieu}. Qu'en pensez-vous ?")
    if reponse == "Mon nombre est plus petit" :
        fin = milieu - 1
    elif reponse == "Mon nombre est plus grand" :
        debut = milieu + 1
```

Appliquons cet algorithme. On joue sur l'intervalle $[0,\,100]$ et le nombre cherché est $43$.

| `#!py debut` | `#!py fin` | `#!py milieu` |           `#!py reponse`           |
| :----------: | :--------: | :-----------: | :--------------------------------: |
|   `#!py 0`   | `#!py 100` |   `#!py 50`   | `#!py "Mon nombre est plus petit"` |
|   `#!py 0`   | `#!py 49`  |   `#!py 24`   | `#!py "Mon nombre est plus grand"` |
|  `#!py 25`   | `#!py 49`  |   `#!py 37`   | `#!py "Mon nombre est plus grand"` |
|  `#!py 38`   | `#!py 49`  |   `#!py 43`   |      `#!py "Tu as trouvé !"`       |

Il a fallu 4 propositions.

???+ question "Combien d'étapes ?"

    On joue à nouveau dans l'intervalle $[0,\,100]$.

    === "Cocher la ou les affirmations correctes"
        
        - [ ] Si le nombre à trouver est $62$, il faut 3 étapes
        - [ ] Si l'on cherche $12$, après 3 propositions, la zone de recherche est $[11,\,23]$
        - [ ] $1$ est plus long à trouver que $0$
        - [ ] Cette méthode donne la bonne réponse en moins de 7 propositions

    === "Solution"
        
        - :white_check_mark: Les zones de recherche sont $[0,\,100]$ (on propose $50$), $[51,\,100]$ (on propose $75$), $[51,\,74]$ (on propose $62$)
        - :x: Les zones de recherche sont $[0,\,100]$ (on propose $50$), $[0,\,49]$ (on propose $24$), $[0,\,23]$ (on propose $11$) et on cherche ensuite dans $[12,\,23]$
        - :white_check_mark: En effet. Pour trouver $1$, il faut passer par les étapes $[0,\,100]$, $[0,\,49]$, $[0,\,24]$, $[0,\,11]$, $[0,\,4]$, $[0,\,1]$, $[1,\,1]$. Il aurait fallu une étape de moins pour trouver $0$
        - :white_check_mark: Dans le cas présent, $1$ est un des nombres les plus longs à déterminer. La réponse précédente montre qu'il faut 7 propositions

???+ question "Combien d'étapes pour trouver $0$ ?"

    Dans toutes les propositions, on cherche $0$. Par contre, l'intervalle change.

    === "Cocher la ou les affirmations correctes"
        
        - [ ] Si l'intervalle est $[0,\,9]$, il faut 3 propositions
        - [ ] Si l'intervalle est $[0,\,19]$, il faut 4 propositions
        - [ ] Si l'intervalle est $[0,\,39]$, il faut 5 propositions
        - [ ] Si l'intervalle est $[0,\,79]$, il faut 6 propositions

    === "Solution"
        
        - :white_check_mark: Les zones de recherche sont $[0,\,9]$ (on propose $4$), $[0,\,3]$ (on propose $1$) et on cherche ensuite dans $[0,\,0]$
        - :white_check_mark: Les zones de recherche sont $[0,\,19]$ (on propose $9$), $[0,\,8]$ (on propose $4$), $[0,\,3]$ (on propose $1$) et on cherche ensuite dans $[0,\,0]$
        - :white_check_mark: Les zones de recherche sont $[0,\,39]$ (on propose $19$), $[0,\,18]$ (on propose $9$), $[0,\,8]$ (on propose $4$), $[0,\,3]$ (on propose $1$) et on cherche ensuite dans $[0,\,0]$
        - :white_check_mark: Les zones de recherche sont $[0,\,79]$ (on propose $39$), $[0,\,38]$ (on propose $19$), $[0,\,18]$ (on propose $9$), $[0,\,8]$ (on propose $4$), $[0,\,3]$ (on propose $1$) et on cherche ensuite dans $[0,\,0]$

Combien d'étapes sont nécessaires ? La question précédente nous donne une piste.

Dans chaque cas, l'intervalle de départ contient deux fois plus de valeurs (10 pour $[0,\,9]$, 20 pour $[0,19]$, *etc*.). On se rend compte que le nombre d'étapes augmente de 1 à chaque fois.

Le coût de cet algorithme est donc lié à la largeur de l'intervalle (on s'en doutait) mais plus précisément au **nombre de fois où l'on peut la diviser par deux** avant que l'intervalle ne soit réduit à une seule valeur.

On dit dans ce cas que l'algorithme est de coût **logarthmique**.

Cette relation est illustrée sur le graphique ci-dessous liant la largeur de l'intervalle au nombre de propositions faites avant de proposer $0$. On observe bien qu'il faut une proposition de plus à chaque fois que le nombre de valeurs dans l'intervalle double.

![Nombre de propositions](images/dichot.svg){ width=90% .center }

!!! note "Remarque"

    L'algorithme étudié dans cette page est appelé « *Recherche dichotomique* ». Il fonctionne sur des tableaux **triés dans l'ordre croissant** et est très efficace.

    « *dichotomie* » signifie « *partager en deux parts égales* »

    En appliquant cette méthode, trouver un nombre compris entre $0$ et $1\,000\,000$ demande moins de $20$ étapes !

    De manière générale, le nombre d'étapes maximal est environ le nombre de bits dans l'écriture binaire de la largeur de l'intervalle de recherche.
    
    C'est une définition approximative du **logarithme en base $2$**, d'où le terme **coût logarithmique**.
<!--
## Jeu du *"devine un nombre entre 1 et 100"*
Si je choisis un nombre entre 1 et 100, quelle est la stratégie optimale pour deviner ce nombre le plus vite possible ?  
(à chaque étape, une indication (trop grand, trop petit) permet d'affiner la proposition suivante)

**Réponse attendue :** la meilleure stratégie est de *couper en deux* à chaque fois l'intervalle d'étude. On démarre de 50, puis 75 ou 25, etc.


Il convient toute fois de remettre en question cette méthode qui paraît *naturellement* optimale : si je propose 90 comme nombre de départ, j'ai certes moins de chance que le nombre soit entre 90 et 100, mais s'il l'est, j'ai gagné un gros avantage car mon nouvel intervalle est très réduit.

On peut alors rappeler la notion d'**espérance probabiliste**.

Exemple : "On lance un dé, s'il tombe sur le 6 vous recevez 2 euros, sinon vous me donnez 1 euro. Voulez-vous jouer ?" 

**Retour sur le jeu du choix du nombre**

Le graphique ci-dessous représente le nombre de coups moyens (sur 10 000 parties simulées)

![image](images/fig1.png)

**Interprétations et remarques** 
- si le choix se porte *toujours* sur le nombre situé à la moitié de l'intervalle (0.5), le nombre de coups moyen avant la victoire (sur 10 000 parties) est environ 6.
- si le choix se porte *toujours* sur le nombre situé à 90 % de l'intervalle (0.9), le nombre de coups moyen avant la victoire (sur 10 000 parties) est environ 11.
- l'asymétrie de la courbe (qui devrait être symétrique) est due aux arrondis par défaut dans le cas de nombres non entiers.

## Conclusion générale de l'activité d'introduction
La stratégie optimale est de diviser en deux à chaque étape l'intervalle d'étude. On appelle cela une méthode par **dichotomie**, du grec ancien διχοτομία, dikhotomia (« division en deux parties »).

La méthode de dichotomie fait partie des méthodes dites *«diviser pour mieux régner»*. 

Extrait de [Wikipedia](https://fr.wikipedia.org/wiki/Diviser_pour_r%C3%A9gner_(informatique)) :

![](images/diviser_pour_regner.png)

-->
##  1. Introduction : recherche d'une valeur dans une liste

### 1.1 Préambule : liste non triée

**Exemple :** pouvez-vous deviner la couleur à laquelle je pense ?


```python
coul = ["bleu", "jaune", "rouge", "vert", "violet", "marron"]
```

Toutes les méthodes (proposition des valeurs dans l'ordre, au hasard, dans l'ordre inverse...) sont équivalentes car la liste n'est pas triée.

!!! question "Exercice"

    <iframe src="https://app.Lumi.education/api/v1/run/JSFmOS/embed" width="950" height="520" frameborder="0" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe>


Dans toute la suite, nous rechercherons un élément dans une liste d'entiers **triée** dans l'ordre croissant.  

### 1.2 Contexte de recherche

Considérons donc le tableau suivant : 

![image](images/fig0.png){: .center}


```python
valeurs = [2, 3, 6, 7, 11, 14, 18, 19, 24]
```

L'objectif est de définir un algorithme de recherche efficace d'une valeur arbitraire présente dans cette liste.

### 1.3 Méthode naïve : recherche par balayage
C'est la méthode la plus intuitive : on essaie toutes les valeurs (par exemple, dans l'ordre croissant) jusqu'à trouver la bonne.


!!! question "Exercice 1"

    Écrire une fonction `trouve(valeurs, cible)` qui renvoie l'indice d'une valeur `cible` dans un tableau `valeurs `. Si la valeur `cible` n'est pas trouvée, on renverra `"non trouvé"`.
    {{ IDE('scripts/indice') }}

??? soluce "Correction"
    ```python linenums='1'
    valeurs = [2, 3, 6, 7, 11, 14, 18, 19, 24]
    def recherche_valeur(tab_valeurs, cible):
        for k in range(len(tab_valeurs)): 
            if tab_valeurs[k] ==  cible : 
                return k 
        return "non trouvé" 

    ```


### 1.3 Complexité de la méthode naïve

!!! note "Complexité de la méthode naïve :heart:"
    Dans le cas d'une recherche naïve, le nombre (maximal) d'opérations nécessaires est proportionnel à la taille de la liste à étudier. Si on appelle $n$ la longueur de la liste, on dit que cet algorithme est **d'ordre $n$**, ou **linéaire**, ou en $O(n)$.

**Remarque :** 
La méthode naïve n'utilise pas le fait que la liste est triée, on aurait pu aussi bien l'utiliser sur une liste non triée.


## 2. Recherche dichotomique

### 2.1 Introduction : le jeu du *«devine un nombre entre 1 et 100»*


???+ question "Règles du jeu"

    Si je choisis un nombre entre 1 et 100, quelle est la stratégie optimale pour deviner ce nombre le plus vite possible ?  
    (à chaque étape, une indication (trop grand, trop petit) permet d'affiner la proposition suivante)
    
    === "Cocher les réponses correctes"
        
        - [ ] Je commence par 1
        - [ ] Je commence par 27
        - [ ] Je commence par 50
        - [ ] Je commence par 90

    === "Solution"
        
        - :x: Si je commence par $1$ et que ce n'est pas la bonne solution, il restera encore $99$ réponses possibles.
        - :x: Prendre des nombres au hasard peut fonctionner, mais après un grand nombre de réponses.
        - :white_check_mark: la meilleure stratégie est de *couper en deux* à chaque fois l'intervalle d'étude. On démarre de 50, puis 75 ou 25, etc...car à chaque fois, on supprime la moitié des réponses
        - :x: si je propose 90 comme nombre de départ, j'ai certes moins de chance que le nombre soit entre 90 et 100, mais s'il l'est, j'ai gagné un gros avantage car mon nouvel intervalle est très réduit.


!!! Tips

    La stratégie optimale est de diviser en deux à chaque étape l'intervalle d'étude. On appelle cela une méthode par **dichotomie**, du grec ancien διχοτομία, dikhotomia (« division en deux parties »).



### 2.2 Algorithme de recherche dichotomique

!!! note "Dichotomie, déroulement intuitif"
    - on se place *au milieu* de la liste.
    - on regarde si la valeur sur laquelle on est placée est inférieure ou supérieure à la valeur cherchée.
    - on ne considère maintenant que la bonne moitié de la liste qui nous intéresse.
    - on continue jusqu'à trouver la valeur cherchée (ou pas)

<!--
### 2.3 Illustration

Recherchons la valeur 14 dans notre liste `lst`.

![image](images/fig3.png){: .center}




- étape 1 : toute la liste est à traiter. On se place sur l'élément central. Son indice est la partie entière de la moitié de la longueur de la liste. Ici il y a 9 éléments, donc on se place sur le 4ème, qui est 11.
- étape 2 : on compare 11 à la valeur cherchée (14). Il faut donc garder tout ce qui est supérieur à 11.
- étape 3 : on se place au milieu de la liste des valeurs qu'il reste à traiter. Ici il y a 4 valeurs, donc il n'y a pas de valeur centrale. On va donc se positionner sur la 2ème valeur, qui est 18.
- étape 4 : on compare la valeur 18 à la valeur cherchée : 14. Elle est supérieure, donc on garde ce qui est à gauche. Il n'y a plus qu'une valeur.
- étape 5 : on se place sur la valeur 14 et on compare avec 14. La valeur est trouvée.

-->
!!! question "Exercice 1"

    <iframe src="https://app.Lumi.education/api/v1/run/yD1uxe/embed" width="950" height="550" frameborder="0" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe>

!!! question "Exercice 2"

    <iframe src="https://app.Lumi.education/api/v1/run/T8oOCV/embed" width="950" height="550" frameborder="0" allowfullscreen="allowfullscreen" allow="geolocation *; microphone *; camera *; midi *; encrypted-media *"></iframe>

### 2.3 Programmation de la méthode de dichotomie

Comprendre la méthode de dichotomie est relativement simple, mais savoir la programmer est plus difficile.

Pour des raisons d'efficacité, nous allons garder *intacte* notre liste de travail et simplement faire évoluer les indices qui déterminent le début et la fin de notre liste.

Une autre méthode pourrait être d'extraire à chaque étape une nouvelle liste (dont on espère qu'elle contient la valeur cherchée), mais la technique utilisée (le *slicing* de liste) consomme beaucoup trop de ressources.

Nous allons donc travailler avec trois variables :

- `indice_debut` (en bleu sur le schéma)
- `indice_fin` (en bleu sur le schéma)
- `indice_central`, qui est égale à `(indice_debut + indice_fin) // 2` (en rouge sur le schéma)
![image](data/fig4.png){: .center}

Nous allons faire *se rapprocher* les indices `indice_debut` et `indice_fin` **tant que** `indice_debut <= indice_fin`


???+ question "Recherche dichotomique dans une liste triée :heart: :heart: :heart:"
    **Reconstituer** la fonction `recherche_dichotomique`
    <iframe src="https://www.codepuzzle.io/IPTFGQ" width="100%" height="600" frameborder="0"></iframe>

**Utilisation**

```python
>>> mylist = [2, 3, 6, 7, 11, 14, 18, 19, 24]
>>> recherche_dichotomique(mylist, 14)
5
>>> recherche_dichotomique(mylist, 2)
0
>>> recherche_dichotomique(mylist, 24)
8
>>> recherche_dichotomique(mylist, 2022)
>>> 
```



### 2.5 Terminaison de l'algorithme
Est-on sûr que l'algorithme va se terminer ?  
La boucle `while` qui est utilisée doit nous inciter à la prudence.  
Il y a en effet le risque de rentrer dans une boucle infinie.  
Pourquoi n'est-ce pas le cas ?

**Aide :** observer la position des deux flèches bleues lors de l'exécution de l'algorithme 
![image](images/fig4.png){: .center}



La condition de la boucle `while` est `indice_debut <= indice_fin `, qui pourrait aussi s'écrire `indice_fin >= indice_debut `.  
Au démarrage de la boucle, on a :


```python
    indice_debut = 0
    indice_fin = len(L) - 1
```

Ceci qui nous assure donc de bien rentrer dans la boucle. 

Ensuite, à chaque étape, les deux variables `indice_debut` et `indice_fin` vont se **rapprocher** jusqu'à ce que le programme rencontre un `return` ou bien jusqu'à ce que `indice_fin` devienne inférieur à `indice_debut`.  

Ceci nous assure donc que le programme va bien se terminer.

**Variant de boucle**  
On dit que la valeur `indice_fin - indice_debut ` représente le **variant de boucle** de cet algorithme. 
Ce variant est un nombre entier, d'abord strictement positif, puis qui va décroître jusqu'à la valeur 0.

### 2.6 Complexité de l'algorithme

Combien d'étapes (au maximum) sont-elles nécessaires pour arriver à la fin de l'algorithme ?  
Imaginons que la liste initiale possède 8 valeurs. 
Après une étape, il ne reste que 4 valeurs à traiter. 
Puis 2 valeurs.  
Puis une seule valeur.  
Il y a donc 3 étapes avant de trouver la valeur cherchée.

!!! abstract "Exercice"
    === "Énoncé"
        1. Remplissez le tableau ci-dessous :

        | taille de la liste | 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 |
        | :----------------- |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
        | nombre d'étapes    | _ | _ |  _  |   3 |  _ | _   | _   | _   | _   |  _  |

        2. Pouvez-vous deviner le nombre d'étapes nécessaires pour une liste de 4096 termes ?
        3. Pour une liste de $2^n$ termes, quel est le nombre d'étapes ?
    === "Correction"
         1. Remplissez le tableau ci-dessous :

        | taille de la liste | 1 | 2 | 4 | 8 | 16 | 32 | 64 | 128 | 256 |
        | :----------------- |:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
        | nombre d'étapes    | 0 | 1 |  2  |   3 |  4 |5    | 6   | 7   | 8   |  9  |

        2. Nombre d'étapes nécessaires pour une liste de 4096 termes : 12
        3. Nombre d'étapes pour une liste de $2^n$ termes : n étapes

**Conclusion :** 

C'est le nombre de puissances de 2 que contient le nombre $N$ de termes de la liste qui est déterminant dans la complexité de l'algorithme. 

Ce nombre s'appelle le *logarithme de base 2* et se note $\log_2(N)$.

 On dit que l'algorithme de dichotomie a une **vitesse logarithmique**. On rencontrera parfois la notation $O(\log_2(n))$.

!!! note "Complexité de la dichotomie :heart: :heart: :heart:"
    La recherche dichotomique se fait avec une **complexité logarithmique**.


Cette complexité est bien meilleure qu'une complexité linéaire. Le nombre d'opérations à effectuer est très peu sensible à la taille des données d'entrée, ce qui en fait un algorithme très efficace.
