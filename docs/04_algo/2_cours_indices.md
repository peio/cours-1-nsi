---
author: Nicolas Revéret, Pierre Marquestaut
title: Parcours séquentiel d'un tableau par indices
---

# Parcours séquentiel d'un tableau par indices

Considérons le tableau des dix langues les plus parlées dans le monde (en nombre de locuteurs dont c'est la langue maternelle - source : [Wikipédia](https://fr.wikipedia.org/wiki/Liste_de_langues_par_nombre_total_de_locuteurs)) :

```python
plus_parlees = [
    "mandarin",
    "espagnol",
    "anglais",
    "hindi",
    "bengali",
    "portugais",
    "russe",
    "japonais",
    "allemand",
    "cantonais",
]
```

Le mandarin est le premier élément du tableau : c'est la langue maternelle la plus importante. La valeur `"mandarin"` est à l'indice `0` dans le tableau.

Il est très courant de parcourir des tableaux en utilisant une boucle « Pour », avec `for` de Python.

## Parcours par indices



On a vu qu'il est possible de parcourir directement les valeurs contenues dans le tableau. 

Une deuxième approche consiste à parcourir les indices, puis d'accéder aux éléments du tableau grâce aux indices.

L'affichage de chaque langue donnera l'algorithme suivant :

```pseudocode
POUR indice ALLANT DE premier_indice A dernier_indice FAIRE
    AFFICHER plus_parlees[indice]
FIN POUR
```

Ce qui donne en Python :

```python
for indice in range(0, len(plus_parlees)):
    print("-", plus_parlees[indice])
```

Cette méthode est légèrement plus complexe, mais est nécessaire pour certains algorithmes, comme les algorithmes de tri.

???+ question "Parcours par indices"

    On donne le programme suivant :

    ```python
    ventes = [5, 4, 6, 8, 7]
    nb_ventes = len(ventes)
    total_ventes = 0
    for i in range(0, nb_ventes):
        total_ventes = total_ventes + ventes[i]
    ```
    
    Quels éléments du programme permettent de dire que qu'il s'agit d'un parcours par indices ?
    
    === "Cocher les réponses correctes"
        
        - [ ] la variable `#!py i`
        - [ ] le mot clé `#!py for`
        - [ ] la fonction `#!py range()`
        - [ ] l'expression `#!py ventes[i]`

    === "Solution"
        
        - :x: Le nom de variable `#!py i` est souvent employé pour l'indice d'un tableau, mais ce n'est en rien obligatoire.
        - :x: le mot clé `#!py for` crée une boucle et permet les parcours par indices et par valeurs.
        - :white_check_mark: la fonction `#!py range()` est utilisée pour créer l'intervale de valeurs que peut prendre l'indice.
        - :white_check_mark: l'expression `#!py ventes[i]` permet d'accéder à un élément du tableau grâce à son indice.

???+ question "Parcours"

    On donne le programme suivant :

    ```python
    ventes = [5, 4, 6, 8, 7]
    total_ventes = 0
    for une_vente in ventes:
        total_ventes = total_ventes + une_vente
    ```

    === "Cocher les affirmations correctes."
        
        - [ ] il s'agit d'un parcours par valeurs.
        - [ ] le résultat du programme sera le même que pour le programme précédent.
        - [ ] le programme va générer une erreur car il n'a pas de fonction `#!py range()`.
        - [ ] tous les éléments du tableau ne seront pas pris en compte.

    === "Solution"
        
        - :white_check_mark: Il s'agit effectivement d'un parcours par valeurs.
        - :white_check_mark: Ici les deux façon d'écrire le parcours, par indices et par valeurs, conduisent effectivement au même résultat.
        - :x: la fonction `#!py range()` n'est pas obligatoire dans une boucle `#!py for`.
        - :x: avec ce parcours, la variable `#!py une_vente` prendra tour à tour toutes les valeurs contenues dans le tableau.


???+ question
    **Reconstituer** le programme, puis **compléter** la structure itérative.
    <iframe src="https://www.codepuzzle.io/IPGR47" width="100%" height="250" frameborder="0"></iframe>

## Compteur

Un compteur permet de déterminer le nombre d'éléments d'un tableau qui respectent une condition donnée.

!!! note "Rappels"

    L'algorithme est le suivant :
    
    ```pseudocode
    compteur <-
    POUR indice ALLANT DE premier_indice A dernier_indice FAIRE
        SI condition
            compteur <- compteur + 1
        FIN SI
    FIN POUR
    ```
    A partir de cette expérience, **proposer** un algorithme qui à partir d'un tableau donné, détermine le nombre d'apparitions (ou d'occurrence) d'une valeur cible.

???+ question 
    Quelle valeur contient la variable `n` à la fin de l'exécution de ce programme ? 
    
    ```python
    valeurs = [1, 2, 1, 3, 4, 6, 7, 2, 1, 2]
    n = 0
    cible = 2
    nb_valeurs = len(valeurs)
    for i in (0, nb_valeurs) :
        if valeurs[i] == cible :
            n = n + 1
    ```
    <iframe src="https://www.codepuzzle.io/IDY9CV" width="100%" height="300" frameborder="0"></iframe>


???+ question 
    **Compléter** le script ci-dessous permettant de compter le nombre de pointures égales à 41 dans le tableau.
    {{ IDE('scripts/pointures_indice') }}


## Accumulateur

Pour calculer la somme d'une série de nombre, on utilise une variable en tant qu'**accumulateur**.

!!! note "Rappels"

    L'algorithme est le suivant :
    ```pseudocode
    somme <- 0
    POUR indice ALLANT DE premier_indice A dernier_indice FAIRE
        somme <- valeurs[indice]
    FIN POUR
    ```
    

???+ question 
    Reconstituer le programme qui permet de calculer la somme d'une liste d'entiers.

    On utilisera un **parcours par indice**.

    **Attention :** certaines instructions sont inutiles !
    <iframe src="https://www.codepuzzle.io/IPSYAN" width="100%" height="450" frameborder="0"></iframe>

???+ question 
    Gabin a commis deux erreurs en créant une fonction pour calculer la somme d'une série de nombres.

    Saurais-tu les trouver ?
    {{ IDE('scripts/somme_erreur_indice') }}


!!! faq "Exercice"
    [Indices ou valeurs](https://codex.forge.apps.education.fr/exercices/indices_valeurs/){ .md-button }


## Moyenne

Pour calculer la moyenne d'une série d'éléments (entiers ou réels), il faut dans un premier tempts calculer leur somme, puis la diviser par le nombre d'éléments.

!!! note "Rappels"

    L'algorithme est le suivant :
    ```pseudocode
    somme <- 0
    POUR indice ALLANT DE premier_indice A dernier_indice FAIRE
        somme <- valeurs[indice]
    FIN POUR
    moyenne <- somme/nb_éléments
    ```

???+ question 
    **Reconstituer** le code de la fonction `moyenne` qui calcule la moyenne d'une série d'entiers.

    Attention : certaines lignes sont en trop et ne servent pas !  A vous de déterminer s'il s'agit d'un parcours par indice ou par valeur
    <iframe src="https://www.codepuzzle.io/IP6GKL" width="100%" height="500" frameborder="0"></iframe>

!!! faq "Exercice"
    [Moyenne simple](https://codex.forge.apps.education.fr/exercices/moyenne/){ .md-button }

## Extremum

La recherche d'un extremum consiste à rechercher la valeur minimale ou maximale d'une série de valeurs.

!!!note "Rappels"

    L'algorithme est le suivant :
    ```pseudocode
    maximum <- valeur[0]
    POUR indice ALLANT DE premier_indice A dernier_indice FAIRE
        SI valeur[indice] > maximum
            maximum <- valeur[indice]
        FIN SI
    FIN POUR
    ```


???+ question 
    **Reconstituer** le code de la fonction `maximum` qui renvoie la valeur maximale d'une série d'entiers.

    Attention : certaines lignes sont en trop et ne servent pas !
    <iframe src="https://www.codepuzzle.io/IPWG4U" width="100%" height="560" frameborder="0"></iframe>

???+ question
    Florence a commis deux erreurs en programmant une fonction qui renvoie la valeur minimum d'une série de nombres.

    Saurais-tu les retrouver ?
    {{ IDE('scripts/min_erreur') }}



!!! faq "Exercice"
    [Maximum](https://codex.forge.apps.education.fr/exercices/maximum_nombres/){ .md-button }