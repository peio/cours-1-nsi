---
author: Jean-Louis Thirot, Valérie Mousseaux et Mireille Coilhac
title: Le codage hexadécimal
---

## I. La base 16 : découverte de l'hexadécimal 

Vous avez déjà rencontré les nombres hexadécimaux lors de la construction de votre site WEB en SNT, avec l'utilisation des feuilles de styles pour la gestion des couleurs. Pour coder une couleur, on utilise un code hexadécimal avec 6 chiffres.

Quelques exemples :

![couleurs](images/couleurs.PNG){ width=80% }

Source : [wikipedia](https://fr.wikipedia.org/wiki/Couleur_du_Web){:target="_blank" }

* Chaque nombre hexadécimal comporte 6 chiffres, mémorisés sur trois octets, utilisés en HTML et dans les feuilles de style en cascade (CSS), et d'autres applications, pour représenter les couleurs.

* Les octets représentent les composantes rouge, vert et bleu de la couleur. Tous les nombres entre 00 et FF (en notation hexadécimale), c'est à dire entre 0 et 255 en notation décimale peuvent êrte représentés sur un octet.  
Ainsi pour la couleur red codée FF0000, il y a FF pour la composante red, 00 pour la composante green et 00 pour la composante blue.

* Chaque couleur doit être codée sur 6 chiffres (en hexadécimal). Si nécessaire, on met donc des 0.

## II. La base 16

### 1. Les nombres en hexadécimal

!!! abstract "Résumé"

	👉 En base 16 (hexadécimal), nous disposons de 16 symboles 0, 1, 2, …, 9, A, B, C, D, E, F pour écrire les nombres.

<table>
<tr >
<td style="width:100px;">Base 10</td>
<td width="50px">1</td>
<td  width="50px">2</td>
<td width="50px">3</td>
<td  width="50px">4</td>
<td  width="50px">5</td>
<td  width="50px">6</td>
<td  width="50px">7</td>
<td  width="50px">8</td>
<td  width="50px">9</td>
<td  width="50px">10</td>
<td  width="50px">11</td>
<td  width="50px">12</td>
<td  width="50px">13</td>
<td  width="50px">14</td>
<td  width="50px">15</td>

</tr>
<tr >
<td style="width:100px;">Base 16</td>
<td width="50px">1</td>
<td  width="50px">2</td>
<td width="50px">3</td>
<td  width="50px">4</td>
<td  width="50px">5</td>
<td  width="50px">6</td>
<td  width="50px">7</td>
<td  width="50px">8</td>
<td  width="50px">9</td>
<td  width="50px">A</td>
<td  width="50px">B</td>
<td  width="50px">C</td>
<td  width="50px">D</td>
<td  width="50px">E</td>
<td  width="50px">F</td>

</tr>
</table>

!!! example "Exemple"

	Nous utilisons toujours le même principe : 
	Le nombre 2021 peut se décomposer de la façon suivante : $7 \times 16^2 + 14 \times 16^1 + 5 \times 16^0$

	$(2021)_{10} = (7E5)_{16}$


### 2. Compter en hexadécimal

<table>
<tr >
<td style="width:100px;">Base 10</td>
<td width="50px">1</td>
<td  width="50px">2</td>
<td width="50px">3</td>
<td  width="50px">4</td>
<td  width="50px">5</td>
<td  width="50px">6</td>
<td  width="50px">7</td>
<td  width="50px">8</td>
<td  width="50px">9</td>
<td  width="50px">10</td>
<td  width="50px">11</td>
<td  width="50px">12</td>
<td  width="50px">13</td>
<td  width="50px">14</td>
<td  width="50px">15</td>
<td  width="50px">16</td>
<td  width="50px">17</td>
<td  width="50px">18</td>
<td  width="50px">19</td>
<td  width="50px">20</td>
</tr>

<tr >
<td style="width:100px;">Base 16</td>
<td width="50px">1</td>
<td  width="50px">2</td>
<td width="50px">3</td>
<td  width="50px">4</td>
<td  width="50px">5</td>
<td  width="50px">6</td>
<td  width="50px">7</td>
<td  width="50px">8</td>
<td  width="50px">9</td>
<td  width="50px">A</td>
<td  width="50px">B</td>
<td  width="50px">C</td>
<td  width="50px">D</td>
<td  width="50px">E</td>
<td  width="50px">F</td>
<td  width="50px">10</td>
<td  width="50px">11</td>
<td  width="50px">12</td>
<td  width="50px">13</td>
<td  width="50px">14</td>
</tr>

</table>

???+ question

    Vous avez compris le principe. Ecrivez les 20 entiers écrits en hexadécimal qui suivent 21 qui est écrit en hexadécimal.

    ??? success "Solution"

        22 - 23 - 24 - 25 - 26 - 27 - 28 - 29 - 2A - 2B - 2C - 2D - 2E - 2F - 30 - 31 - 32 - 33 - 34 - 35

???+ question

    Quel est l'entier écrit en hexadécimal qui suit le nombre FF écrit en hexadécimal ?

    ??? success "Solution"

        100

???+ question

    Quel est l'entier écrit en hexadécimal qui suit le nombre FFF écrit en hexadécimal ?

    ??? success "Solution"

        1000

???+ question

    Quel est l'entier écrit en hexadécimal qui suit le nombre 2FFF écrit en hexadécimal ?

    ??? success "Solution"

        3000

??? note pliée "Utilisation du binaire et de l'hexadécimal"

	Vous l'aurez remarqué, les nombres en base 2 s'écrivent avec beaucoup de chiffres.
	Si cela répond bien à la structure interne de la mémoire d'un ordinateur, cela les rend difficiles à lire pour les humains.

	😀 Pour cette raison, on utilise fréquemment la base 16. Nous l'avons déjà vu pour le codage des couleurs.


## III. Les conversions

### 1. De la base 16 à la base 10


!!! example "Exemple : de la base 16 à la base 10"

	 $(80C)_{16} = (8 \times 16^2 + 0 \times 16^1 + 12 \times 16^0)_{10} = (2060)_{10}$

???+ question "De la base 16 à la base 10"

    Convertir en décimal : $(E26A)_{16}$

    ??? success "Solution"

    	$(14 \times 16^3 + 2 \times 16^2 + 6 \times 16^1 + 10 \times 16^0 )_{10} = (57962)_{10}$


### 2. De la base 10 à la base 16

En fait le principe est le même que celui que nous avons déjà étudié pour passer de la base 10 à la base 2.

!!! abstract "Le principe"

	* Nous allons faire des divisions successives par 16, car nous voulons convertir notre nombre en base 16.
	* On poursuit les divisions jusqu'à obtenir un quotient égal à **0** pour la dernière division.
	* On lit ensuite les restes, en partant du bas.	

!!! example "Etudier cet exemple"

	Source : [Académie de Limoges](http://pedagogie.ac-limoges.fr/sti_si/accueil/FichesConnaissances/Sequence2SSi/co/ConvDecimalHexadecimal.html){:target="_blank" }

	![divisions](images/conv_dec_hexa.png){ width=60% }


???+ question "Conversion"

    $(1256)_{10}  = (?)_{16 }$ 

    ??? success "Solution"

    	4E8


### 3. En base 16 : les groupements par 4 bits

Remarquons que $(2^4)_{10} = (16)_{10}=(10000)_2$

Les chiffres de 0 à 15 seront donc codés sur 4 bits en binaire, et sur un seul (1, 2 ... F) en hexadécimal.

De même,  $(10)_{16} = (16)_{10} = (2^4)_{10} = (0001\ 0000)_2$

De même $(FF)_{16} = 15 \times 16^1 + 15 = (255) _{10} = (1111\ 1111)_2$

&#128073;On constate que le 1er groupe de 4 bits code le premier chiffre en hexadécimal, et que le second groupe de 4 bits code le second chiffre hexadécimal.

Cette propriété reste vraie pour de plus grands nombres. 

!!! example "Etudier cet exemple"

	$(4E5)_{16} = (0100\ 1110\ 0101)_2$ 
	
	En effet : 

	* $(4)_{16}  = (0100)_2$
	* $(E)_{16}  = (1110)_2$
	* $(5)_{16}  = (0101)_2$

!!! abstract "Le principe"

	Pour convertir de la base 2 vers la base 16, **ou inversement**, il suffit de faire des groupes de 4 bits.

!!! example "Exemple"

	![D6](images/D6.jpg){ width=30% }

### 4. 😀 Conversion directe entre l'hexadécimal et le binaire

#### a. Commençons par convertir un seul chiffre écrit en hexadécimal

!!! example "Exemple"

	$(D)_{16} = (?)_2$

	On sait que $(D)_{16} = (13)_{10}$

	On peut évidemment convertir 13 en base 2 en réalisant des divisions successives par 2.

	💡 On peut aussi utilser un tableau comme pré"senté ci-dessous :

	13 > 8. On met 1 pour la colonne 8. De plus 13 - 8 = 5  
	5 > 4. On met 1 pour la colonne 4. De plus 5 - 4 = 1  
	1 < 2. On met 0 pour la colonne 2.  
	On termine en mettant 1 dans la colonne 1  

	$13 = 1 \times 8 + 1 \times 4 + 0 \times 2 + 1 \times 1$ 

	|1 |1 | 0|1 |
	|:--:|:--:|:--:|:--:|
	| 8| 4|2 | 1|

	Et donc : $(D)_{16} = (1101)_2$


#### b. Pour un nombre à plusieurs chiffres en hexadécimal

!!! example "Exemple"

	On écrit chaque chiffre en binaire sur 4 bit.


	$(AC1)_{16} = (?)_2$

	$(A)_{16} =10=(1010)_2$

	$(C)_{16} =12=(1100)_2$

	$(1)_{16} =1=(0001)_2$

	![AC1](images/AC1.jpg){ width=30% }

	et donc : $(AC1)_{16} = (1010\ 1100\ 0001)_2$


## IV. L'hexadécimal et Python


👉 Le préfixe `0x` sert à expliquer que le nombre qui suit est écrit en hexadécimal.

???+ question

    Tester ci-dessous, et ajoutez vos propres essais.

    {{IDE('scripts/hexa')}}

???+ question

    Tester ci-dessous, et ajoutez vos propres essais.

    {{IDE('scripts/type_hexa')}}
