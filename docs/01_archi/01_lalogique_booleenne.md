---
author: Pierre marquestaut
title: Logique booléenne
---

# La logique booléenne

## 1. Les variables booléennes

!!! abstract "Définition"

    Une **variable booléenne** ne peut prendre que deux valeurs :

    - **vrai**, représenté par convention par $1$ et par `True` en python 
    - **faux**, représenté par convention par $0$ et par `False` en python

## 2. Les portes logiques

### La porte NON

Avec la porte NON, la sortie est à Vrai si son entrée est à False.

<script src="https://logic.modulo-info.ch/simulator/lib/bundle.js "></script>
<div style="width: 100%; height: 100px">
  <logic-editor mode="tryout">
    <script type="application/json">
      {
        "v": 3,
        "opts": {"showOnly": ["or", "and", "in", "out", "not"]},
        "in": [{"pos": [50, 50], "id": 0, "val": 0}],
        "gates": [{"type": "NOT", "pos": [150, 50], "in": 1, "out": 2}],
        "out": [{"pos": [250, 50], "id": 3}],
        "wires": [[0, 1], [2, 3]]
      }
    </script>
  </logic-editor>
</div>


### La porte ET

Avec la porte ET, la sortie est à Vrai si ses deux entrées sont à Vrai.

<div style="width: 100%; height: 105px">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in1: {type: 'in', pos: [40, 30], id: 7},
          in0: {type: 'in', pos: [40, 75], id: 13},
          out0: {type: 'out', pos: [250, 50], id: 14},
          and0: {type: 'and', pos: [140, 50], in: [15, 16], out: 17},
        },
        wires: [[7, 15], [13, 16], [17, 14]]
      }
    </script>
  </logic-editor>
</div>

### La porte OU

Avec la porte OU, la sortie est à Vrai si l'une ou l'autre de ses entrées est à Vrai.

<div style="width: 100%; height: 105px">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in1: {type: 'in', pos: [40, 30], id: 7},
          in0: {type: 'in', pos: [40, 75], id: 13},
          out0: {type: 'out', pos: [250, 50], id: 14},
          or0: {type: 'or', pos: [150, 50], in: [18, 19], out: 20},
        },
        wires: [[7, 18], [13, 19], [20, 14]]
      }
    </script>
  </logic-editor>
</div>


### La porte OU exclusif

Avec la porte OU exclusif, la sortie est à Vrai si l'une ou l'autre de ses entrées est à Vrai, mais pas les deux à la fois.

<div style="width: 100%; height: 105px">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in1: {type: 'in', pos: [40, 30], id: 7},
          xor0: {type: 'xor', pos: [160, 45], in: [10, 11], out: 12},
          in0: {type: 'in', pos: [40, 75], id: 13},
          out0: {type: 'out', pos: [255, 45], id: 14},
        },
        wires: [[7, 10], [13, 11], [12, 14]]
      }
    </script>
  </logic-editor>
</div>

Cette porte peut être réalisée à partir des portes de bases (ET, OR et NON).

<div style="width: 100%; height: 195px">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in1: {type: 'in', pos: [40, 30], id: 7, val: 1},
          in0: {type: 'in', pos: [35, 165], id: 13, val: 1},
          out0: {type: 'out', pos: [565, 90], id: 14},
          or0: {type: 'or', pos: [455, 90], in: [18, 19], out: 20},
          not0: {type: 'not', pos: [150, 135], in: 21, out: 22},
          and0: {type: 'and', pos: [250, 50], in: [23, 24], out: 25},
          not1: {type: 'not', pos: [145, 80], in: 26, out: 27},
          and1: {type: 'and', pos: [260, 155], in: [28, 29], out: 30},
        },
        wires: [[20, 14], [7, 21], [13, 26], [27, 24], [7, 23], [25, 18], [22, 28], [13, 29], [30, 19]]
      }
    </script>
  </logic-editor>
</div>

!!! note "Remarque"

    En Python, on retrouve les même portes logiques à savoir `not`, `and`, `or` et `^` (ou exclusif).

    Tester dans la console ci-dessous le code suivant :
    ```pycon
    >>> a = True 
    >>> b = False
    >>> a or b
    >>> not a
    >>> a and b
    ```

    {{ terminal }}

## 3. Les opérateurs de comparaison

!!! abstract "Définition"

    Les opérateurs de comparaison permettent de comparer des valeurs entre elles :

    * `==` : égalité (pour des nombres ou des chaînes).
    * `!=` : inégalité (pour des nombres ou des chaînes).
    * `>=` `>` `<` `<=` : comparaison

!!! warning "`=` ou `==`"
    Il est fréquent de confondre le symbole d'affectation `=` avec le symbole de comparaison `==`.

    Imaginer ce que fait le code suivant puis le tester dans la console ci-dessous :
    ```pycon title=""
    >>> a = 5 
    >>> a == 4
    ```

    {{ terminal() }}

    ??? soluce "Solution"
        * `a = 5` est l'instruction qui affecte la valeur $5$ dans la variable `a`.
        * `a == 4` est l'expression qui compare la valeur contenue dans la variable `a` (à savoir $5$) avec la valeur $4$.


## 5. Les expressions booléennes

!!! abstract "Définition"
    Une **expression booléenne** est une expression qui est évaluée par une valeur booléenne, `True` ou `False`.

    Une expression booléenne est une combinaison de *comparaisons*, de *variables booléennes* et *portes logiques*.

Plusieurs expressions booléennes peuvent conduire à la même table de vérité.

<div style="width: 100%; height: 220px">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in0: {type: 'in', pos: [185, 80], id: 0},
          in1: {type: 'in', pos: [185, 140], id: 1},
          and0: {type: 'and', pos: [315, 110], in: [2, 3], out: 4},
          out0: {type: 'out', pos: [490, 110], id: 5},
          not0: {type: 'not', pos: [405, 110], in: 6, out: 7},
        },
        wires: [[0, 2], [1, 3], [4, 6], [7, 5]]
      }
    </script>
  </logic-editor>
</div>

Une autre version de cette expression peut être la suivante :

<div style="width: 100%; height: 220px">
  <logic-editor mode="tryout">
    <script type="application/json">
      { // JSON5
        v: 6,
        components: {
          in0: {type: 'in', pos: [185, 80], id: 0},
          in1: {type: 'in', pos: [185, 140], id: 1},
          out0: {type: 'out', pos: [490, 110], id: 5},
          not0: {type: 'not', pos: [270, 80], in: 6, out: 7},
          or0: {type: 'or', pos: [395, 110], in: [8, 9], out: 10},
          not1: {type: 'not', pos: [270, 140], in: 11, out: 12},
        },
        wires: [[10, 5], [7, 8], [1, 11], [0, 6], [12, 9]]
      }
    </script>
  </logic-editor>
</div>

!!! note "Remarques"

    Les expressions booléennes sont utilisées dans les structures conditionnelles et itératives.

    ```python title="Structure conditionelle"
    if x > 5:
      y = 4
    ```
    ```python title="Structure itérative"
    while x > 5:
      x = x - 1
    ```


!!! note "Priorités"

    Les expressions booléennes respectent plusieurs règles de priorité.

    - On évalue les comparaisons avant les portes logiques
    - On évalue d'abord les expressions entre parenthèse
    - la porte ET est prioritaire sur le parte OU

Ainsi `True or False and True` est évaluée :
<pre>
   True or <u>False and True</u>
   <u>True or     False</u>
    True
</pre>

<iframe 
  src="{{ page.canonical_url }}../activites/Expressions_booleennes.html"
  width="900" height="300"
  id="modele"
  title="Vocabulaire" 
  frameborder="0" 
  allow="autoplay; fullscreen; picture-in-picture">
</iframe>


<iframe src="https://www.codepuzzle.io/IPGFWP2" width="100%" height="350" frameborder="0"></iframe>

??? tips "Commentaires"

    Quand on a plusieurs valeurs à tester (comme ici les valeurs $4$ et $6$), il est nécessaire de faire deux comparaisons.
    ```python
    if valeur_de == 6 or valeur_de == 4:
      gagne = True
    ```

    En effet, il est tentant d'écrire `valeur_de == 6 or 4` (car plus proche du langage parlé) mais la comparaison est prioritaire par rapport à la porte *OU*.

    Un autre solution est d'utiliser le mot-clé `in`, afin de comparer la variable à une liste de valeurs :
    ```python
    if valeur_de in [6, 4]:
      gagne = True
    ```



    

