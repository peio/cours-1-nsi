---
author: Pierre Marquestaut
title: Les Variables
---


# Expressions et Variables



## Définition

Les variables sont des éléments qui associent un **nom** à une **valeur**, qui sera implantée dans la **mémoire** du système programmé. 

## Affectation

L'**affectation** est une instruction qui modifie la valeur contenue dans la variable.

!!! note 

    Voici comment écrire l'affectation de la valeur $5$ dans la variable `ma_variable` en différents langages
    
    === "en C"
        
        ```
        ma_variable = 5;
        ```

    === "en Ada"
        
        ```
        ma_variable := 5;
        ```

    === "en pseudocode"
        
        ```
        ma variable <- 5
        ```

On parle d'**initialisation** quand on affecte une variable pour la première fois.
 
## Nom

!!! note "Choix du nom"

    Le nom d'une variable est choisi par la programmeur. Il doit cependant respecter certaines règles.

<iframe id="vocabulaire"
    title="Nom des variables"
    width="100%"
    height="620"
    src="{{ page.canonical_url }}../activites/nom variables.html">
</iframe>

??? soluce "Explications"
    * Un nom de variables est composé des lettres de l'alphabet (en minuscule ou majuscule) ainsi que le caractère `_`.
    * Un nom de variable peut contenir des chiffres mais il ne peut pas commencer par un chiffre.
    * Certains mots-clés (`if`, `for`...) sont déjà utilisés par le langage et ne peuvent pas être utilisés pour des variables.
    

## Type

Une variable est caractérisée par son **type**.

Les principaux types de base sont :

| Nom du type         |Nom en Python|Exemple  |
|---------------------|-------------|---------|
| entier              | int         | 5       | 
| réel                | float       | 6.3     |
| booléen             | bool        | True    |
| chaine de caractères| str         | "sésame"|

La déclaration d'une variable est la création d'une variable avec la réservation, ou **allocation**, d'une zone de mémoire suffisante pour contenir le **type** de variable.

!!! note 

    Voici comment écrire une déclaration de variables en différents langages
    
    === "en C"
        
        ```
        int ma_variable;
        ```

    === "en Ada"
        
        ```
        ma_variable INTEGER;
        ```

    === "en Basic"
        
        ```
        Dim ma_variable as INTEGER;
        ```

!!! note "Déclaration en Python"

    Dans le langage Python, il n'y a pas de déclaration de variable car elle se fait de manière implicite.

    ```python
    ma_variable = 5
    ```

    En initialisant la variable `ma_variable` avec la valeur $5$, l'interpréteur Python va allouer automatiquement une zone de mémoire suffisante pour stocker un entier.



## Expression

En programmation, une **expression** est la notation d'un calcul à réaliser. 

Elle est formée d'une combinaison de valeurs, variables, opérateurs et d'appels de fonction.

L'expression est **évaluée** par l'interpréteur Python. 

```python
a = 5
b = a + 4
```

Dans cet exemple, l'interpréteur évalue d'abord l'expression `a + 4` et trouve $9$, et va ensuite affecter cette valeur à la variable `b`.

!!! warning "Erreur d'initialisation"

    Une erreur se produit lorsque l'on utilise une variable *non initialisée* dans une expression.

!!! note "Console"

    La **console** de Python permet d'afficher rapidement l'évaluation d'une expression.

    Dans la console ci-dessous, évaluer l'expression ```5 * 6```, puis après avoir défini la variable ```a = 5```, évaluer ```a + 6```

    {{ terminal() }}

## Incrémentation et décrémentation

L'**incrémentation** est l'opération qui consiste à ajouter une valeur entière fixée à un compteur. Sans autre précision, la variable est augmentée de $1$.

```python
a = 5
a = a + 1
```
Dans cet exemple, l'interpréteur évalue d'abord l'expression `a + 1` et trouve $6$, et va ensuite affecter cette valeur à la variable `a`. L'instruction a bien permis de rajouter la valeur $1$ à la variable `a`

!!! note "Incrémentation et boucle""

    Une boucle bornée réalise une incrémentation de l'itérateur (ci-dessous la variable `i`). La valeur par défaut du pas d'incrémentation est $1$.

    ```python
    for i in range(5):
        ...
    ```


!!! note "Forme abrégée"

    Voici comment écrire une incrémentation de façon abrégée en différents langages
    
    === "en C"
        
        ```
        ma_variable++;
        ```

    === "en Python"
        
        ```
        ma_variable += 1
        ```

La **décrémentation** est l'opération qui consiste à enlever une valeur entière fixée à un compteur.

!!! note "Décrémentation et boucle"

    Il est également possible de réaliser la décrémentation d'une variable avec une boucle bornée. Dans ce cas, il faut préciser un pas d'incrémentation négatif.
    
    ```python
    for i in range(5, 0, -1):
        ...
    ```

## Questions


<iframe id="vocabulaire"
    title="Vocabulaire sur les variables"
    width="100%"
    height="450"
    src="{{ page.canonical_url }}../activites/voc variable.html">
</iframe>

<iframe id="vocabulaire"
    title="Vocabulaire sur les variables"
    width="100%"
    height="400"
    src="{{ page.canonical_url }}../activites/QCM variable.html">
</iframe>