def etre_accompagne(age):
    accompagne = None
    if age < 12:
        accompagne = True
    if age > 12 and age < 77:
        accompagne = False

    return accompagne