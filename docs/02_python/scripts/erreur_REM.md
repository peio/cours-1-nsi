!!! note
    Quand vous écrivez une boucle `for`, veillez bien à :
    * finir la ligne du for par les deux points :
    * indenter sous le for les instructions qui doivent être répétées. Si l'indentation ne s'est pas faite automatiquement après appui sur la touche Entrée, c'est que vous avez oublié les :.

