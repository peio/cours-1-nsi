def valeur_absolue1(x) :
    '''fonction qui calcule la valeur absolue du nombre x
    >>> valeur_absolue1(-1)
    1
    '''
    
    if x < 0:
        return -x
    else :
        return x