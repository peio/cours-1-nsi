# --- PYODIDE:env --- #
from js import document
if "restart" in globals():
    restart()

# --- PYODIDE:code --- #
from turtle import *

def carre(cote):
    ...

    
carre(30)
carre(50)

# on cache la tortue
hideturtle()  

# --- PYODIDE:post --- #
done()
document.getElementById("cible_5").innerHTML = svg()   