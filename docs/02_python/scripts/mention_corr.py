def mention_bac(note):
    if note < 8:
        return 'Recalé'
    elif note < 10:
        return 'Rattrapage'
    elif note < 12:
        return 'Admis'
    elif note < 14:
        return 'Assez bien'
    elif note < 16:
        return 'Bien'
    elif note < 18:
        return 'Très bien'
    else:
        return 'Félicitations'
        
assert mention_bac(5) == 'Recalé'
assert mention_bac(13) == 'Assez bien'
