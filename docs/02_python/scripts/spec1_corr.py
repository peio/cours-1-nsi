def etre_accompagne(age):
    assert type(age)==int
    assert age>=7 and age<=77
    accompagne = None
    if age < 12:
        accompagne = True
    if age >12 and age < 77:
        accompagne = False
    
    assert type(accompagne) == bool
    return accompagne