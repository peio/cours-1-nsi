def valeur_absolue2(x) :
    '''fonction qui calcule la valeur absolue du nombre x
    >>> valeur_absolue2(-1)
    1
    '''
    return -x