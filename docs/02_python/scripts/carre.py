def carre(x):
    """
    fonction qui renvoie le carre d'un nombre
    @argument1 : nombre flottant ou entier
    @return : nombre flottant ou entier
    """
    return x * x