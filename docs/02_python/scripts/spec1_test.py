assert etre_accompagne(10), "La fonction ne passe pas les tests pour une utilisation normale"
assert not etre_accompagne(20)  , "La fonction ne passe pas les tests pour une utilisation normale"
validation = True
try:
    etre_accompagne(6)  
except AssertionError:
    terminal_message("1234","La première précondition sur les valeurs est correcte","success")
else:
    terminal_message("1234","La précondition sur les valeurs est incorrecte","error")
    validation = False
try:
    etre_accompagne(80)
except AssertionError:
    terminal_message("1234","La deuxième précondition sur les valeurs est correcte","success")
else:
    terminal_message("1234","La précondition sur les valeurs est incorrecte","error")
    validation = False
 
try:
    etre_accompagne(77)
except AssertionError:
    terminal_message("1234","La postcondition sur les types de sortie est correcte","success")
else:
    terminal_message("1234","La postcondition sur les types de sortie est incorrecte","error")
    validation = False

try:
    etre_accompagne(10.3)
except AssertionError:
    terminal_message("1234","La précondition sur les types d'entrée est correcte","success")
else:
    terminal_message("1234","La précondition sur les types d'entrée est incorrecte","error")
    validation = False

assert validation is True, "La fonction ne respecte pas les spécifications"