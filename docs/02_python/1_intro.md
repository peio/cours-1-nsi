---
author: Pierre Marquestaut
title: Introduction à Python
---

# Introduction à Python

[Un peu d'Histoire](https://ladigitale.dev/digiquiz/q/655ddd2c14434/){ .md-button target="_blank" rel="noopener"}
<iframe src="https://ladigitale.dev/digiquiz/q/655ddd2c14434" allow="fullscreen; autoplay;" frameborder="0" width="100%" height="600"></iframe>

## Langage de programmation



!!! abstract "Programme"
    Un **programme informatique** est un ensemble d'**instructions**  destinées à être exécutées par un ordinateur.

    Le **code source** est un programme informatique écrit de façon lisible pour un humain.
    
!!! abstract "Langages de programmation"

    Un **langage de programmation** est une notation conventionnelle destinée à implémenter des **algorithmes** et produire des programmes informatiques qui les appliquent.

    Un programme peut être sous différentes formes : code source, script, fichier exécutable...

    Un langage permet donc de décrire les instructions à l'intérieur du code source.

!!! note "Pseudo-code"
 
    Le **pseudo-code** est une façon de décrire un algorithme en langage presque naturel, sans référence à un langage de programmation en particulier. 



!!! note "Différents langages de programmation"

    Il existe plus de 150 langages de programmation différents.

    Ci-dessous, on trouve un même algorithme traduit en pseudo-code, puis en trois différents langages de programmation.

    === "en pseudocode"
        
        ```pseudocode
        Fonction moyenne(valeurs)
            somme <- 0
            nombre <- 0
            pour chaque élément dans valeurs
                somme <- somme + élément
                nombre <- nombre + 1
            Fin pour
            renvoyer somme/nombre
        Fin Fonction

        ```
    === "en Python"
        
        ```python
        def moyenne(valeurs):
            somme = 0
            for element in valeurs:
                somme += element
            return somme/float(len(valeurs))
        ```

    === "en Ada"
    
        ```python
        function moyenne (valeurs : Vector) return Float is
            somme : Float := 0.0;
            begin
                for indice in valeurs'range loop
                    somme := somme + valeurs(indice);
                end loop;
                return somme / Float(valeurs'Length);
            end moyenne;
        ```

    === "en C"

        ```python
        double moyenne(double *valeurs, int len)
        {
            double somme = 0;
            int i;
            for (i = 0; i < len; i++)
                somme += valeurs[i];
            return somme / len;
        }
        ```

    
!!! note "Choix du langage"

    Lors du développement d’un projet, avant la phase de programmation, une des première chose à faire est le choix du ou des langages à utiliser.

    Les critères de choix sont :

    - Le domaine d’application
    - La facilité d’utilisation
    - Le paradigme
    - La machine cible

## Exécution d'un programme




!!! abstract "Exécution"
    L'exécution est le processus par lequel un ordinateur met en œuvre les instructions d'un programme, en mettant à disposition ses ressources matérielles : processeur, mémoires, périphériques...
    
!!! warning
    Un code source est lisible par un être humain, mais n'est pas compréhensible pas par la machine. Il est donc obligatoire de le rendre lisible par la machine. 

    Cela peut se faire par un **compilateur** ou un **interpréteur**.


!!! abstract "Compilation"

    Un **compilateur** permet de transformer un code source en un fichier exécutable.

    ![compilation](img/fr-compiled-languages.png)

    Dans les langages compilés, on trouve le langage C, le Java, l'Ada...


!!! abstract "Interprétation"

    Un **interpréteur**  transforme les instructions d'un code source en instructions exécutables par la machine.

    ![compilation](img/fr-interpreted-languages.png)

    Dans les langages interprétés, le code source est aussi appelé **script**.

    Les langages Python, javascript et PHP sont des langages interprétés.



## Environnement Python

!!! example "Environnement"
    La fenêtre ci-dessous est un environnement Python.

    A gauche se trouve un **terminal** qui peut contenir un script de plusieurs instructions, et à droite se trouve la console.

    Exécuter ce script pour voir sur le résultat sur la fenêtre graphique.

    {{ IDEv('scripts/turtle1')}}
    
    <div id="cible_1" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>


!!! example "Erreur"

    Cliquer sur le bouton "Afficher la console"

    La console a plusieurs utilités, l'une d'entre elles est d'afficher les erreurs du code.

    Dans le terminale, supprimer une parenthèse puis executer de nouveau le code. (une erreur doit s'afficher dans la console)


!!! example "La console"
    Dans la console, chaque ligne commence par un **prompt**. 

    Dans la console Python ce prompt est constitué de trois chevrons: `>>>`

    Il est possible de taper des instructions directement dans la console. Il faudra alors le faire ligne par ligne.

    Taper le code suivant dans la console :

    ```pycon
    >>> forward(50)
    >>> left(45)
    >>> forward(100)
    >>> done()
    ```

## Commentaires
!!! abstract "Définition"
    Un commentaire est un texte ajouté au code d'un programme servant à décrire le code source, facilitant sa compréhension par les humains.

    Une ligne de commentaire est donc ignorée à l'exécution.

    En Python, un commentaire commence par le symbole dièse `#`.

!!! example "Exemple"
    Dans le code ci-dessus, la ligne `# on cache la tortue` est ignorée par l'interpréteur.

    Par contre, elle permet au programmeur de comprendre l'instruction `hideturtle()`.

## Bloc d'instructions

!!! abstract "Définition"
    Un bloc d'instructions est un ensemble d'instructions qui se réalisent sous certaines conditions.

    En Python,un bloc d'instructions commence par le symbole `:` et est marqué par une indentation, c'est-à-dire d'espaces supplémentaire (généralement 4 espaces, soit une tabulation)

!!! example "Exemple"
    ```python
    for i in range() : #début du bloc
        haut()
        bas()          #fin du bloc
    gauche()
    ```

!!! note "bloc d'instructions dans d'autres langages"


    === "en Ada"
        Le bloc d'instructions commence par le mot-clé `begin` et se termine par le mot-clé `end`.

        ```pseudocode
        function moyenne (Item : Vector) return Float is
            Sum : Float := 0.0;
            begin
                for I in Item'range loop
                    Sum := Sum + Item(I);
                end loop;
                return Sum / Float(Item'Length);
            end moyenne;
        ```

    === "en C"
        Le bloc d'instructions est encadré par des accolades.

        ```pseudocode
        double moyenne(double *v, int len)
        {
            double sum = 0;
            int i;
            for (i = 0; i < len; i++)
                sum += v[i];
            return sum / len;
        }
        ```

    === "en Lua"
        Le bloc d'instructions se termine par le mot-clé `end`.

        ```python
        function mean (numlist)
            if type(numlist) ~= 'table' then return numlist end
            num = 0
            table.foreach(numlist,function(i,v) num=num+v end)
            return num / #numlist
        end
        ```
    On peut remarquer que les autres programmes sont également indentés. Il ne s'agit pas d'une obligation, mais pour rendre le code plus lisible.

!!! note "Blocs d'instructions imbriqués"
    Il est fréquent d'avoir des blocs d'instructions imbriqués, c'est-à-dire un bloc d'instructions contenant lui-même un ou plusieurs blocs d'instructions.


## Questions flash

<iframe id="vocabulaire"
    title="Vocabulaire sur les langages"
    width="100%"
    height="400"
    src="{{ page.canonical_url }}../activites/voc langage.html">
</iframe>


*Sources des images : https://lucidar.me*