---
author: Pierre Marquestaut
title: Spécifications
---

# Spécifications

## 1. Définition

!!! abstract "spécification fonctionnelle"
    Dans une démarche de projet, la spécification fonctionnelle consiste au découpage d’un logiciel à réaliser en fonctions et à la description de ces fonctions.

!!! example "Exemple"
    On souhaite créer un programme qui permet d'afficher un itinéraire.

    La spécification fonctionnelle va décomposer ce programme en différentes fonctions :

    * Recherche de la position GPS
    * Sélection des coordonnées d'arrivée
    * Calcul de l'itinéraire optimal
    * Affichage de l'itinéraire.

## 2. Prototype d'une fonction

!!! abstract "Prototype"
    Le prototype d’une fonction est l’ensemble des caractéristiques nécessaires à son appel :
    
    * Son **nom**
    * Ses **attributs** (nature, type)
    * Sa **valeur de retour** (nature, type)


    
???+ question 

    On a le prototype suivant :
    ```python linenums='1'
    def ma_fonction(a : str, b : float) -> int
    ```
    Parmi les appels suivants, quels sont ceux qui sont conformes avec le prototype ?
    === "Cocher la réponse correcte"
        
        - [ ] `reponse = 5 + ma_fonction("toto", 3.4)`
        - [ ] `ma_fonction(6, 7.65)`
        - [ ] `ma_fonction("question")`
        - [ ] `reponse =  "" + ma_fontion("bonjour", 4.5)`
        - [ ] `ma_fonction("NSI", 0.5)`
        - [ ] `ma_fonction("BAC", 3.4, 15)`


    === "Solution"
        
        - :white_check_mark: 
        - :x: Le premier argument doit être de type `str`. 
        - :x: Il manque le deuxième argument.
        - :x: La valeur de retour est de type `int` et ne peut être additionné avec une valeur de type `str`. 
        - :white_check_mark: 
        - :x: Il y a un argument en trop.

## 3. Conditions

!!! abstract "Préconditions"
    Une **précondition** est une condition à vérifier à l’appel d’une fonction afin de pouvoir en garantir le résultat.

??? note "`#!py assert` ?"

    Le mot clé `#!py assert` est utilisé en Python afin de vérifier que des propositions sont vraies.
    

    Ainsi, l'instruction `#!py assert 3 + 5*7 == 38` permet de vérifier que l'expression `#!py 3 + 5 * 7` est bien évaluée à `#!py 38`.
    
    Si c'est le cas, le programme continue de se dérouler normalement. Dans le cas contraire, le programme est interrompu et une erreur est signalée. 

    Il est également possible de rajouter un message afin de rendre l'erreur plus explicite.

    ```python
    assert 3 + x * 7 == 38, "La valeur de x n'est pas solution de l'équation"
    ```

???+ question 

    La fonction racine prend un paramètre `nombre` qui doit être associé à un argument entier et positif.
    Quelles sont les assertions nécessaires à cette fonction ?
    === "Cocher la réponse correcte"
        
        - [ ] `assert nombre >= 0`
        - [ ] `assert type(nombre) == float`
        - [ ] `assert nombre == 0`
        - [ ] `assert type(nombre) == bool`
        - [ ] `assert nombre < 0`
        - [ ] `assert type(nombre) == int`


    
    === "Solution"
        
        - :white_check_mark:
        - :x: Le nombre serait un nombre réel.
        - :x: 
        - :x:
        - :x:       
        - :white_check_mark: 


!!! abstract "Postconditions"
    Une **postcondition** est une condition à vérifier à la fin d’une fonction. une condition que doivent vérifier les paramètres d'entrée et de sortie ; en dehors de cette condition, la solution apportée au problème n'est pas correcte.

???+ question 

    Une fonction a un paramètre `nombre` qui doit être associé à un argument entier et positif.
    Quelles sont les assertions nécessaires à cette fonction ?
    === "Cocher la réponse correcte"
        
        - [ ] `assert nombre >= 0`
        - [ ] `assert type(nombre) == float`
        - [ ] `assert nombre == 0`
        - [ ] `assert type(nombre) == bool`
        - [ ] `assert nombre < 0`
        - [ ] `assert type(nombre) == int`


    
    === "Solution"
        
        - :white_check_mark:
        - :x: Le nombre serait un nombre réel.
        - :x: 
        - :x:
        - :x:       
        - :white_check_mark: 

## 4. Documentation
 
!!! abstract "Documentation"
    La documentation d’une fonction explique ce que fait cette fonction et comment l’utiliser. Elle doit contenir :

    * L’objectif de la fonction
    * Les spécifications du prototype
    * Les préconditions et postconditions

!!! note "Docstring"
    En python les **docstrings** permettent de documenter une fonction, un module ou tout autre unité logiciel.
    
    Ils sont placés en début de fonction, indentés, et encadré par trois guillemets.

!!! example "Exemple"
    {{ IDEv('scripts/carre')}}

    La fonction `carre` est documentée par un docstring.

    Après avoir exécuté ce script, taper l'instruction `help(carre)`. Qu'obtient-on ?

    ??? soluce "Solution"
        La fonction `help` fait apparaitre le contenu du doctring.

        Cela permet à un programmeur qui ne connait pas la fonction de savoir comment l'utiliser.



## Exercice

Une attraction de fête foraine est destinée aux personnes agées de $7$ à $77$ ans. Par contre, les enfants de moins de 12 ans doivent être accompagné.

La fonction `etre_accompagne` prend un paramètre entier compris entre $7$ et $77$ et renvoie une valeur booléenne, qui vaut `True`si la personne doit être accompagnée, et `False` sinon.

Cette fonction est déjà écrite ci-dessous. Compléter la fonction afin d'ajouter les préconditions et postconditions nécessaires.

{{ IDE('scripts/spec1',STD_KEY="1234") }}

