---
author: Pierre Marquestaut
title: Jeux de test
---

# Jeux de tests

## Principe

!!! note "Découpage d'un programme"

    Il est compliqué de trouver un *bug* à l'intérieur d'un programme de plusieurs dizaines, voire plusieurs centaines de lignes.

    La phase de spécification d'un projet informatique permet de découper un programme en différentes fonctions, ce qui permet de faciliter la recherche de bugs.



!!! abstract "Test unitaire"

    On parle de **test unitaire** le test d'une fonction qui compose un programme.


!!! abstract "Test"

    Un test consiste à comparer le **résultat attendu** d'une fonction avec la **valeur renvoyée** par cette fonction.

???+ question "Exemple"

    La fonction `valeur_absolue` noté $f(x) = |x|$ est définie par :

     * $f(x) = x$ pour $x >= 0$
     * $f(x) = -x$ pour $x < 0$

    Proposer un test pour la fonction `valeur_absolue`

    ??? soluce "Solution"

        Par exemple, pour la valeur $-1$, on attend la valeur de retour $1$.





!!! abstract "Jeu de tests"

    Un test unitaire est constitué de ce qu’on appelle un **jeu de tests** qui consiste à vérifier sur des exemples que le résultat est bien celui attendu. 

    Un jeu de tests doit prendre en compte l'ensemble des cas d'usage de la fonction :

    * les **cas typique**, qui représentent des données d'entrée normale,
    * les **cas limite**, qui corresponde à des situations extrêmes, en dehors d'une utilisation normale.

???+ question "Exemple"

    Proposer un jeu de tests pour la fonction `valeur_absolue`

    ??? soluce "Solution"

        |valeur d'entrée|valeur attendue|
        |---------------|---------------|
        |   $-1$        |      $1$      |
        |   $0$         |      $0$      |
        |   $1$         |      $1$      |






!!! warning "Correction du programme"

    Ce n’est pas parce qu’un jeu de test est réussi que le programme sera correct. 

Pour le reste de l'activité, on utilisera deux fonctions : un fonction valide `valeur_absolue1` et une non valide `valeur_absolue2`

## Tests manuels



!!! note "Tests dans la console"

    Une fois la fonction en mémoire, la **console** associée à l'éditeur Python permet de réaliser des tests rapidement.

???+ question ""

    Exécutez le code ci-dessous :

    {{ IDEv('scripts/valeurabsolue2')}}
    Exécuter le code a permis de mettre en mémoire la fonction.

    Tester la fonction dans la console (partie de droite) comme ci-dessous :

    ```pycon
    >>> valeur_absolue(-1)
    1
    >>> valeur_absolue(0)
    0
    ```
    Les tests effectués sont-il suffisants pour prouver la validité de la fonction ?
    
    ??? soluce "Solution"

        Les tests précédents sont insuffisants car ne prennent pas en compte le cas des valeurs d'entrée positives.


    


???+ question ""

    Exécutez le code ci-dessous :

    {{ IDEv('scripts/valeurabsolue1')}}
    Exécuter le code a permis de mettre en mémoire la fonction.

    Tester la fonction dans la console (partie de droite) comme ci-dessous :

    ```pycon
    >>> valeur_absolue(-1)
    1
    >>> valeur_absolue(0)
    0
    ```

## Tests automatisés

!!! tips "Tests manuels vs tests automatisés" 

    Faire des tests dans la console permet de vérifier rapidement la validité d'une fonction.

    En revanche, effecter les tests à la main peut devenir fastidieux s'il y a plusieurs modifications de la fonction.

!!! tips "Documentation"

    De plus, la présence d'un jeu de tests contribue à la **documentation** de la fonction.

### Fonction de test

!!! note 
    On peut présenter les tests à l'intérieur d'une fonction.
    
???+ question ""

    **Compléter** le jeu de tests puis **exécuter** le script suivant.

    {{ IDEv('scripts/valeurabsolue2test')}}

    Si un test échoue, une erreur survient.


???+ question ""

    **Compléter** le jeu de tests puis **exécuter** le script suivant.

    {{ IDEv('scripts/valeurabsolue1test')}}

    Si tous les tests réussissent, le message s'affiche.


## Module `Doctest`

!!! abstract "Doctest"

    **doctest** est un module inclus dans la bibliothèque standard du langage de programmation Python qui permet de générer facilement des tests.

    Cela consiste à intégrer le jeu de tests à l'intérieur du **doctring** de la fonction.

???+ question "Tester avec les doctests"
    
    **Compléter** le jeu de tests puis **exécuter** le script suivant.


    {{ IDEv('scripts/valeurabsolue1doctest')}}

    Une fois le script exécuté, la fonction se trouve placée en mémoire.

    Dans la console, **entrer** les deux lignes suivantes :

    ```pycon
    >>> import doctest
    >>> doctest.testmod()
    ```
    ??? soluce "Résultat"

        Le module doctest le nombre de tests effectués et le nombres de tests réussis.

???+ question "Tester avec les doctests"
    
    **Compléter** le jeu de tests puis **exécuter** le script suivant.
    
    {{ IDEv('scripts/valeurabsolue2doctest')}}
    Une fois le script exécuté, la fonction se trouve placée en mémoire. Dans la console, **entrer** les deux lignes suivantes :

    ```pycon
    >>> import doctest
    >>> doctest.testmod()
    ```

    ??? soluce "Résultat"

        Pour le test qui a échoué, le module doctest indique quelle était la valeur attendue et la valeur effectivement renvoyée par la fonction. 