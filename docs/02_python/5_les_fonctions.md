---
author: Pierre Marquestaut
title: Les fonctions
---

## 1. Les fonctions



!!! example "Exemple"

    Le code suivant utilise plusieurs fonctions : `forward`, `left`, `hideturtle` et `done`.
 
    {{ IDE('scripts/tortue1_2') }}


    <div id="cible_1" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>

    Ainsi, la ligne `forward(120)` réalise l'**appel** de la fonction `forward` avec $100$ pour **argument** : cette instruction entraine le déplacement de la tortue de $100$ pixels.

    ![forward](img/forward.svg)


!!! abstract "Définition"

    Une **fonction** est un ensemble d’instructions qui est exécuté chaque fois que l’on appelle cette fonction.

    Un fonction renvoie une valeur résultat qui dépend des arguments passés lors de l'appel.

    ![procedure](img/fonction2.svg)

    Une fonction est interprétée comme une **expression**.

    ```pycon
    >>> 2 + double(5)
    12
    ```

!!! abstract "Procédure"

    Une **procédure** est une fonction qui ne renvoie pas de résultat.

    ![procedure](img/procedure.svg)

    Une procédure est interprétée comme une **instruction**.

!!! note "Intérêt"

    Il s'agit souvent d'un petit bout de programme destiné à accomplir une action particulière comme afficher une phrase, calculer une moyenne, tracer un carré, lire un fichier, etc...

    En regroupant le code dans une fonction on évite des répétitions inutiles tout en facilitant la compréhension et la maintenance générale du programme.

!!! abstract "Appel d'une fonction"
    L'**appel** d'une fonction entraine l'exécution des instructions associées à cette fonction.

    On peut appeler une même fonction autant de fois que souhaités.

## 2. Définition d'une nouvelle fonction

On souhaite pouvoir utiliser une procédure `carré` qui dessine un carré de $100$ pixels de côtés.

!!! example "Exercice"

    Selon vous, que fait le code suivant ?
    {{ IDE('scripts/turtle3') }}


    <div id="cible_3" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>

??? bug "Solution"

    Le code produit une erreur !!

    En effet, l'interpréteur Python ne connaît pas la fonction `carre`. Elle ne fait pas parti de son vocabulaire.

    Pour utiliser une fonction, il faut que l'interpréteur connaîsse sa **définition**.

!!! abstract "Définition d'une fonction"
    La définition d'une fonction est introduite par le mot clé `def`. 

    Un bloc d'instructions constitue le corps de la fonction.

!!! example "Exercice"

    Remplacer les `...` par les instructions nécessaires pour pouvoir tracer un carré.
    {{ IDE('scripts/turtle4') }}


    <div id="cible_4" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>

    Vérifier le bon comportement de ce code.


!!! warning "Fin de la procédure"
    Une procédure se termine quand toutes les instructions de la procédure ont été interprétées. 


## 3. Paramètres d'une fonction

On a vu précédement que l'on peut déplacer plus ou moins la tortue en modifiant la valeur passée en argumant de la fonction `forward`.
De la même façon, on souhaite que notre procédure `carre` puisse dessiner des carrés de différentes longueurs.

Pour ce faire, on va ajouter des paramètres à la définition de la fonction.


!!! note "Paramètres"

    Les **paramètres** sont des variables associées à la fonction.

    Les paramètres sont initialisés par les arguments passés lors de l'appel de la fonction.

    Dans l'exemple ci-dessous, l'appel `carre(20)` initialise le paramètre `cote` de la fonction `carre` avec la valeur $20$.

!!! warning "Nombre d'arguments"
    Il est donc nécessaire qu'il y ait autant d'arguments que de paramètres.

!!! example "Exercice"

    On a rajouté à la définition de notre procedure carré un **paramètre** `cote`
    
    Remplacer les `...` par les instructions nécessaires pour pouvoir tracer un carré de longueur `cote`.
    {{ IDE('scripts/turtle5') }}


    <div id="cible_5" class="admonition center" style="display: flex;justify-content: center;align-content:center;flex-direction: column;margin:auto;min-height:5em;text-align:center">
    Le  tracé sera affiché ici
    </div>

    Vérifier le bon comportement de ce code.

 
## 4. Valeur de retour

!!! note "Résultat"
    La *valeur de retour* est l'expression renvoyée par la fonction après son exécution.

    Le mot-clé `return` permet de renvoyer le résultat.

!!! example "Exemple"


    ```python
    def triple(x):
        return 3 * x
    ```

    La fonction renvoie le résultat du calcul de `3` fois `x`.


!!! warning "Fin de la fonction"
    Si l'interpréteur rencontre le mot clé `return`, la fonction se termine, même s'il reste des instructions après le `return`.

    ```python
    def triple(x):
        return 3 * x
        print(3 * x)
    ```

    Dans l'exemple ci-dessus, la ligne `print(3 * x)` ne sera pas prise en compte par l'interpréteur, car après le `return`


## Questions


<iframe id="vocabulaire"
    title="Vocabulaire sur les fonctions"
    width="100%"
    height="550"
    src="{{ page.canonical_url }}../activites/voc fonction.html">
</iframe>


## Exercice

[Exercices sur les fonctions](https://codex.forge.apps.education.fr/exercices/fonctions/){ .md-button }