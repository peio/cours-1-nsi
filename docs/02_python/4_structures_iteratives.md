---
author: Pierre Marquestaut
title: Les structures itératives
---

# Les structures itératives


La principale caractéristique d'un ordinateur est d'exceller dans les opérations répétitives.
Au lieu d'écrire plusieurs fois (qui peut être des centaines, voire des milliers de fois) une même instruction, il est préférable d'utiliser une **structure itérative** ou **boucle**.


## La structure REPETER

![loop](img/loop.svg)

La structure itérative la plus simple consiste à répéter une même instruction un nombre de fois donnée.

!!! note 

    Voici comment écrire une telle structure en différents langages
    
    === "en Python"
        
        ```python
        instructions A
        for _ in range(n):
            instructions B

        instructions C
        ```

    === "en pseudocode"
        
        ```pseudocode
        Bloc A
        REPETER n FOIS 
            Bloc B
        FIN REPETER
        Bloc C
        ```
    === "en LUA"
        
        ```code
        instructions A
        for count = 1, n do
            instructions B
        end
        instructions C
        ```

    === "en LOGO"
        
        ```code
        instructions A
        repeat n [instructions B] 
        instructions C
        ```



???+ question 
    On exécute le code suivant :
    ```python
    a = 4
    b = 2
    for _ in range(4):
        a = a + 3
        b = b * 2
    ```
    **Donner** la valeur contenue dans les variables `a` et `b` à la fin de l'exécution.
    <iframe src="https://www.codepuzzle.io/IDYNUM" width="100%" height="400" frameborder="0"></iframe>

???+ question 
    Sofia a commis deux erreurs de syntaxe en écrivant son code.

    Saurez-vous les retrouver ?
    {{ IDE('scripts/erreur') }}


???+ question 
    La fonction `carre_aleatoire` permet de dessiner un carré aux dimensions aléatoires.
    ``````
    **Modifier** le programme pour qu'il dessine 5 carrés aléatoires, et ce avec le minimum de lignes possibles (moins de 8 lignes).
    <p style="border:solid 1px black;margin:auto;" id="fig_geometry1">Votre image sera ici</p>
    {{ IDE('scripts/repeter') }}
    


## la structure POUR CHAQUE

### Les itérables

!!! note "Rappels"
    Quand on souhaite stocker une données (voire plusieurs données) en mémoire, on associe sa valeur à un type et une identité.

    Ces différents renseignements sont rassemblés dans un **objet**.
    
    Une **variable** est une étiquette associée à un objet donné.

En informatique, certaines objets sont dites **itérables** : 

- les *tableaux*, comme par exemple `[4, 3, 17]`  qui est composé des trois entiers   `4`, `3` et `17` ;
- les *tuples*, comme par exemple `("Pierre", 43, "Biarritz")`  qui est composé des éléments   `"Pierre"`, `43` et `"Biarritz"` ;
- les *dictionnaires*, comme  `{"bleu":1 ; "rouge":2}` qui possède deux clés `"bleu"`  et `"rouge"` ;
- les *chaînes de caractères*, ainsi `NSI` est composée des trois caractères `N`,  `S` et `I`.
- les *intervalles*, issus de la fonction `range`

Les objets itérables contiennent généralement plusieurs éléments, mais peuvent aussi en contenir qu'une seule ou aucune. 

En revanche, les objets de type *entier*, *flottant* ou *booléen* possèdent toujours qu'un seul élément et sont donc **non itérables**.


### Parcourir les éléments d'un itérable. 

Nous avons vu qu'il est possible d'exécuter plusieurs fois une même instruction ou suite d'instructions.

On peut également vouloir appliquer un traitement à chaque élément d'une donnée itérable.

!!! note 

    Voici comment écrire une telle structure en différents langages
    
    === "en Python"
        
        ```python
        instructions A
        for i in donnees:
            instructions B

        instructions C
        ```

    === "en pseudocode"
        
        ```pseudocode
        Bloc A
        POUR CHAQUE element DE donnees
                Bloc B
        FIN POUR
        Bloc C
        ```


    === "en ADA"
        
        ```code
        instructions A
        for element of donnees loop
            instructions B
        end loop;
        instructions C
        ```

    === "en PHP"
        
        ```code
        instructions A
        foreach ($donnees as $element) {
            instructions B
        }
        instructions C
        ```


???+ question 

    Qu'affiche le programme suivant :
    ```python linenums='1'
    for k in 'NSI':
        print(k)
    ```
    
    === "Cocher la réponse correcte"
        
        - [ ] Trois fois `NSI`
        - [ ] Rien
        - [ ] Tour à tour les 3 lettres : `N`, `S`, `I`
        - [ ] Trois fois la lettre `N`

    === "Solution"
        
        - :x: 
        - :x: 
        - :white_check_mark: La variable `k` prend donc **successivement** toutes les lettre de la chaîne de caractères `"NSI"`. Pour chaque valeur de `k`, on exécute l'instruction `print(k)`, donc chaque lettre de ```"NSI"``` s'affiche l'une après l'autre.
        - :x: 


???+ question 

    Qu'affiche le programme suivant :
    ```python linenums='1'
    for m in 'NASA':
        print("bonjour")
    ```
    
    === "Cocher la réponse correcte"
        
        - [ ] Quatre fois `NASA`
        - [ ] Quatre fois `bonjour`
        - [ ] Tour à tour les 4 lettres : `N`, `A`, `S`, `A`
        - [ ] Trois fois la lettre `N`

    === "Solution"
        
        - :x: 
        - :white_check_mark: la **variable de boucle** `m` est **muette** : elle n'apparaît dans les instructions indentées sous le `for`.  Elle prend successivement les valeurs `'N`, `'A'`, `'S'` et `'A'`, mais on ne le voit pas.
        - :x: 
        - :x:



???+ question 

    **Compléter** le programme pour qu'il affiche le texte suivant :
    ```pycon
    je vais au lycée le lundi
    je vais au lycée le mardi
    je vais au lycée le mercredi
    je vais au lycée le jeudi
    je vais au lycée le vendredi
    ```
    {{ IDE('scripts/semaine') }}

!!! warning "Attention"
    Très souvent, la donnée itérable que la boucle va parcourir aura été *au préalable* stockée dans une variable.

???+ question 

    **Compléter** le programme pour qu'il affiche le même texte qu'à l'exercice précédent :
    {{ IDE('scripts/semaine2') }}

Notez l'importance d'avoir choisi des noms de variables explicites : ils aident grandement à la lisibilité du code.

???+ question 
    La fonction `carre` permet de dessiner un carré à la dimension donnée : `carre(2)` dessine un carré de 2 unités de coté.

    **Modifier** le programme pour qu'il dessine 4 carrés, de côté 1, 3, 5 et 7, et ce avec le minimum de lignes possibles (moins de 6 lignes).
    <p style="border:solid 1px black;margin:auto;" id="fig_geometry2">Votre image sera ici</p>
    {{ IDE('scripts/repeter2') }}

 


## la boucle POUR i ALLANT DE début A fin

Pour certains algorithme, comme les algorithmes de tri, il est nécessaire de faire varier une varier un variable à l'intérieur d'un intervalle donné.

!!! note 

    Voici comment écrire une telle structure en différents langages
    
    === "en Python"
        
        ```python
        instructions A
        for i in range(début, fin + 1):
            instructions B

        instructions C
        ```

    === "en pseudocode"
        
        ```pseudocode
        Bloc A
        POUR i ALLANT début A fin
            Bloc B
        FIN POUR
        Bloc C
        ```
    === "en C"
        
        ```pseudocode
        instructions A
        for (i = debut; i < fin + 1; i++){
            instructions B
        }            
        instructions C
        ```
    === "en Pascal"
        
        ```pseudocode
        instructions A
        for a := debut to fin do
        begin
            instructions B
        end          
        instructions C
        ```

!!! note 
    En Python, la variable prendra la valeur `début` à la première itération (ou première passage dans la boucle), puis sera incrémenté à chaque itération, mais ne prendra pas la dernière pas la valeur `fin + 1` qui est exclue de l'intervalle.

???+ question 

    On propose le programme suivant :
    ```python linenums='1'
    for i in range(2,5):
        print(i)
    ```
    Quelles sont les affirmations exactes ?

    === "Cocher les réponses correctes"
        
        - [ ] L'instruction `print`sera exécuter trois fois.
        - [ ] La première valeur affichée sera `3`.
        - [ ] La dernière valeur affichée sera `5`.
        - [ ] A la fin la valeur de `i`est `4`.

    === "Solution"
        
        - :white_check_mark: Elle sera exécutée pour les valeurs `2`, `3` et `4`
        - :x:  A la première itération,`i`prendra la première valeur, à savoir `2`
        - :x: La valeur `5`est exclue de l'intervalle et ne sera pas affichée.
        - :white_check_mark: `5`étant exclue, la dernière valeur de `i` et donc la dernière valeur affichée est `4`.
        
        


!!! faq 
    L'exercice suivant permet la maîtrise de la fonction `range`.
    
    [Autour du range](https://e-nsi.gitlab.io/pratique/N0/100-range/sujet/){ .md-button target="_blank" rel="noopener"}

???+ question 
    La fonction `carre` permet de dessiner un carré à la dimension donnée : `carre(2)` dessine un carré de 2 unités de coté.

    **Modifier** le programme pour qu'il dessine 7 carrés, de côté 1 à 7, et ce avec le minimum de lignes possibles (moins de 8 lignes).
    <p style="border:solid 1px black;margin:auto;" id="fig_geometry3">Votre image sera ici</p>
    {{ IDE('scripts/repeter3') }}
    

