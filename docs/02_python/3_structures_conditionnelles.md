---
author: Pierre Marquestaut
title: Les structures conditionnelles
---

# Les structures conditionnelles

Les **conditions** nous permettent de rendre nos programmes adaptatifs en leur donnant la possibilité de se comporter différemment selon qu'une certaine condition est réalisée ou pas.


## Structure conditionnelle de base : *si ... alors ... sinon ...*

![SiAlorsSinon.PNG](img/index.png)

!!! note 

    Voici comment écrire une telle structure en différents langages
    
    === "en Python"
        
        ```python
        instructions A
        if condition :
            instructions B
        else:
            instructions C
        instructions D
        ```

    === "en pseudocode"
        
        ```pseudocode
        instructions A
        SI condition vraie ALORS
            instructions B
        SINON
            instructions C
        FIN SI
        instructions D

        ```
    === "en Javascript"
        
        ```python
        instructions A
        if (condition) {
            instructions B
        }
        else {
            instructions C
        }
        instructions D
        ```

    === "en Basic"
        
        ```python
        instructions A
        IF (condition) THEN instructions B
        ELSE instructions C

        instructions D
        ```

## Structure conditionnelle de base : *si ... alors ...*

La clause `else` est facultative. On obtient alors la structure suivante :

!!! note 

    Voici comment écrire une telle structure en différents langages
    
    === "en Python"
        
        ```python
        instructions A
        if condition :
            instructions B

        instructions C
        ```

    === "en pseudocode"
        
        ```pseudocode

        Bloc A
        Si condition vraie faire :
            Bloc B
        Bloc C
        ```
    === "en COBOL"
    
        ```pseudocode

        instructions A
        IF condition 
            instructions B
        END-IF
        instructions C
        ```

Ainsi, si la condition est vraie, on effectuera les blocs A-B-C.<br>
Si la condition n'est pas vraie, on effectuera les blocs A-C

???+ question 
    On exécute le code suivant :
    ```python
    a = 4
    b = 3
    if b > 5:
        a = 2
    b = 7
    ```
    **Donner** la valeur contenue dans les variables `a` et `b` à la fin de l'exécution.
    <iframe src="https://www.codepuzzle.io/IDM4Y8" width="100%" height="450" frameborder="0"></iframe>

???+ question 
    **Reconstituer** la fonction `est_majeur` qui prend un entier `age` en paramètre et qui renvoie une chaîne de caractère qui vaut "majeur" si l'âge est supérieur à 18, et "mineur" sinon.
    <iframe src="https://www.codepuzzle.io/IPE6JN" width="100%" height="350" frameborder="0"></iframe>

???+ question 
    Ethan a commis 3 erreurs de syntaxe en codant son programme.
    Saurez-vous les corriger ?
    {{ IDE('scripts/corrige') }}

???+ question 
    **Écrire** la fonction `mystère` qui met en œuvre le pseudocode suivant :

    ```pseudocode
    FONCTiON mystere(a, b, c)
        SI a = b ALORS
            k <- 5
        SINON
            k <- 3
        FIN SI
        RENVOYER k * c
    FIN FONCTION
    ```
    {{ IDE('scripts/algo') }}
    
???+ question 
    Il faut mesurer au moins 1m30 pour pouvoir entrer dans un parc d'attractions.

    **Écrire** une fonction `peut_participer` qui prend une taille en cm, et qui renvoie une chaine de caractères qui indique s'il `"oui"` ou `"non"` il peut rentrer dans le parc.
    {{ IDE('scripts/parc') }}


##  Les conditions multiples

On a parfois besoin de tester plusieurs conditions successivement.


!!! note 

    Voici comment écrire une telle structure en différents langages
    
    === "en Python"
        
        ```python
        instructions A
        if condition1:
            instructions B
        elif condition2:
            instructions C
        else:
            instructions D
        instructions E
        ```

    === "en pseudocode"
        
        ```pseudocode
        instructions A
        SI condition1 vraie ALORS 
            instructions B
        SINON SI condition2 vraie ALORS 
            instructions C
        SINON
            instructions D
        FIN SI
        instructions E
        ```
        
    === "en C++"
        
        ```python
        instructions A
        if condition1 {
            instructions B
        }
        else if condition2 {
            instructions C
        }
        else {
            instructions D
        }
        instructions E
        ```


En Python, on dispose d'une syntaxe plus lègère qui permet de clarifier un peu le programme par rapport à d'autres langage : on peut condenser le `else if` en une seule terme `elif`.

La clause `else` est facultative.


A savoir que la `condition2` est évaluée que si la `condition1` est fausse.

???+ question 
    On exécute le code suivant :
    ```python
    a = 4
    b = 3
    if b < 5:
        a = 2
    elif a == 2:
        b = 4
    elif b < 4:
        a = 5
    else:
        b = 7
    ```
    **Donner** la valeur contenue dans les variables `a` et `b` à la fin de l'exécution.
    <iframe src="https://www.codepuzzle.io/D8279" width="100%" height="550" frameborder="0"></iframe>

???+ question 
    **Écrire** une fonction `bissextile` qui prend une année  en paramètre et qui renvoie s'il s'agit d'une année bissextile.
    Une année est bissextile si elle est multiple de 4 mais pas multiple de 100, ou si elle est multiple de 400.
    <iframe src="https://www.codepuzzle.io/IP8E4F" width="100%" height="450" frameborder="0"></iframe>

???+ question 
    Dominique a commis 2 erreurs en codant sont programme.
    Saurez vous les corriger ?
    {{ IDE('scripts/corrige2') }}

???+ question 
    **Écrire** la fonction `mystère` qui met en œuvre le pseudocode suivant :

    ```pseudocode
    FONCTiON mystere(a, b, c)
        SI a = b ALORS
            k <- 5
        SINON SI c > 5 ALORS
            k <- 3
        SINON 
            k <- 10
        FIN SI
        RENVOYER k * c
    FIN FONCTION
    ```
    {{ IDE('scripts/algo2') }}

???+ question 
    **Écrire** une fonction `mention_bac` qui prend en paramètre une moyenne du bac et qui renvoie la mention.
    <table>
    <tr>
        <td style="border:1px solid"><strong>Moyenne</strong></td><td style="border:1px solid; text-align: center">$[0;8[$</td><td style="border:1px solid; text-align: center">$[8;10[$</td><td style="border:1px solid; text-align: center">$[10;12[$</td>
        <td style="border:1px solid; text-align: center">$[12;14[$</td><td style="border:1px solid; text-align: center">$[14;16[$</td><td style="border:1px solid; text-align: center">$[16;18[$</td><td style="border:1px solid; text-align: center">$[18;20]$</td>
    </tr>
    <tr>
        <td style="border:1px solid"><strong>Mention</strong></td><td style="border:1px solid;text-align: center">Recalé</td><td style="border:1px solid;text-align: center">Rattrapage</td><td style="border:1px solid; text-align: center">Sans mention</td><td style="border:1px solid; text-align: center">Assez bien</td>
        <td style="border:1px solid; text-align: center">Bien</td><td style="border:1px solid;text-align: center">Très bien</td><td style="border:1px solid; text-align: center">Félicitations</td>
    </tr>
    </table>
    {{ IDE('scripts/mention') }}




