---
authors: Gilles Lassus, Pierre Marquestaut
title: L'architecture Von Neumann
---

# L'architecture Von Neumann

[Un peu d'Histoire](https://ladigitale.dev/digiquiz/q/6554791933b63/){ .md-button target="_blank" rel="noopener"}
<iframe src="https://ladigitale.dev/digiquiz/q/6554791933b63" allow="fullscreen; autoplay;" frameborder="0" width="100%" height="500"></iframe>


## I. Modèle de Von Neumann


!!! abstract "Principe"

    L'architecture de Von Neumann repose sur le principe qu'il est possible de considérer un programme informatique comme **données**.

!!! abstract "Architecture"

    Dans l'architecture de von Neumann, un ordinateur est constitué de quatre parties distinctes :

    * Le  **CPU** : Central Processing Unit (unité centrale de traitement) appelé aussi processeur ;
    * la  **mémoire**  où sont stockés les données et les programmes ;
    * Des **bus** qui sont des fils conduisant des impulsions électriques et qui relient les différents composants ;
    * des **entrées-sorties** (E/S ou I/O input/Output) pour échanger avec l'extérieur.

### I.1 Le processeur

!!! abstract "CPU"
    Les instructions qui composent les programmes sont exécutées par le **processeur** ou **CPU** (Central Processing Unit). Il est schématiquement constitué de 3 parties.

      1. L’unité arithmétique et logique (UAL ou ALU en anglais) est chargée de l ’ exécution de tous les calculs que peut réaliser le microprocesseur. Nous allons retrouver dans cette UAL des circuits comme l’additionneur.
      2. L ’unité de contrôle est chargée du séquençage des opérations : elle permet d’exécuter les instructions (les programmes).
      3. Une toute petite quantité de mémoire appelée les registres, qui permettent de m émoriser de l’information transitoirement pour opérer dessus ou avec.


<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/modele vonneumann.html"
        width="900" height="500"
        id="modele"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>

!!! note "Exécution des instructions"

    ![suite](images/instructions.jpg){ width=10%; align=right }

    un processeur exécute les instructions qui composent un programme selon le principe ci-contre.

!!! abstract "vitesse de traitement"
    La vitesse de traitement d’un processeur s’exprime en **IPS** (Instructions Par Seconde) ou en **FLOPS** (opérations à virgule flottante par seconde).

!!! abstract "Transistor"

    Un processeur est constitué de plusieurs millions de transistors. 

!!! abstract "loi de Moore"

    Énoncée en 1965, la loi de Moore prédit que le nombre des transistors dans un microprocesseur double tous les 2 ans
    ![https://commons.wikimedia.org/wiki/File:Loi_de_Moore.png](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a4/Loi_de_Moore.png/512px-Loi_de_Moore.png){: .center}
    
    The original uploader was QcRef87 at French Wikipedia., [CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/), via Wikimedia Commons

!!! note "Parallélisme"
    Depuis les années 2000, les fondeurs de processeurs ne sont plus en mesure d'augmenter le nombre de processeurs à l'intérieur d'un même processeur.

    La solution retenu est de faire travailler plusieurs processeurs en parallèles.

### I.2 Unité Arithmétique et Logique

!!! abstract "Unité Arithmétique et Logique"
    Une unité arithmétique et logique, ou ALU, est un circuit qui peut réaliser plusieurs opérations et permet de sélectionner, via un ou plusieurs bits de contrôle, l’opération qui est réalisée.

    Les opérations proposées sont, comme le nom de l’ALU indique, des opérations arithmétiques (typiquement, l’addition et la soustraction) et des opérations logiques (par exemple, un ET et un OU logiques).

!!! example "UAL simplifié"

    Cette ALU sait effectuer l’addition ou la soustraction de deux nombres entiers représentés sur 4 bits.

    Elle a ainsi 8 bits d’entrée pour les données et 4 bits de sorties, à gauche et à droite.

    En plus de l’addition et de la soustraction, elle sait aussi faire les opérations logiques ET et OU — en tout donc, quatre opérations. Pour sélectionner l’une des quatre opérations, on ne peut plus se contenter d’un seul bit de contrôle, mais nous allons en utiliser deux pour avoir quatre combinaisons possibles. Ce sont les deux entrées supérieures gauches de l’ALU (on ignore ici l’entrée $Cin$).

La convention utilisée pour la sélection de l’opération est la suivante :

|$Op$| Opération éffecuée|
|----|-------------------|
|00  |Addition           |
|01  |Soustraction       |
|10  |Porte OU           |
|11  |Porte ET           |


!!! question "A vous de jouer"

    **Connecter** cette ALU à 8 entrées et à 4 sorties de manière à lui faire effectuer l’opération $7+2=9$.

    **Connecter** ensuite une entrée pour chaque bit de contrôle qui permettra d’effectuer la soustraction avec les mêmes données d’entrée, donc $7-2=5$ .

    <script src=" https://logic.modulo-info.ch/simulator/lib/bundle.js "></script>
    <div style=" width: 100%; height: 400px ">
    <logic-editor showonly=" in in.nibble out out.nibble out.nibble-display ">
        <script type=" application/json ">
        {
            " v": 4, "components" : [ {"type": "alu" , "pos" : [300, 200], "in" : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 16], "out" : [10, 11, 12, 13, 1000, 15, 14]} ] } </script>
                </logic-editor>
    </div>

!!! abstract "Drapeaux"

    L’ALU a deux sorties en plus (appelés **drapeaux** ou **flags**), en bas du composant :

    * la sortie $V$ (pour *oVerflow*) vaut $1$ lors d’un dépassement de capacité.

    * la sortie  $Z$ (pour *Zero*) vaut $1$ lorsque tous les bits de sortie valent $0$.

!!! question "A vous de faire"
    **Réaliser** l'opération $9+9=18$ et vérifier s'il y a un dépassement grâce au drapeau.

    **Réaliser** l'opération $9-9=0$ et vérifier que le drapeau Zéro est activé.

### I.3 Les Bus informatique

!!! abstract "Bus informatique"
    Un bus informatique est un dispositif de transmission de données partagé entre plusieurs composants d'un système numérique.

    * Le **bus d’adresse** permet de faire circuler des adresses (par exemple l’adresse d’une donnée à aller chercher en mémoire).
    * Le **bus de données** permet de faire circuler des données.
    * Le **bus de contrôle** permet de spécifier le type d’action (exemples : écriture ou lecture d’une donnée en mémoire).

     ![bus](images/bus.jpg)

## II. Les constituants d'un ordinateur

### II.1 Composition d'un ordinateur

!!! note "Différents composants"

    Un ordinateur moderne est conçu sur le modèle de Von Neumann. 

    On retrouve parmi ses constituants les mêmes éléments que dans le modèle.

<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/role_composant.html"
        width="900" height="300"
        id="quizz1"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>

On peut situer ces différents constituants à l'intérieur de l'ordinateur.

<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/Composant ordinateu.html"
        width="100%" height="910"
        id="quizz2"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>


<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/contenu ordinateur.html"
        width="100%" height="780"
        id="quizz3"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>

### II.2 La mémoire

!!! abstract "Volatile ou permanente"

    La mémoire se divise entre *mémoire volatile* (RAM ou mémoire vive) qui gère les programmes et données en cours de fonctionnement et *mémoire permanente* qui gère les programmes et données de base de la machine.

<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/memoire.html"
        width="900" height="620"
        id="quizz1"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>

!!! note "Conclusion"

    On remarque que plus une mémoire est rapide, moins elle peut contenir de données. 

    Ainsi les registres servent au fonctionnement du processeur et disposent d'une vitesse d'accès très rapide. Au contraire, les mémoires de stockage proposent un accès aux données beaucoup plus lent mais permettent d'en stocker une grande quantité.

### II.3 Les entrées-sorties

Les **entrées-sorties** permettent l'échange de donnes entre l'unité de calcul et les **périphériques**, dispositifs qui ajoutent des fonctionnalités (affichage, stockage, impression...) à l'ordinateur.


<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/ES.html"
        width="900" height="4500"
        id="ES"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>