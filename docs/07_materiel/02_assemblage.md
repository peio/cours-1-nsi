---
authors: Gilles Lassus, Pierre Marquestaut
title: Langage d'assemblage
---

# Langage d'assemblage



## Le simulateur

Nous allons travailler sur un simulateur de CPU présent sur [cette page](http://www.peterhigginson.co.uk/AQA/){ :target="_blank" }.

!!! note "Présentation"

    Il est relativement facile de distinguer les différentes parties du simulateur :

    * à droite, la mémoire vive ou RAM (Main Memory) ;
    * au centre, le microprocesseur (avec ses différentes composantes : ALU, CU, registres, …) ;
    * à gauche, la zone d’édition (Assembly Language), permettant de saisir des programmes en assembleur.

    ![presentation](images/drawit-diagram-13-1024x773.png)

 
!!! note "La RAM"

    Le contenu des différentes cellules de la mémoire peut être affiché dans différents formats :

    On peut modifier le format d’affichage de la mémoire à l’aide du bouton **OPTIONS** situé en bas dans la partie gauche du simulateur.

!!! question "A faire"

    À l’aide du bouton **OPTIONS**, passer à un affichage en *binaire*.

 

 
!!! note "Format"

    Chaque cellule de la mémoire est accessible par son adresse. Il existe deux formats d’adressage des cellules de la mémoire :

    * 32 bits – format mot (option word mode) – par féfaut
    les adresses vont de 000 à 199 (codée en base 10).
    * 8 bits – format octet (option byte mode)
    les adresses vont de 000 à 799 (codée en base 10).

!!! question "A faire"

    On peut modifier le format d’adressage de la mémoire à l’aide du bouton OPTIONS.
    
    **Régler** la mémoire de sorte d’avoir un affichage hexadécimal, avec des cellules au format 8 bits (*byte mode*).

 

 

 
!!! note "Le CPU"

    Dans la partie centrale du simulateur, on trouve les différent composants du microprocesseur :

    * les registres (Registers) : 13 registres (R0 à R12)
    1 registre spécial (PC) qui contient l’adresse mémoire de l’instruction en cours d’exécution ;

    ![presentation](images/AQA_RAM_reg.png)

    * l’unité de contrôle (Control Unit) qui contient l’instruction machine en cours d’exécution (au format hexadécimal)

    ![presentation](images/AQA_RAM_CU.png)

    * l’unité arithmétique et logique (Arithmetic and Logic Unit)
    
    ![presentation](images/AQA_RAM_ALU.png)

 
## Programmer en assembleur

La zone d’édition (Assembly Language) permet de saisir des programmes en assembleur.

???+ question "Premier programme"
    Le programme Python :
    ```python
    a = 42
    ```

    peut être traduit par le programme assembleur suivant :

    ```code
    MOV R0,#42
    STR R0,150
    HALT
    ```
    Dans la zone d’édition (Assembly Language) saisir les lignes de code précédentes puis cliquer sur le bouton **Submit**.

    Le programme vient d’être mis dans les cellules mémoires d’adresses 000, 001 et 002 :

    L’assembleur a converti les 3 lignes du programme en instructions machines, chacune occupant une cellule de 32 bits :

    * *adresse 000 :* MOV R0,#42
    * *adresse 001 :* STR R0,150
    * *adresse 002 :* HALT

???+ question "Deuxième programme"
    Le programme Python :
    ```python
    a = 2
    b = a + 1
    ```

    peut être traduit par le programme assembleur suivant :

    ```code
    MOV R0, #2
    STR R0, 150
    ADD R0, R0, #1
    STR R0, 166
    HALT
    ```

    Dans la zone d’édition (Assembly Language) saisir les lignes de code précédentes puis cliquer sur le bouton **Submit**.

    **Modifier** le programme précédent pour traduire le programme suivant :

    ```python
    a = 2
    b = 3
    c = a + b
    ```

??? soluce "Solution"

    Le programme peut être traduit par :

    ```code
    MOV R0, #2
    MOV R1, #3
    STR R0, 150
    STR R1, 158
    ADD R0, R0, R1
    STR R0, 166
    HALT
    ```

???+ question "Erreur du programme"

    Le programme suivant génère une erreur :
    ```
    MOV R0,#42
    STR R0, 8
    STR R0,150
    HALT
    ```
    **Imaginer** la raison de l'erreur produite puis **tester** le propramme pour le vérifier

??? note "Conclusion"
    Il est possible que des données puissent être écrite dans une zone réservée au programme, modifiant ainsi ce dernier et créant une erreur.

    C'est une erreur fréquente sur des cartes microcontrôleur où la mémoire disponible est limitée.

<iframe src="http://www.peterhigginson.co.uk/AQA/" width="100%" height="520" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
