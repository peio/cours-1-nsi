---
author: Dominique Filoé, Laurent BONNAUD et Mireille Coilhac
title: Réseau
---


### 1. Un bref aperçu : Les différentes couches du modèle TCP/IP

<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQinchAQTLob6l4W-pOiWdj15qHcj5CDQKSSIG_xiLBuxeiMEgmq-u_Z_l4LlKe4Q/embed?start=false&loop=false&delayms=3000" frameborder="0" width="1280" height="749" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

👉 Pour visualiser ce diaporama, cliquer si nécessaire sur le numéro de la diapositive, et se positionner sur la première

<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/osi.html"
        width="900" height="500"
        id="osi"
        title="osi" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>


<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/protocoles_reseau.html"
        width="900" height="500"
        id="reseau"
        title="reseau" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>

<div class="centre" markdown="span">
    <iframe 
        src="{{ page.canonical_url }}../activites/protocoles.html"
        width="900" height="500"
        id="protocole"
        title="protocole" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>





### 3. Les réseaux 

Il existe énormément de réseaux dans le monde (les ordinateurs du lycée sont, par exemple, connectés en **« réseau »**).

![](images/wan.png){ width=25%; : .center }

Certains réseaux sont reliés à d'autres réseaux ce qui forme **des réseaux de réseaux**.

Voici un exemple de **réseau informatique** :

![](images/constituant.png){ width=70%; : .center }

Les équipements présents sur ce réseau sont :

!!! note "**ordinateur**, **imprimante réseau**, **serveur**, ..."
    
    ![](images/ordimpserv.png){ width=50%; : .center }

    Ces équipements disposent d'une **carte réseau** (interface Ethernet ou interface Wifi) afin de se **connecter matériellement au réseau** :

    ![](images/interface.png){ width=50%; : .center }
    
    Ces équipements possèdent un **identifiant unique** nommé **adresse IP** et peuvent héberger certains **services** réseaux :

    - stockage de pages web ;
    - serveur d'impression ;
    - stockage de fichiers ;
    - ...

!!! note "Switch ou commutateur"
    ![](images/constituant.png){ width=30%; : align=right }

    ![](images/switch.png){ width=20% }

    Le switch (ou commutateur) **relie les équipements, d'un même réseau, entre eux** via des câbles Ethernet et **achemine les données sur ce réseau**.
    
    Il ne possède pas d'adresse IP mais il est capable de lire les **adresses matérielles MAC** des trames de données numériques circulant sur le réseau afin d'**aiguiller correctement l'information entre les équipements présents sur ce réseau**.  
    Il intervient donc sur la couche 2 du modèle OSI : liaison de données /DATALINK, c’est-à-dire sur la couche accès au réseau du modèle TCP/IP.

!!! note "Routeur"
    ![](images/constituant.png){ width=30%; : align=right }

    ![](images/routeur.jpg){ width=20%}

    Sur le schéma ci-dessus, le routeur possède **plusieurs adresses IP** : une adresse IP côté réseau local (LAN : Local Area Network) et une autre adresse IP côté réseau Internet (WAN : Wide Area Network).

    Il dispose ici de **deux cartes réseau** afin de se connecter aux deux réseaux.

    De façon générale, un routeur **établit la communication entre au moins deux réseaux** (il dispose donc d'au moins deux cartes réseau) et **route les données entre ces réseaux** en utilisant l'**adressage IP** (contenant le réseau de destination).

!!! note "Type de connexion"
    ![](images/constituant.png){ width=30%; : align=right }

    ![](images/cable.png){ width=20% }

    La connexion entre ces différents équipements réseaux utilise donc un **support de transmission** **filaire** (ex : câble réseau) ou **sans fil** (ex : WIFI).

!!! note ""Carte réseau et adresse MAC""
    ![](images/interface.png){ width=40% }

    Toutes les cartes réseau possèdent une **adresse matérielle MAC**, affectée par le constructeur de la carte, afin d’être **identifiée de façon unique sur le réseau auquel elle est connectée** (parmi toutes les autres cartes réseau).

## II. Adresses IP

!!! info "Adresse IP"

    &#128073; Chaque machine sur le réseau est identifiée par une adresse IP. La norme actuelle est IPv4, la migration est en cours
    vers IPv6.

    Dans la norme IPv4 l'adresse est codée sur 32 bits (4 octets), soit 4 entiers de 0 à 255 (car 255 en base 10 s'écrit 1111 1111 en base 2)

    Exemple d'adresse IP : 168.1.2.10

### 1. Masque de sous réseau

!!! info "Masque de sous réseau"

    L'adresse IP est en réalité composée de **2 parties** : l'adresse **du réseau**, et l'adresse **de la machine sur le réseau**. 


    &#128073; Le masque permet de connaitre la partie réservée à l'adresse du réseau.

!!! info "Masque de sous réseau : notations"

    Il existe deux façons de noter le masque. 

    ???+ note "Première notation possible avec l'adresse du masque"

        **&#128161; Première notation possible :** IP = 168.1.2.5   mask = 255.255.255.0 

        Dans cet exemple très simple, les valeurs 255 indiquent la partie réseau, les 0 indiquent la partie identifiant la machine sur le réseau. 

        ![](images/res_mach.png){ width=20% }

        Dans l'exemple ci-dessus 168.1.2.0 est l'adresse du réseau, seul le dernier nombre identifie une machine.

        Dans ce cas-là, toute adresse 168.1.2.x identifiera une machine sur le réseau. x peut prendre 256 valeurs, ce sous réseau peut donc 
        connecter 254 machines car il y a deux **adresses réservées** :  
        
        * 👉 adresses réservées 168.1.2.0 celle du réseau, obtenue avec uniquement des 0 sur la plage d'adressage machines
        * 👉 168.1.2.255 celle du broadcast (adresse réservée pour une diffusion sur toutes les machines du réseau) obtenue avec uniquement des 1 sur la plage d'adressage  machines.  

    ???+ note "Deuxième notation possible : notation CIDR"

        &#128073; On peut aussi noter le masque de la façon suivante : 168.1.2.0/24

        **/24** indique que 24 bits sont utilisés pour l'adresse réseau. Les 8 bits restant codent donc l'adresse machine.

        &#128512; On peut avec cette notation plus simple, utiliser une très faible quantité d'information pour l'adresse machine. Cela est
        préconisé pour un routeur, qui ne gère généralement pas un très grand nombre de machines.

        10.1.2.5/30 indiquera par exemple que l'adresse réseau est codée sur 30 bits, seulement 2 bits (4 valeurs possibles) sont utilisables pour des machines. C'est très    peu !


### 2. Exemples :

!!! example " Un exemple détaillé"

    Considérons la machine dont la configuration réseau est : IP : 172.128.10.5 ; Masque : 255.255.192.0 .  
    On écrit de façon plus courante : 172.128.10.5/18  
    On obtient l'adresse du sous réseau avec l'opérateur AND.  

    Écrivons en binaire l'adresse IP et le masque :  
    $172.128.10.5$ s'écrit en binaire :  $10101100.10000000.00001010.00000101$  
    $255.255.192.0$ s'écrit en binaire : $11111111.11111111.11000000.00000000$  

    Posons l'opération du ET logique entre l'adresse IP de la machine et le masque :  

    ![calcul de masque](images/ex_calc_masque.jpg){ width=50% .center}

    On met en décimal le résultat : $172.128.0.0$  est l'adresse du réseau.  

    La seconde partie nous permet de savoir combien de machines peut contenir ce réseau.  
    On peut aller de $000000.00000000$ à $111111.11111111$.  
    En décimal : de 0 à 16383. C'est à dire 16384 adresses possibles...  

    &#127797; Enfin pas tout à fait :

    * Il faut retirer l'adresse du réseau lui-même : $172.128.0.0$ 
    * Il faut également retirer (la dernière ) l'adresse de broadcast :  $10101100.10000000.00111111.11111111$ c'est à dire l'adresse $172.128.63.255$


    Donc en tout : on peut connecter 16382 machines dans ce réseau.

???+ question "Exercice papier"

    On donne l'adresse IP d'un matériel suivante : Adresse IP : 192.168.1.100/20  
    
    Déterminer : 

    1. le masque de sous-réseau
    2. l'adresse du réseau
    3. le nombre de machines que l'on peut connecter à ce réseau
    4. l'adresse broadcast
    5. l'adresse IP de la première machine
    6. l'adresse IP de la dernière machine

    ??? success "Solution"

        Adresse IP : 192.168.1.100/20  
        En binaire : 11000000.10101000.00000001.01100100

        1. Le masque de sous-réseau est :
        11111111.11111111.11110000.00000000  
        c'est à dire en décimal :  
        255.255.240.0  
            
        2. L'adresse du réseau :    
        11000000.10101000.00000001.01100100    
         AND  
        11111111.11111111.11110000.00000000   
        👉  
        L'adresse du réseau est donc :  
        11000000.10101000.00000000.00000000  
        c'est à dire en décimal :  
        192.168.0.0   
              
        3. Le nombre de machines que l'on peut connecter à ce réseau :  
        La dernière adresse possible pour la plage réservée aux adresses machines est 1111  11111111
        On peut donc connecter $2^{12} -2  = 4094$ machines
            
        4. L'adresse broadcast est donc  
        11000000.10101000.00001111.11111111  
        C'est à dire en décimal : 192.168.15.255 

        5. L'adresse IP de la première machine est  
        11000000.10101000.00000000.00000001  
        C'est à dire en décimal : 192.168.0.1  
              
        6. L'adresse IP de la dernière machine est   
        11000000.10101000.00001111.11111110  
        C'est à dire en décimal : 192.168.15.254  




### 3. Deux routeurs liés par un câble

!!! danger "Attention"

    &#127797; Même si entre deux routeurs, il n’y a rien d’autre qu’un câble, ce câble constitue un réseau.  
    En pratique ce réseau pourrait avoir pour masque 255.255.255.252 (en CIDR /30) .  
    Sur un tel réseau, il n’y a donc que quatre adresses possibles. Etant donné que deux sont prises pour le réseau et le broadcast, il n’en reste que deux pour connecter des matériels. Ce seront donc les deux adresses IP des routeurs, qui se termineront donc forcément par .1 et .2 (l’adresse se terminant par .0 étant celle du réseau, et celle se terminant par .3 celle du broadcast)  



## III. Le logiciel Filius

Il est matériellement compliqué de mettre en place un réseau pour effectuer des tests. À la place nous allons utiliser un simulateur de réseau relativement simple à prendre en main, mais suffisamment performant : Filius 

👉 Si le logiciel Filius n’est pas installé : le télécharger ici : 

<a target="wims_external" href="https://lernsoftware-filius.de/Herunterladen">https://lernsoftware-filius.de/Herunterladen</a>

👉  Installer.  
&#9888;&#65039; **Sélectionner Français**  
&#128557; **Après ce sera trop tard…**  
![installation](images/capture_install_Filius.png){ width=35% .center}

Le logiciel dispose de deux modes ; on passe d’un mode à l’autre en cliquant sur l’icône correspondante :

* le mode conception, activé par l’icône « marteau »  
 ![marteau](images/iconmarteau.png){ width=10% }
* le mode simulation, activé par l’icône « flèche verte »  
![simulation](images/iconsimul.png){ width=10% }


??? tip "Astuces : vérifier en cas de problèmes avec le logiciel Filius"

    * Etre en mode simulation pour ajouter lignes de commandes
    * Les lignes de commandes doivent êre installées des deux côtés pour un ping
    * Pour ping : les 2 doivent être configurés
    * DHCP ne doit pas être configuré par DHCP mais en statique. Vérifier que son IP a bien été configurée
    * Pour que DHCP fonctionne : appuyer sur la flèche verte simulation
    * Pour mettre les adresses passerelles : décocher DHCP
    * Pour reconnaître un fil, cliquer sur l’interface, cela apparaît en vert




## IV. Réseau local - Protocoles ARP et ICMP

[ARP et ICMP](http://nsi.goupill.fr/ressources/animation-reseau-simple.svg#1_0){ .md-button target="_blank" rel="noopener" }

### 1.	Comment communiquer entre deux ordinateurs ? 

😊 Nous utiliserons le logiciel **Filius**.

* En mode conception, créer un ordinateur portable seul par « glisser-déposer ». Un double-clic sur cet ordinateur permet d’accéder à sa configuration réseau. Son adresse IP par défaut est 192.168.0.10. Changer cette adresse en 192.168.1.1 puis cocher Utiliser l’adresse IP comme nom (à faire sur chaque machine à l’avenir).
Nous conservons comme masque 255.255.255.0
* Créer un second ordinateur portable. Changer son adresse IP en lui attribuant l’adresse 192.168.1.2. 
* Relier les deux ordinateurs par un câble ethernet (prise RJ45).  
![fil](images/fil.png){ width=10% .center}  
Observer qu’un câble posé peut ensuite être supprimé : clic-droit puis « supprimer ».  
* Penser à sauvegarder votre travail régulièrement dans votre dossier Documents
* Passer en mode simulation. Par un double-clic sur la première machine (192.168.1.1), ouvrir l’installateur de logiciels.  
![deux_ordis](images/deux_ordis.png){ width=40% .center}  

* Installer la ligne de commande en la faisant passer à gauche avec  
![fleche](images/fleche.png){ width=5% .center}  

⚠️ **Ne pas oublier de cliquer sur « appliquer la modification »**

* Ouvrir la ligne de commande (double-clic) et saisir l’instruction `ipconfig `.

Quelles informations nous apporte cette commande `ipconfig` ?

??? success "Solution"

    L’adresse IP, le masque de sous-réseau et l’adresse physique MAC.

* Saisir la commande `arp` à l’invite de commande.

Que donne le tableau affiché ?

??? success "Solution"

    Les adresses IP et MAC des machines connues sur le réseau.

!!! info "Adresse MAC"

    Une adresse MAC (Media Access Control), parfois nommée adresse physique ou adresse Ethernet, est un identifiant physique stocké dans une carte réseau ou une interface réseau similaire. À moins qu'elle n'ait été modifiée par l'utilisateur,  elle est unique au monde.  
    Elle est constituée de 6 octets écrits sous forme hexadécimale, chacun séparé par " : " (par exemple : 00:13:A9:58:32:EB).  
    Les 3 premiers octets définissent le constructeur de la carte réseau (ici Sony Corporation), les 3 derniers le numéro de fabrication. On peut retrouver le constructeur à partir de l’adresse MAC. 

* Effectuer un `ping` vers la machine 192.168.1.2, la commande à saisir est : `ping 192.168.1.2`.
* Si tout va bien, on observe que le câble se colore en vert le temps du transfert de données et qu’aucun paquet n’est perdu à ce stade.
* Saisir à nouveau la commande `arp` à l’invite de commande
    Qu’est ce qui a changé depuis le ping ?

??? success "Solution"

    La machine 2 est maintenant connue.

???+ note dépliée

    Lorsque l’ordinateur 192.168.1.1 fait un ping vers 192.168.1.2 (couche réseau) et qu’il passe la requête à la couche liaison de données, cette couche a besoin de l’adresse MAC pour réaliser cette requête. Or il ne la connait pas. Un échange avec le protocole ARP permet de l’obtenir. 

* Faire un clic-droit sur la machine 192.168.1.1 et afficher les échanges de données.
Sélectionner la ligne du haut et lire les explications qui s’affichent dans la fenêtre du bas.

![](images/arp.png){ width=90%; align=center}  

![](images/osi.png){ width=90%; align=center }


Source de l’image : https://cisco.goffinet.org/ccna/fondamentaux/modeles-tcp-ip-osi/  
Légendes des icônes utilisées : (source : https://www.edrawsoft.com/fr/cisco-network-topology-icons.html)  

![](images/legende.png){ width=40%}

!!! info "Les protocoles ARP et ICMP"

    * Les protocoles **ARP** et **ICMP** sont de la couche 3 du modèle OSI, c’est-à-dire de la couche internet du modèle TCP/IP
    * Le protocole **ARP** : Adresse Resolution Protocol permet de lier à une adresse logique IP une adresse physique MAC.
    * Le protocole **ICMP** : Internet Control Message Protocol permet de véhiculer des messages de contrôle et d'erreur.

👉 Nous allons étudier plus précisément les lignes de l’échange de données.

???+ question "En observant le contenu des commandes ARP"

    Expliquer comment 192.168.1.1 obtient l’adresse MAC de 192.168.1.2. En particulier à qui envoie-t-il la demande ?

    ??? success "Solution"

        * La machine 1 envoie sur le réseau (à toutes les machines : adresse de diffusion) une demande de l'adresse MAC de la machine 2. (ligne 1 de l'image, avec l'explication en bas.) 
        * La machine concernée lui renvoie son adresse MAC (ligne 2 de l'image). 

???+ question "Quelle est l’adresse IP de diffusion ?"

    ??? success "Solution"

        C'est la dernière adresse du réseau qui devrait être 192.168.1.255 mais sous Filius c'est 255.255.255.255   

???+ question "`ping`"

    Quel est le protocole utilisé par le ping ? Dans quelle couche du modèle TCP/IP se situe-t-il ?

    ??? success "Solution"

        Protocole ICMP couche internet.   


???+ question "`ping ?`"

    A l’issue du ping, comment l’ordinateur 192.168.1.1 sait-il que l’ordinateur 192.168.1.2 est bien connecté ?

    ??? success "Solution"

        Il reçoit un « pong »   


??? success "Fichier de correction Filius à télécharger"

    🌐 Fichier `reseau_deux_ordis.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_deux_ordis.fls)

<!--- La correction à télécharger plus tard 

🌐 Fichier `reseau_deux_ordis.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_deux_ordis.fls)

 -->


### 2. Comment créer un réseau avec davantage d’ordinateurs ?

* Revenir en mode conception et créer un troisième puis un quatrième ordinateur portable. 

👉 Sur Filius les « portables » servent à simuler des ordinateurs, et les « ordinateurs » servent à simuler des serveurs. 

* Vérifier alors qu’il est impossible de les relier aux autres par un câble (« il n’y a plus de connecteur disponible »). Pour créer un réseau avec plus de 2 ordinateurs il faut utiliser des switchs (ou commutateur). 

* Câbler 4 ordinateurs autour d’un switch puis donner à ces nouveaux ordinateurs des adresses IP pertinentes.
* Tester les connexions de votre réseau local en faisant des « ping » entre les ordinateurs.
* Sauvegarder votre projet sous un nom pertinent (ex : ReseauLocalAvecSwitch.fls)

??? success "Fichiers de corrections Filius à télécharger"

    🌐 Fichier `reseau_plus_grand_q2_filius_1.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_plus_grand_q2_filius_1.fls)

<!--- La correction à télécharger plus tard 

🌐 Fichier `reseau_plus_grand_q2_filius_1.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_plus_grand_q2_filius_1.fls)

-->


### 3. Comment ajouter une tablette ou un smartphone dans votre réseau ?

On souhaite maintenant pouvoir connecter des périphériques sans utiliser de câbles. Nous pouvons utiliser le Bluetooth ou le Wi-Fi. 
Le logiciel Filius ne propose pas de point d’accès. Nous ne pourrons pas l’utiliser pour simuler l’ajout de matériel Wi-Fi sur notre réseau. Pour utiliser le Wi-Fi, il faut ajouter un « point d’accès »
Voir la vidéo suivante qui utilise un autre logiciel de simulation : Packet Tracer: (8 minutes) 

![](images/wifi.png){ width=50% .center}  

### 4. Ajout d’un serveur DHCP

Si vous voulez ajouter des matériels dans votre réseau, sans avoir à paramétrer manuellement chacune des adresses IP de votre réseau, vous devez ajouter un serveur DHCP.

* Revenir en mode conception.

* Ajoutez un nouveau périphérique Ordinateur, le nommer Serveur DHCP, le relier au switch, le configurer avec l’adresse IP 192.168.1.253 .

* Double-cliquez sur le serveur DHCP que vous venez d’installer. 

* Cliquez sur « configuration du service DHCP » : Une fenêtre s’ouvre et permet de définir les IP locales maximales et minimales parmi celles autorisées par le masque. Renseignez les IP comme indiqué ci-dessous : 

    * Pour le début de plage, tapez : 192.168.1.10 
    * Pour la fin de plage, tapez : 192.168.1.50 
    * Cocher la case « activer le service DHCP » 
    * Tapez OK 

* Double-cliquez sur chaque ordinateur du réseau, cocher « adressage automatique par serveur DHCP » sauf sur le serveur DHCP qui doit être configuré en statique. 

* Lancez à nouveau une simulation. Si vous avez bien gardé les adresses IP comme noms de vos matériels, vous voyez immédiatement l’adresse IP assignée par le serveur DHCP. 

* Ajouter un portable dans ce réseau, le configurer automatiquement grâce au DHCP, et vérifier les connexions avec les autres matériels avec des `ping`.

Résultat obtenu :  

![](images/dhcp.png){ width=60% } 


??? success "Fichier de correction Filius à télécharger"

    🌐 Fichier `reseau_avec_DHCP_filius_1.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_avec_DHCP_filius_1.fls)

<!--- La correction à télécharger plus tard 

🌐 Fichier `reseau_avec_DHCP_filius_1.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_avec_DHCP_filius_1.fls)

-->

## V. Deux réseaux

???+ note "connecter deux réseaux"

    De nombreuses raisons peuvent amener à connecter plusieurs réseaux entre eux. Pour notre activité, nous prendrons l’exemple de deux réseaux locaux internes à notre lycée (réseau pédagogique et réseau administratif).  
    Pour des questions matérielles et de sécurité, il est préférable de séparer ces deux réseaux, tout en créant une liaison entre eux pour les relier (car on peut parfois avoir besoin d’échanges de données entre ces réseaux). Le lien entre ces réseaux se fait matériellement à l’aide d’un routeur.

### 1. Deux réseaux séparés

Reprendre le projet du TD précédent, puis l’enregistrer sous un nouveau nom (ex : ReseauxAvecRouteur.fls)

![](images/td2_res1.png){ width=90% .center} 

* En mode conception, ajouter un routeur (sélectionner 2 interfaces, c’est à dire deux prises RJ45) puis ajouter un switch, une machine de type portable et une autre de type ordinateur « tour ». Paramétrer leurs interfaces réseaux avec les adresses IP 192.168.2.1 pour le portable et 192.168.2.2 pour l’ordinateur « tour », et avec les masques 255.255.255.0

* En mode simulation, sur la machine 192.168.1.10, tester les connexions vers les autres machines avec la commande ping. 

???+ question

    Quelles sont les machines qui ne peuvent pas être atteintes ?

    ??? success "Solution"

        Les machines d'IP 192.168.2.1 et 192.168.2.2

!!! info "Routeur"

    Chaque ordinateur d’un réseau doit connaitre l’adresse IP du routeur pour pouvoir lui transmettre les paquets n’appartenant pas à son réseau. Cette adresse est renseignée dans le champ Gateway (ou passerelle) de chaque ordinateur. 


* En mode conception, Faire un clic droit sur le routeur puis configurer ses deux interfaces (c’est-à-dire les fils) en leur assignant la dernière adresse disponible de chaque réseau concerné. Quand vous cliquez sur une interface, elle apparaît en vert, ce qui permet de ne pas se tromper d’adresse.

* En mode simulation, sur la machine 192.168.1.10, tester à nouveau les connexions vers les machines injoignables avant la configuration du routeur. Le problème est-il résolu ?

* Il faut donc finaliser la configuration de ces réseaux. En mode conception, renseigner la passerelle (ou gateway en anglais) de chaque ordinateur. Pour cela, il faut commencer par décocher l’adressage automatique par serveur DHCP. Vous pourrez tous les recocher à la fin lorsque vous aurez terminé. 

⚠️  Attention, chaque réseau possède sa propre passerelle ! 

👉 L’adresse IP de la passerelle pour un matériel donné est la première qui est rencontrée sur le routeur utilisé pour « sortir » du réseau. Par exemple la passerelle pour le portable 192.168.1.10 est 192.168.1.254

* En mode simulation, vérifier que toutes les machines peuvent désormais être atteintes depuis 192.168.1.10 avec la commande `ping`.

* Effectuer un `traceroute` (ligne de commande) de la machine 192.168.1.10 vers le portable 192.168.2.1. Noter le chemin parcouru par les paquets de données entre ces deux machines.

* Effectuer de même d’autres essais de `traceroute` de la machine 192.168.2.1 vers des machines du réseau 192.168.1.0

* Effectuer un `traceroute` (ligne de commande) de la machine 192.168.2.1 vers la machine 192.168.2.2. 
Que constatez-vous ?

👉 Sauvegarder le fichier ReseauxAvecRouteur.fls.

??? success "Fichier de correction Filius à télécharger"

    ⌛ Attendez un peu ... 

<!--- La correction à télécharger plus tard 

🌐 Fichier `reseau_deux_ordis.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_DHCP_routeur_filius_2.fls)

 -->

### 2. On relie un réseau à un serveur

Sauvegarder le fichier précédent sous un autre nom, par exemple ReseauxAvecRouteurServeur.fl.
Le modifier pour obtenir la configuration suivante :

![serveur](images/td2_serveur.png){ width=90% .center} 

👉 Compléter les configurations sachant que :

Réseau du serveur : 200.200.200.0  
Serveur : 200.200.200.200  
Masque 255.255.255.0  
Passerelle : 200.200.200.254  

👉 Tester des `ping` d’une machine du réseau 192.168.1.0 vers le serveur, et inversement.

??? success "Fichier de correction Filius à télécharger"

    🌐 Fichier `reseau_deux_ordis.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_DHCP_routeur_serveur_filius_2.fls)

<!--- La correction à télécharger plus tard 

🌐 Fichier `reseau_deux_ordis.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_DHCP_routeur_serveur_filius_2.fls)

 -->

## VI. Le WEB et les échanges.

### 1.	Comment ajouter un serveur DNS ?

Dans cette section, nous allons ajouter un serveur DNS qui va traduire des noms de domaines en adresse IP.

👉 Vous pouvez reprendre un fichier du TD précédent, et l’enregistrer sous ReseauAvecServeur.fls

![DNS](images/td3_res1.png){ width=90% .center} 

* Nous voulons créer la configuration ci-dessus. En mode conception, ajouter l’ordinateur « tour » et essayer de le relier au routeur. Le message « Il n’y a plus de port disponible » apparaît.

Double-cliquer sur le routeur puis **Configurer > Gérer les connections**. 
En cliquant sur **+**, ajouter une troisième interface locale. Fermer la fenêtre et se rendre dans l’onglet correspondant pour lui attribuer l’adresse IP **192.168.0.254**.

![DNS](images/connexions.png){ width=60% .center} 

* Relier l’ordinateur ajouté au routeur. Attribuer l’adresse IP 192.168.0.1 à cet ordinateur et n’oubliez pas de renseigner l’adresse de la passerelle.

* En mode simulation, vérifier avec quelque ping la bonne connexion entre les différentes machines.

* En mode simulation, ajouter un serveur DNS à ce nouvel ordinateur. 

![DNS](images/dns.png){ width=60% .center} 

*  Configurer ce serveur DNS en cliquant sur « DNS Serveur DNS ».

![DNS](images/config_dns.png){ width=70% .center} 

Dans le champ "Nom de domaine" ajouter le nom de domaine **www.nsi.fr** et dans le champ Adresse IP ajouter  **192.168.2.2** (l’autre ordinateur « tour » de notre réseau qui sera notre serveur web). Cliquer sur « **Ajouter** ».

* **Démarrer** le serveur DNS. 

* Configurer le champ DNS des différents ordinateurs portables en précisant l’adresse IP du serveur DNS créé.

* En mode simulation, effectuer un ping www.nsi.fr à partir de différentes machines du réseau (sur lesquelles on aura installé la ligne de commande). Vers quel ordinateur vont-ils se connecter ?

* Si tout va bien, l’aventure se termine avec 0 paquet perdu !


??? success "Fichier de correction Filius à télécharger"

    ⌛ Attendez un peu ... 

<!--- La correction à télécharger plus tard 

🌐 Fichier `reseau_serveur_filius_3.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_serveur_filius_3.fls)

 -->


### 2.	Comment ajouter un serveur web sur notre réseau ?

On veut maintenant héberger nos pages web sur un serveur de notre réseau. Ce serveur devra être accessible par toutes nos machines, via l’URL www.nsi.fr. Nous choisissons la machine 192.168.2.2 comme serveur.

👉 **Sur le serveur 192.168.2.2 :**

* Enregistrer le précédent réseau, comprenant le serveur DNS, sous un autre nom (ex : ServeurWeb.fls)
* Passer en mode simulation et installer un serveur Web et un éditeur de texte sur la machine 192.168.2.2.

![web](images/inst_web.png){ width=60% .center} 

* Démarrer  le serveur web. 
* A l’aide de l’éditeur de texte (Menu Fichier / Ouvrir), modifier le code HTML du fichier **index.html** dans le dossier webserver qui est la page retournée par défaut aux clients et **ENREGISTRER** la modification (avec Fichier puis enregistrer)

👉 **Sur le client 192.168.1.10 :** 

* Installer un **client Web (navigateur web)** sur la machine 192.168.1.10. 
* Démarrer le navigateur et saisir l’URL www.nsi.fr dans la barre d’adresse, pour envoyer une **requête HTTP au serveur Web**. La page d’accueil modifiée du serveur devrait s’afficher.

![page web](images/page.png){ width=60% .center} 


??? success "Fichier de correction Filius à télécharger"

    ⌛ Attendez un peu ... 

<!--- La correction à télécharger plus tard 

🌐 Fichier `reseau_serveur_web_filius_3.fls` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/reseau_serveur_web_filius_3.fls)

 -->


### 3. Observation des échanges 

* Activer l’**affichage des données sur la machine 192.168.1.10** avec un clic droit
* Au besoin, relancer la **requête HTTP** précédente (www.nsi.fr) à l’aide du navigateur puis analyser l’échange de données.


Vous devriez obtenir un échange équivalent à celui-ci-dessous.

Pour la suite des questions, on se servira des numéros de ligne de cette capture d’écran. Notre machine 192.168.1.10 a dans la simulation ci-dessous l’adresse IP 192.168.1.1

![échanges données](images/echanges.png){ width=98% .center} 

???+ question "A quoi servent les échanges lignes 1 et 2 (protocole ARP) ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ]  Récupérer l'@ MAC du routeur
        - [ ]  Récupérer l'@ MAC du serveur DNS
        - [ ]  Interroger le serveur DNS pour récupérer l'@ IP de nsi.fr
        - [ ]  Afficher la page web NSI

    === "Solution"
        
        - :white_check_mark: Récupérer l'@ MAC du routeur
        - :x: ~~Récupérer l'@ MAC du serveur DNS~~
        - :x: ~~Interroger le serveur DNS pour récupérer l'@ IP de nsi.fr~~
        - :x: ~~Afficher la page web NSI~~

???+ question "A quoi servent les échanges lignes 3 et 4 ?"

    === "Cocher la ou les affirmations correctes"
        
        - [ ]  Interroger le serveur web nsi
        - [ ]  Interroger le serveur DNS pour récupérer l'@ IP de nsi.fr
        - [ ]  Afficher la page web NSI
        - [ ]  Récupérer l'@ MAC du serveur DNS

    === "Solution"
        
        - :x:
        - :white_check_mark: Interroger le serveur DNS pour récupérer l'@ IP de nsi.fr
        - :x:
        - :x:

???+ question "Encapsulation des données"

    On s'intéresse à la ligne 3 et plus particulièrement aux 4 couches du modèle TCP/IP qui forme la trame Ethernet suivante.

    ![encapsulations](images/encaps.png){ width=70% } 

    ???+ question "En partant de la machine 192.168.1.1"

        dans quel sens s'encapsulent les données pour former la trame Ethernet ?

        === "Cocher la ou les affirmations correctes"
        
            - [ ]  Internet	Réseau	Transport	Application
            - [ ]  Application	Transport	Internet	Réseau
            - [ ]  Réseau	Internet	Transport	Application
            - [ ]  Réseau	Transport	Internet 	Application

        === "Solution"
        
            - :x:
            - :white_check_mark: Application	Transport	Internet	Réseau
            - :x:
            - :x:

    ???+ question "Les différentes couches"

        Inscrire dans les cases le nom de la couche TCP/IP 

        ![trame sujet](images/trame.png){ width=90% }

        ??? success "Solution"

            ![trame correction](images/trame_corr.png){ width=90% }


!!! info "Connexion TCP"

    On peut représenter l’ouverture d’une connexion TCP par le schéma ci-dessous. 

    ![connexion](images/syn.png){ width=70% }

???+ question "Connexion TCP"

    Retrouver les lignes correspondant à cette ouverture de connexion TCP

    ??? success "Solution"

        Les lignes 5, 6 et 7 correspondent à l'ouverture de connexion TCP.
        On voit 

        * ligne 5 : SYN
        * ligne 6 : SYN + ACK
        * ligne 7 : ACK


???+ question "Couche application"

    Quelles lignes envoient le fichier index.html que l'on a modifié sur le serveur NSI ?

    ??? success "Solution"

        * La ligne 10 envoie de 192.168.2.2 vers 192.168.1.1 le fichier index.html.
        * La ligne 12 demande l'image splashscreen-mini.png
        * Les lignes 14, 16, 18, 20, 22, et 24 correspondent aux trames qui transfèrent cette image. Cela correspond à une image d'un peu moins de 9 Ko : six trames de 1,5 ko.


## VII. Protocole du bit alterné

Ce paragraphe a été réalisé par Gilles LASSUS

Ce protocole est un exemple simple de fiabilisation du transfert de données. 

### 1. Contexte

- Alice veut envoyer à Bob un message M, qu'elle a prédécoupé en sous-messages M0, M1, M2,...
- Alice envoie ses sous-messages à une cadence Δt fixée (en pratique, les sous-messages partent quand leur acquittement a été reçu ou qu'on a attendu celui-ci trop longtemps : on parle alors de _timeout_)

### 2. Situation idéale

![](images/ideale.png){: .center} 

Dans cette situation, les sous-messages arrivent tous à destination dans le bon ordre. La transmission est correcte.

### 3. Situation réelle
Mais parfois, les choses ne se passent pas toujours aussi bien. Car si on maîtrise parfaitement le timing de l'envoi des sous-messages d'Alice, on ne sait pas combien de temps vont mettre ces sous-messages pour arriver, ni même (attention je vais passer dans un tunnel) s'ils ne vont pas être détruits en route.

![](images/realite.png){: .center} 

Le sous-message M0 est arrivé après le M1, le message M2 n'est jamais arrivé...

Que faire ?

Écartons l'idée de numéroter les sous-messages, afin que Bob puisse remettre dans l'ordre les messages arrivés, ou même redemander spécifiquement des sous-messages perdus. C'est ce que réalise le protocole TCP (couche 4 — transport), c'est très efficace, mais cher en ressources. Essayons de trouver une solution plus basique.

### 3. Solution naïve...

Pourquoi ne pas demander à Bob d'envoyer un signal pour dire à Alice qu'il vient bien de recevoir son sous-message ?
Nous appelerons ce signal ACK (comme _acknowledgement_, traduisible par «accusé de réception»).
Ce signal ACK permettra à Alice de renvoyer un message qu'elle considérera comme perdu :

![](images/naive.png){: .center} 

N'ayant pas reçu le ACK consécutif à son message M1, Alice suppose (avec raison) que ce message n'est pas parvenu jusqu'à Bob, et donc renvoie le message M1.

### 4. Mais peu efficace...

![](images/naivebad.png){: .center} 

Le deuxième ACK de Bob a mis trop de temps pour arriver (ou s'est perdu en route) et donc Alice a supposé que son sous-message M1 n'était pas arrivé. Elle l'a donc renvoyé, et Bob se retrouve avec deux fois le sous-message M1. La transmission est incorrecte. 
En faisant transiter un message entre Bob et Alice, nous multiplions par 2 la probabilité que des problèmes techniques de transmission interviennent. Et pour l'instant rien ne nous permet de les détecter.

### 5. Bob prend le contrôle

Bob va maintenant intégrer une méthode de validation du sous-message reçu. Il pourra décider de le garder ou de l'écarter. Le but est d'éviter les doublons.

Pour réaliser ceci, Alice va rajouter à chacun de ses sous-messages un bit de contrôle, que nous appelerons FLAG (drapeau). Au départ, ce FLAG vaut 0. 
Quand Bob reçoit un FLAG, il renvoie un ACK **égal au FLAG reçu**.

Alice va attendre ce ACK contenant le même bit que son dernier FLAG envoyé :

- tant qu'elle ne l'aura pas reçu, elle continuera à envoyer **le même sous-message, avec le même FLAG**.
- dès qu'elle l'a reçu, elle peut envoyer un nouveau sous-message en **inversant** («alternant») **le bit de son dernier FLAG** (d'où le nom de ce protocole).


Bob, de son côté, va contrôler la validité de ce qu'il reçoit : il ne gardera que **les sous-messages dont le FLAG est égal à l'inverse de son dernier ACK**. C'est cette méthode qui lui permettra d'écarter les doublons.

Observons ce protocole dans plusieurs cas :

##### 5.1 Cas où le sous-message est perdu

![](images/alt2.png){: .center} 



##### 5.2 Cas où le ACK  est perdu
![](images/alt1.png){: .center} 

Le protocole a bien détecté le doublon du sous-message M1.

##### 5.3 Cas où un sous-message est en retard

![](images/alt3.png){: .center} 

Le protocole a bien détecté le doublon du sous-message M1... mais que se passerait-il si notre premier sous-message M1 était _encore plus_ en retard ?


### 6. Conclusion
Le protocole du bit alterné a longtemps été utilisé au sein de la couche 2 du modèle OSI (distribution des trames Ethernet). Simple et léger, il peut toutefois être mis en défaut, ce qui explique qu'il ait été remplacé par des protocoles plus performants.

---
**Bibliographie**
- Numérique et Sciences Informatiques, 1re, T. BALABONSKI, S. CONCHON, J.-C. FILLIATRE, K. NGUYEN, éditions ELLIPSES.
- Prépabac NSI 1ère, C.ADOBET, G.CONNAN, G. ROZSAVOLGYI, L.SIGNAC, éditions Hatier.



## VIII. Interconnexion de deux réseaux

!!! info "Rappel"

    Les « routeurs » sont aussi nommés « passerelles » ou « gateway ».

### 1. Interconnexion de deux réseaux.

On donne le réseau ci-dessous : 

!!! danger "Attention"

    ⚠️  Ne pas lancer de simulation avec le logiciel Filius avant d’avoir complété les questions a), b) et c) 

![2 routeurs](images/deux_res.png){: .center} 

Pour chaque réseau, on choisit :

* Pour les adresses IP des routeurs : les dernières possibles.
* Pour l’adresse IP du serveur DHCP : la dernière possible, une fois celle des routeurs attribuées. 

**Compléter les adresses IP ci-dessous**

???+ question "a) Pour le réseau 192.168.1.0"

    * Passerelle : 
    * Serveur DHCP : 

    ??? success "Solution"

        * Passerelle : 192.168.1.254
        * Serveur DHCP : 192.168.1.253

???+ question "b) Pour le réseau 10.0.0.0"

    * Passerelle « Routeur 0 » : 
    * Passerelle « Routeur 1 » : 

    ??? success "Solution"

        * Passerelle « Routeur 0 » : 10.255.255.254
        * Passerelle « Routeur 1 » : 10.255.255.253


???+ question "c) Pour le réseau 192.168.2.0"

    * Passerelle : 
    * Serveur DHCP : 

    ??? success "Solution"

        * Passerelle  : 192.168.2.254
        * Serveur DHCP : 192.168.2.253

💻 Construire le réseau ci-dessus, et configurer tous les équipements. (Utiliser le DHCP)
Pour les routeurs, **dans l’onglet général, il ne faut pas cocher « routage automatique »**

👉 Vérifier avec des  ping, la connexion entre :

* deux postes du réseau 192.168.1.0
* deux postes du réseau 192.168.2.0
* un poste du réseau 192.168.1.0, et un poste du réseau 192.168.2.0 . Que constatez-vous ? 

??? success "Solution"

    Cela échoue.
