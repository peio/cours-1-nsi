---
author: Pierre Marquestaut
title: Les p-uplets
---

# Les p-uplets

## I. Définition

Un **p-uplet** (ou *tuple* en anglais) est une suite ordonnée d’éléments qui peuvent être chacun de n’importe quel type. Les valeurs sont regroupées entre des parenthèses et séparées par des virgules.

```python
a = (5, 4, 1, 2)
```
Les p-uplet ressemblent beaucoup aux tableaux : on accède à chaque élément grâce à son indice.
Cependant, la grande différence est qu'un p-uplet ne peux pas être modifié : on dit que ce sont des objets **immuables**. 


!!! example "Une erreur"

    Dans le terminal ci-dessous, définir le p-uplet `a` comme dans l'exemple ci-dessus, afficher la première valeur puis essayer de la modifier.

    {{ terminal() }}

    Une erreur est générée, expliquant que les variables de type *tuple* ne permettent pas les affectations.

Un p-uplet peut être utilisé pour stocker un **enregistrement** issu d'une table de données.

Par exemple :
```python
ville = ("Biarritz", 64100, 43.48333,  -1.56667)
```



!!! note "Singleton"

    Un singleton est un tuple ne contenant qu'un seul élément. Il s'écrit ainsi, avec une valeur et une virguele :

    ```python
    singleton = ("Une seule valeur",)
    ```

## II . Affectation multiple

En Python, il est possible d'affecter en une seule instruction plusieurs variables avec les valeurs contenues dans un p-uplet (en anglais, on parle de  "*unpacking*") :


```python
fruits = ("pomme", "banane", "cerise")
vert, jaune, rouge = fruits
```
Dans l'exemple précédent, la variable `vert` contient la valeur `"pomme"`.

???+ question 

    Quelles valeurs contiennent les variable `nom`, `prenom` et `age` à la fin de l'exécution de ce programme ? 

    ```python
    enregistrement = ("Dupont", "Antoine", 23)
    nom, prenom, age = enregistrement
    ```
    <div class="centre">
        <iframe src="https://www.codepuzzle.io/ID6CBR" width="100%" height="600" frameborder="0"></iframe>
    </div>



## III.Permutation de valeurs

 L'affectation multiple permet d'échanger des valeurs en une seule instruction, et sans avoir recours à des variables temporaires.

 ```python
 a = 5
 b = 8
 a, b = b, a
 ```
L'expression `b, a` est d'abord évaluée pour obtenir le p-uplet `8, 5`, puis la valeur $8$ est affectée à la variable `a` et la valeur $5$ à la variable `b`.

???+ question 

    Quelles valeurs contiennent les variable `a`, `b` et `c` à la fin de l'exécution de ce programme ? 

    ```python
    a, b, c = 2, 4, 5
    b, c, a = a, b, c
    ```
    <div class="centre">
        <iframe src="https://www.codepuzzle.io/IDCXAW" width="100%" height="450" frameborder="0"></iframe>
    </div>



## IV. Retour de fonction

Dans une fonction, renvoyer un p-uplet peut permettre de renvoyer plusieurs valeurs.


```python
def somme_et_difference(a, b):
    return a + b, a - b

somme, difference = somme_et_difference(8, 2)
```
A la  fin de l'exécution, la variable `somme` contiendra la valeur $10$ et la variable `différence` contiendra la valeur $6$.


???+ question

    <div class="centre">
        <iframe src="https://www.codepuzzle.io/IPUYQ7" width="100%" height="370" frameborder="0"></iframe>
    <div>/