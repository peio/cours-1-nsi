import math

# un tableau reels en compréhension qui contient les réels de 0 à 6.3 (inclus) par pas de 0.1
reels = ...

# un tableau arrondis en compréhension qui arrondis les valeurs du tableau reels à l'entier inférieur. 
# on utilisera la fonction math.floor
arrondis = ...

# Un tableau `sommes` qui comprend les sommes des éléments pris deux à deux dans deux tableaux de même taille.
valeurs_1 = [2, 3, 1, 5]
valeurs_2 = [4, 1, 7, 0]
sommes = ...

assert sommes == [6, 4, 8, 5]
