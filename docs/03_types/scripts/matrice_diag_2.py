def get_diag_2(matrice) -> list :
    """
    Entrée : matrice : un tableau de tableaux de mêmes longueurs
    Sortie : diag_2 : une tableau dont les éléments sont les éléments de la 2eme diagonale (de en bas à gauche à en haut  droite)
    >>> m = [ [1, 3, 4],
              [5 ,6 ,8],
              [2, 1, 3] ]
    >>> get_diag_2(m)
    [4, 6, 2]

    """

    ...


m = [[1, 3, 4],
     [5, 6, 8],
     [2, 1, 3]]

assert get_diag_2(m) == [4, 6, 2]

