---
author: Jean-Louis Thirot, Mireille Coilhac, Pierre Marquestaut
title: Tableau de tableaux

---


# Tableau de tableaux

!!! example "L'échiquier"
    Pour situer une pièce sur un jeu d'échec, on a besoin de deux coordonnées, une pour chaque axe.
    ![echec](img/echec.jpg){ width=30% }

!!! note "Matrice"
    Une matrice permet de représenter des informations repérées par deux coordonnées.

!!! example "Une première matrice"
    En mathématique, une matrice est représentée de la façon suivante :

    \begin{bmatrix}
    1 & 3 & 4\\
    5 & 6 & 8\\
    2 & 1 & 3
    \end{bmatrix}



## Représentation d'une matrice

Nous pouvons représenter chaque ligne de la matrice par un tableau :

```python
ligne1 = [1, 3, 4]
ligne2 = [5, 6, 8]
ligne3 = [2, 1, 3]
```

Pour accéder à la première colonne, on devra utiliser `ligne1[0]`, `ligne2[0]` et `ligne3[0]`, ce qui n'est pas forcément pratique pour automatiser.

On peut améliorer en ne créant qu'une unique variable pour stocker l'ensemble des valeurs : on obtient alors un tableau de tableaux !

```python
m = [ligne1, ligne2, ligne3]
```

ou directement :


```python
m = [[1, 3, 4], [5, 6, 8], [2, 1, 3]]
```

que l'on peut aussi écrire ainsi pour plus de lisibilité :

```python
m = [[1, 3, 4],
     [5, 6, 8],
     [2, 1, 3]]
```
???+ question

    Représenter la matrice suivante en python :

    \begin{bmatrix}
    1 & 2 & 3 & 4\\
    5 & 6 & 7 & 8\\
    \end{bmatrix}

    {{ IDE('scripts/matrice1') }}
    
Ainsi on accède à la première ligne par `#!py m[0]` et à la première colonne de la première ligne par `#!py  m[0][0]`

???+ question

    On a la matrice suivante ```pieces = [["Dame", "Roi", "Fou", "Pion"], ["Pion", "Tour", "Cavalier", "Fou"]] ```

    Quelle expression permet d'obtenir la valeur ```"Roi"``` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] ```pieces[1][1]```
        - [ ] ```pieces[0][1]```
        - [ ] ```pieces[0, 1]```
        - [ ] ```pieces[1][0]```

    === "Solution"
        

        - :x: ~~```pieces[1][1]```~~ Les valeurs ne sont pas les bonnes.
        - :white_check_mark: ```pieces[0][1]```
        - :x: ~~```pieces[0, 1]```~~ La syntaxe n'est pas correcte.
        - :x: ~~```pieces[1][0]```~~ Le premier indice est celui de la ligne, le second de la colonne.



## Parcours un tableau de tableaux

!!! abstract "Double boucle"
    Pour parcourir l'ensemble des valeurs contenues dans un tableau de tableaux, il est nécessaire d'utiliser une **double boucle**, c'est-à-dire une boucle à l'intérieur d'une autre boucle.

???+ question "un exemple"

    <iframe src="https://www.codepuzzle.io/IPKULF" width="100%" height="540" frameborder="0"></iframe>

​

???+ question "Tester"
    Dans l'éditeur ci dessous, tester le code précédent :
    {{ IDE() }}
