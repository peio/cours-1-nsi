---
author: Pierre Marquestaut
title: Construction par compréhension
---

# Construction par compréhension

## I. Initialisation

Un tableau peut s'initialiser de plusieurs manières.

Un tableau peut être initialisé par extension, en énumérant chaque élément qu'il contient.

```python
>>> tableau_par_extension = [0, 0, 0, 0]
>>> un_autre_tableau = [5, -1, 6, 7, 0, -4]
```
!!! note
    Cette méthode  est simple mais ne peut s'appliquer pour des tableaux ne contenant qu'un nombre limité d'éléments. 
    Difficile en effet d'utiliser cette méthode pour initialiser un tableau contenant des centaines, voire des milliers d'éléments.

Un tableau peut être initialisé par concaténation, en concaténant un nombre de fois donné le même tableau.

```python
>>> tableau_par_concatenation = [0] * 4  
>>> tableau_par_concatenation
[0, 0, 0, 0]
```
!!! note 
    La méthode par concaténation permet de créer des tableaux dont les éléments sont tous identiques ($0$ dans l'exemple précédent). Si on souhaite que les éléments soient différents (comme une série d'entiers de 0 à 5), des instructions supplémentaires sont nécessaires.
    ```python
    tableau = [0] * 6
    for i in range(6):
        tableau[i] = i
    ```

Le langage Python permet une écriture simplifiée de la création de tableaux, résumée en une seule expression : il s'agit de la **construction par compréhension**.

Cela permet notamment de créer des tableaux que nous n'aurions pas su créer avec la méthode précédente. Par exemple, comment créer le tableaux contenant les 1000 premiers carrés : 1, 4, 9, 16, 25 etc ? 

!!! warning initialisation par concaténation

    L'initialisation par concaténation présentée ci-dessus est une solution facile à comprendre pour les débutants. En revanche, elle peut se révéler source d'erreurs (voir l'exercice [Copie de tableaux](https://codex.forge.apps.education.fr/exercices/copie_tableau/){ target="_blank"}), notamment à la création d'un tableau de tableaux. La construction par compréhension est donc à privilégier.

Le code suivant permet également de créer un tableau de 4 éléments initialisés à 0. C'est une création de tableau **par compréhension**. on dit aussi que le tableau est **écrit en compréhension**.
```python
>>> tableau = [0 for i in range(4)]
>>> tableau
[0, 0, 0, 0]
```
De la même façon, la série d'entiers de 0 à 10 peut s'obtenir avec :
```python
>>> tableau = [i for i in range(6)]
>>> tableau
[0, 1, 2, 3, 4, 5]
```

???+ question

    ```python
    tableau = [i for i in range(5, 15)]
    ```

    === "Avec quelle ligne pourrait-on remplacer la ligne précédente ? (Cocher la réponse correcte)"
        
        - [ ] `#!py tableau = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]`
        - [ ] `#!py tableau = [i, i, i, i, i, i, i, i, i, i]`
        - [ ] `#!py tableau = [0, 0, 0, 0, 0, 0, 0, 0, 0]`
        - [ ] `#!py tableau = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14]`

    === "Solution"
        
        
        - :x: La valeur 15 est exclue.
        - :x: i prend les valeurs de l'intervalle.
        - :x: La valeur n'est pas contante.
        - :white_check_mark: i prend tour à tour les valeurs de 5 jusqu'à 14.

???+ question
    On souhaite créer un tableau `cinq` en compréhension qui contient 20 entiers 5.
    <iframe src="https://www.codepuzzle.io/IPT3WK" width="100%" height="180" frameborder="0"></iframe>


???+ question

    Compléter le script ci-dessous :

    {{ IDE('scripts/construction') }}

??? success "Solution"

    ```python
    # un tableau cents en compréhension qui contient 10 entiers 100.
    cents = [100 for k in range(10)]

    # un tableau entiers en compréhension qui contient les 10 entiers entre 1 et 10.
    entiers = [k for k in range(1, 11)]

    ```
On peut aussi utiliser des chaînes de caractères : 
```pycon
>>> mon_tableau = ["p = " + str(p) for p in range(5)]
>>> mon_tableau
['p = 0', 'p = 1', 'p = 2', 'p = 3', 'p = 4']
```
???+ question

    Ecrire en compréhension : `['NSI-1', 'NSI-2', 'NSI-3', 'NSI-4', 'NSI-5', 'NSI-6', 'NSI-7', 'NSI-8', 'NSI-9', 'NSI-10']`

    {{ IDE() }}

??? success "Solution"

    ```python
    mon_tab = ["NSI-"+ str(i) for i in range(1, 11)]
    print(mon_tab)
    ```

### Quelques exemples
  

???+ question

    Répondre sur **papier**.

    Donner les tableaux tableau_1, tableau_2, tableau_4, tableau_6 et tableau_7

    ```python
    tableau_1 = [3 for i in range(4)]
    tableau_2 = [4-i for i in range(3)]
    tableau_3 = [1, 2, 3]
    tableau_4 = [tableau_3[i]**2 for i in range(len(tableau_3))]
    tableau_5 = ["a","b","c"]
    tableau_6 = [tableau_5[i]*2 for i in range(len(tableau_5))]
    tableau_7 = [elem*2 for elem in lst5]
    ```

??? success "Solution"

    ```python
    tableau_1 = [3, 3, 3, 3]
    tableau_2 = [4, 3, 2]
    tableau_4 = [1, 4, 9]
    tableau_6 = ['aa', 'bb', 'cc']
    tableau_7 = ['aa', 'bb', 'cc']
    ```

## II. Appliquer une traitement

Grâce à la construction par compréhension, il est possible d'appliquer un traitement (opération, fonction...) à chaque élément d'un tableau.

Dans le code suivant, on crée un tableau dont les éléments sont les valeurs de l’intervale élevées au carré .
```python
>>> carre = [i**2 for i in range (11)]
>>> carre
[0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100]
>>> racine = [math.sqrt(k) for k in carre]
>>> racine
[0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
```
???+ question

    ```python
    double = [i*2 for i in range(10)]
    ```

    === "Avec quelle ligne pourrait-on remplacer la ligne précédente ? (Cocher la réponse correcte)"
        
        - [ ] `#!py double = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]`
        - [ ] `#!py double = [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]`]`
        - [ ] `#!py double = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18]`
        - [ ] `#!py double = [4, 8, 12, 16]`

    === "Solution"
        
        
        - :x: Un traitement est appliqué aux valeurs prises par i.
        - :x: Le mauvais traitement est appliqué.
        - :white_check_mark: le tableau est composé du double de chaque valeur de l'intervalle [0,9]
        - :x: Le tableau double doit contenir autant d'éléments que le tableau d'origine.

???+ question
    On souhaite créer un tableau `dizaines` en compréhension qui contient 10 entiers allant de 0 à 90 de 10 en 10.
    <iframe src="https://www.codepuzzle.io/IPRBEF" width="100%" height="180" frameborder="0"></iframe>

???+ question

    Compléter le script ci-dessous :

    {{ IDE('scripts/traitement') }}

??? success "Solution"

    ```python
    import math

    # un tableau reels en compréhension qui contient les réels de 0 à 6.3 (inclus) par pas de 0.1
    reels = [valeur/10 for valeur in range(64)]

    # un tableau arrondis en compréhension qui arrondis les valeurs du tableau reels à l'entier inférieur. 
    # on utilisera la fonction math.floor
    arrondis = [math.floor(i) for i in reels]

    # Un tableau `sommes` qui comprend les sommes des éléments pris deux à deux dans deux tableaux de même taille.
    valeurs_1 = [2, 3, 1, 5]
    valeurs_2 = [4, 1, 7, 0]
    sommes = [valeurs_1[p] + valeurs_2[p] for p in range(len(valeurs_1))]
    assert sommes == [6, 4, 8, 5]
    ```
    ```
        
## III. Appliquer un filtre

La construction par compréhension permet d'appliquer un filtre à une structure de données de départ, afin de ne garder que certains éléments. On utilise pour cela une condition précédée du mot-clé `if`.

On peut ainsi créer un tableau avec l'ensemble des nombres pairs parmi les nombres d'un tableau initial.

```python
>>> tableau = [0, 1, 6, 5, 4, 11, 12, 23, 26]
>>> pairs = [p for p in tableau if p%2 == 0]
>>> pairs
[0, 6, 4, 12, 26]
```

???+ question 

    ```python
    tableau = [i for i in range(5, 15)]
    c = [j for j in tableau if j < 10]
    ```

    === "Avec quelle ligne pourrait-on remplacer les lignes précédentes ? (Cocher la réponse correcte)"
        
        - [ ] `#!py nouveau_tableau = [5, 6, 7, 8, 9, 10, 11, 12, 13, 14]`
        - [ ] `#!py nouveau_tableau = [5, 6, 7, 8, 9]`
        - [ ] `#!py nouveau_tableau = [9, 8, 7, 6, 5]`
        - [ ] `#!py nouveau_tableau = [10, 11, 12, 13, 14]`

    === "Solution"   
        
        - :x: La condition entraine la sélection de certaines valeurs.
        - :white_check_mark: on sélectionne tous les éléments du tableau inférieurs à 10.
        - :x: Les éléments conservent l'ordre dans lequel il se trouvent dans le tableau initial.
        - :x: La condition indique les éléments qui sont conservés.

???+ question
    On souhaite créer un tableau `negatifs_carré` en compréhension qui contient les nombres réels négatifs du tableau  `nombres `, élevés au carré

     `nombres = [1, 0, -2, 9, -5, 4, -7, 5, -8]`
    <iframe src="https://www.codepuzzle.io/IPW7MP" width="100%" height="180" frameborder="0"></iframe>

???+ question

    Compléter le script ci-dessous :

    {{ IDE('scripts/ssensemble') }}

??? success "Solution"

    ```python
    # un tableau positifs en compréhension qui contient 
    #les nombres réels positifs du tableau nombres
    nombres = [1, 0, -2, 9, -5, 4, -7, 5, -8]
    positifs = [k for k in nombres if k > 0]

    # un tableau voyelle_phrase en compréhension qui ne contient que les voyelles 
    # contenues dans la chaine de caractère phrase
    phrase = "je ne suis pas sans voix !"
    VOYELLES = "aeiouy"
    voyelle_phrase = [caractere for caractere in phrase if caractere in VOYELLES]
    ```

## IV. Matrice

Un matrice est un tableau de tableaux de même longueurs.
Pour construire une matrice, on peut donc utiliser des **compréhensions imbriquées**.

```python
>>> matrice = [[k for k in range(4)] for j in range(3)]
>>> matrice
[[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]]
```

Il est également possible d'extraire une colonne d'une matrice.
```python
>>> colonne_2 = [ligne[2] for ligne in matrice]
>>> colonne_2
[2, 2, 2]
```
???+ question 

    ```python
    matrice = [[j for i in range(4)] for j in range(4)]
    ```

    === "Avec quelle ligne pourrait-on remplacer la ligne précédente ? (Cocher la réponse correcte)"
        
        - [ ] `#!py matrice = [[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11], [12, 13, 14, 15]]`
        - [ ] `#!py matrice = [[0, 0, 0, 0], [1, 1, 1, 1], [2, 2, 2, 2], [3, 3, 3, 3]]`
        - [ ] `#!py matrice = [[0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3], [0, 1, 2, 3]]`
        - [ ] `#!py matrice = [0, 1, 2, 3]`

    === "Solution"   
        
        - :x: La valeur j est constante pour chaque ligne.
        - :white_check_mark: La valeur j prend la valeur 0 pour la première ligne, puis 1, etc.
        - :x: La valeur j est constante pour chaque ligne.
        - :x: Les constructions imbriquées engendrent un tableau de tableaux.

???+ question
    On souhaite un tableau `matrice_carree` en compréhension qui contient des booléen `False` dans une matrice 4x4.
    <iframe src="https://www.codepuzzle.io/IPKSER" width="100%" height="180" frameborder="0"></iframe>

???+ question

    Compléter le script ci-dessous :

    {{ IDE('scripts/matrice') }}

??? success "Solution"

    ```python
    # un tableau matrice_carre_10 en compréhension, matrice 10x10
    # dont chaque ligne contient les entiers de 1 à 10.
    matrice_carre_10 = [[k for k in range(1, 11)] for j in range(10)]

    # un tableau ligne_5 en compréhension qui contient la colonne 5 (située à l'indice 4)
    # de la matrice matrice_carre_10
    colonne_5 = [ligne[4] for ligne in matrice_carre_10]

    # un tableau diagonale en compréhension qui contient la colonne 1ère colonne de la 1ère ligne,
    # la 2ème colonne de la 2ème ligne.... de la matrice carrée matrice_carre_10
    diagonale = [matrice_carre_10[i][i] for i in range(len(matrice_carre_10))]
    ```

???+ question

    Compléter le script ci-dessous :

    {{ IDE('scripts/matrice_diag_2') }}

## V. Exercices

!!! faq "Exercice"
    [Copie d'un tableau](https://e-nsi.gitlab.io/pratique/N1/119-copie_tableau/sujet/){ .md-button target="_blank" rel="noopener"}

!!! faq "Exercice"
    [Indices d'une occurence](https://e-nsi.gitlab.io/pratique/N1/119-recherche_indices/sujet/){ .md-button target="_blank" rel="noopener"}
   


