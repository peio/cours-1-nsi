---
author: Mireille Coilhac, Chrles Poulemaire, Pierre Marquestaut
title: Les dictionnaires
---

# Les dictionnaires

![dico](img/dictionnaire.jpg){ width=60% }

## I. Définition

!!! abstract "Notion de dictionnaire"

    Un **dictionnaire** ou **tableau associatif** est une structure de données qui permet de stocker un ensemble d’éléments identifiés par un objet non mutable appelé **clé**.

    Un dictionnaire est donc un ensemble **clés / valeurs**.  

!!! note "Types possibles des clés"

    On utilise souvent des chaînes de caractères comme clé, mais cela peut être aussi un nombre, un tuple ou tout autre type non mutable.



Un répertoire téléphonique est un exemple de tableau associatif :

- les clés sont les noms
- les valeurs sont les numéros de téléphone



!!! note "Analogie"

    L'analogie avec les dictionnaires usuel est évidente : la clé est le mot, la valeur associée à la clé est la définition du mot (dictionnaire français), ou sa traduction (dictionnaire français-anglais). 



!!! abstract "Syntaxe"

    Un dictionnaire s'écrit entre accolades sous la forme : `{clé1 : valeur1 , clé2 : valeur2 , ... ,cléN : valeurN}`. 


???+ question

    On peut choisir comme clé d'un dictionnaire

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `"nom"`
        - [ ] `["pomme", "Golden"]`
        - [ ] `5`
        - [ ] `("pain", "baguette")`

    === "Solution"
        
        - :white_check_mark: `"nom"` est une chaîne de caractères immuable
        - :x: Une liste **n'est pas immuable**
        - :white_check_mark: Un entier est immuable
        - :white_check_mark: Un tuple de chaines de caractères est immuable


???+ question

    On peut choisir 

    === "Cocher la ou les affirmations correctes"
        
        - [ ] comme clé d'un dictionnaire `"Kouign amann"`.
        - [ ] comme clé d'un dictionnaire `["farine", "beurre", "sucre"]`.
        - [ ] comme valeur d'un dictionnaire `"Kouign amann"`.
        - [ ] comme valeur d'un dictionnaire `["farine", "beurre", "sucre"]`.

    === "Solution"
        
        - :white_check_mark: `"Kouign amann"` est une chaîne de caractères immuable.
        - :x: Une liste **n'est pas immuable**.
        - :white_check_mark: On peut choisir n'importe quel type pour une valeur.
        - :white_check_mark: On peut choisir n'importe quel type pour une valeur.

## II. Opération sur les dictionnaires

### 1. Création d'un dictionnaire par extension

!!! abstract "Syntaxe"

    Nous pouvons directement créer le dictionnaire par extension : `dictionnaire = {une_cle : une_valeur , autre_cle : autre_valeur, …}`


!!! example "Exemple"

    Imaginons que je fasse l'inventaire de mon dressing :

    | habits | quantité |
    | :--: | :--: |
    | pantalons | 3 |
    | pulls | 4 |
    | tee-shirts | 8 |

    La création du dictionnaire représentant mon dressing se fera par :
        ```python
        >>> dressing = {"pantalons":3, "pulls":4, "tee-shirts":8}
        ```
    On dit que `"pantalon"`, `"pulls"` et `"tee-shirts"` sont **les clés** et que $3$, $4$ et $8$ sont **les valeurs** qui leur sont respectivement associées.


## 2. Accès à une valeur

!!! abstract "Accès par la clé"
    Dans un tableau, on accède à une valeur à partir de l'indice qui lui est associé.
    Dans un dictionnaire, on accède à une valeur à partir de la clé qui lui est associée.

!!! example "Exemple"

    A partir du dictionnaire `dressing` défini précédemment, l'accès à une valeur se fera par :

    ```pycon
    >>> dressing["pulls"]
    4
    ```

    On dit que ```"pulls"``` est **la clé** et que 4 est **la valeur** associée à la clé.

    !!! warning "Attention"

        Si on tente d'accéder à une valeur avec une clé qui n'appartient pas au tableau, on obtient une erreur :

        ```pycon
        >>> dressing["chaussette"]
            Traceback (most recent call last):
            File "<stdin>", line 1, in <module>
            KeyError: 'chaussette'
        ```

???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    On veut accéder au nombre de lapins. Quelle commande faut-il saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `ferme_gaston[0]`
        - [ ] `ferme_gaston["lapin"]`
        - [ ] `ferme_gaston("lapin")`
        - [ ] `ferme_gaston(lapin)`

    === "Solution"
        
        - :x: ~~`ferme_gaston[0]`~~ On accède à une valeur grâce à une clé.
        - :white_check_mark: `ferme_gaston["lapin"]`
        - :x: ~~`ferme_gaston("lapin")`~~ Il faut des crochets, non des parenthèses.
        - :x: ~~`ferme_gaston(lapin)`~~ "lapin" est une chaîne de carctères. Ne pas oublier les guillemets.

???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    On saisit la commande `ferme_gaston["poule"]`. Qu'obtient-on ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `5`
        - [ ] `None`
        - [ ] `Une erreur`
        - [ ] `0`

    === "Solution"
        
        - :x: ~~`5`~~ On accède à une valeur grâce à une clé.
        - :x: ~~`None`~~ Il faut des crochets, non des parenthèses.
        - :white_check_mark: la clé `"poule""` n'appartient pas au dictionnaire. La commande entraine donc une erreur.
        - :x: ~~`0`~~ Il faut des crochets, non des parenthèses.

### 3. Modification d'un dictionnaire

!!! abstract "Accès par la clé"
    Comme pour les tableaux, on modifie une valeur associée à une clé en lui affectant une nouvelle valeur.

!!! example "Exemple"

    A partir du dictionnaire `dressing` défini précédemment, l'accès à une valeur se fera par :

    ```pycon
    >>> dressing["pulls"] = 5
    >>> dressing
    {'pantalons': 3, 'pulls': 5, 'tee-shirts': 8}
    ```

!!! note "Ajout d'une clé"

    Si la clé n'est pas encore présente, l'affectation ajoute un nouveau couple clé/valeur.

    ```pycon
    >>> dressing["chaussettes"] = 10
    >>> dressing
    {'pantalons': 3, 'pulls': 5, 'tee-shirts': 8, 'chaussettes': 10}
    ```

???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    Gaston a acheté 3 lapins. Il faut modifier le dictionnaire.  
    Quelle commande peut-on saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `ferme_gaston[0] = 8`
        - [ ] `("lapin" : 5) = ("lapin" : 8)`
        - [ ] `ferme_gaston("lapin") = 8`
        - [ ] `ferme_gaston["lapin"] = 8`
        - [ ] `ferme_gaston["lapin"] = ferme_gaston["lapin"] + 3`

    === "Solution"
        
        - :x: ~~`ferme_gaston[0] = 8`~~ On accède à une valeur grâce à une clé.
        - :x: ~~`("lapin" : 5) = ("lapin" : 8)`~~
        - :x: ~~`ferme_gaston("lapin") = 8`~~ Il faut des crochets, non des parenthèses.
        - :white_check_mark: `ferme_gaston["lapin"] = 8` juste mais préférer la répose ci-dessous
        - :white_check_mark: `ferme_gaston["lapin"] = ferme_gaston["lapin"] + 3`

???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4}`
    On veut ajouter la paire `"dragon : 2"` Quelle commande faut-il saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `ferme_gaston["dragon": 2]`
        - [ ] `ferme_gaston["dragon"] = 2`
        - [ ] `2 = ferme_gaston["dragon"]`
        - [ ] `ferme_gaston["dragon", 2]`
        - [ ] `ferme_gaston("dragon", 2)`

    === "Solution"
        
        - :x: ~~`ferme_gaston["dragon": 2]`~~
        - :white_check_mark: `ferme_gaston["dragon"] = 2`
        - :x: ~~`2 = ferme_gaston["dragon"]`~~
        - :x: ~~`ferme_gaston["dragon", 2]`~~
        - :x: ~~`ferme_gaston("dragon", 2)`~~

### 4. Compléter un dictionnaire vide.

!!! abstract "Syntaxe"

    Une façon très pratique de créer un dictionnaire, est de partir d'un dictionnaire vide. Il suffit ensuite d'insérer des couples **clé : valeur** en utilisant les clés.
    
!!! example "Exemple"  

    ```python
    dressing = {}
    dressing["pulls"] = 4
    dressing["pantalons"] = 3
    dressing["tee-shirt"] = 8
    ```

???+ example "Exemple"

    On fournit la liste des courses suivantes

    | articles | quantité |
    | :--: | :--: |
    | yaourts | 12 |
    | poulet | 1 |
    | baguette | 2 |

???+ question "Tester"

    **Compléter** le code suivant pour que le dictionnaire `panier` contienne cette liste de course.

    {{ IDE('scripts/panier1') }}



## III. Propriétés d'un dictionnaire

### 1. Appartenance

!!! abstract "Mot clé `in`" 

    Le mot-clé `in` permet de créer une expression booléenne qui vérifie l'appartenance d'une clé à un dictionnaire.

!!! example  "Exemple"

    ```pycon
    >>> "tricot" in dressing
    False
    >>> "pull" in dressing
    True
    ```

### 2. Longueur

!!! info "Longueur"

    On appelle longueur d’un dictionnaire le nombre de paires clé:valeur qui le compose. 
    
    Cette longueur peut-être obtenue à l’aide de la fonction `len`.

!!! example "Exemple"

    ```pycon
    >>> len(dressing)
    3
    ```
???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 6, "vache": 7, "cochon": 2, "cheval": 4, "dragon": 2}`
    Que vaut l'expression `len(ferme_gaston)` ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] 5
        - [ ] 10
        - [ ] Un message d'erreur
        - [ ] Rien
        - [ ] 2

    === "Solution"
        
        - :white_check_mark: 5 .C'est la longueur du dictionnaire. Il y a 5 paires (clés: valeur)
        - :x: ~~10~~
        - :x: ~~Un message d'erreur~~
        - :x: ~~Rien~~
        - :x: ~~2~~

### 3. Suppression d'une clé

!!! note "Suppression"

    On utilise l'instruction `del` pour supprimer une clé et la valeur associée d'un dictionnaire.

!!! example "Exemple"

    ```pycon
    del dressing["chaussettes"]
    ```

???+ question

    On considère le dictionnaire `ferme_gaston = {"lapin": 5, "vache": 7, "cochon": 2, "cheval": 4, "dragon": 2}`
    On veut supprimer la paire `"dragon": 2`. Quelle commande faut-il saisir ?

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `del ferme_gaston[4]`
        - [ ] `del ("dragon")`
        - [ ] `del ferme_gaston("dragon")`
        - [ ] `del ferme_gaston("dragon": 2)`
        - [ ] `del ferme_gaston["dragon"]`

    === "Solution"
        
        - :x: ~~`del ferme_gaston[4]`~~ On accède à une valeur grâce à une clé.
        - :x: ~~`del ("dragon")`~~
        - :x: ~~`del ferme_gaston("dragon")`~~
        - :x: ~~`del ferme_gaston("dragon": 2)`~~
        - :white_check_mark: `del ferme_gaston["dragon"]`

### 4. Extraction des clés et/ou des valeurs Méthodes `.keys()`, `.values()` et `.items()`

En Python, il est possible d'extraire d'un dictionnaire des listes des clés et des valeurs qu'il contient.

!!! note "Méthode `.keys()`"

    La méthode `.keys()` permet de lister les clés d'un dictionnaire :

    ```pycon
    >>> dressing.keys()
    dict_keys(['pantalons', 'pulls', 'tee-shirts'])
    ```

!!! note "Méthode `.values()`"

    La méthode `.values()` permet de lister les valeurs d'un dictionnaire :

    ```pycon
    >>> dressing.values()
    dict_values([3, 4, 8])
    ```

!!! note "Méthode `.items()`"

    La méthode `.items()` permet de lister les couples clé/valeur :

    ```pycon
    >>> dressing.items()
    dict_items([('pantalons', 3), ('pulls', 4), ('tee-shirts', 8)])
    ```   


## IV Parcours d'un dictionnaire

Un dictionnaire est une structure qui peut être parcourue afin d’obtenir des informations sur chacun de ses éléments.

Il existe trois types de parcours : le parcours par clé, le parcours par valeur et le parcours par élément.

### 1. Parcours par clé

!!! abstract "Syntaxe"

    On utilise la méthode `.keys()` pour parcourir la liste des clés contenues dans le dictionnaire.

    ```python
    for clé in dico.keys():
        bloc d'instruction
    ```

!!! note "Simplification"

    On accède directement à un parcours par clé avec la syntaxe suivante :

    ```python
    for clé in dico:
        bloc d'instruction
    ```

### 2. Parcours par valeurs

!!! abstract "Syntaxe"

    On utilise la méthode `.values()` pour parcourir la liste des valeurs contenues dans le dictionnaire.

    ```python
    for valeur in dico.values():
        bloc d'instruction
    ```


### 3. Parcours par éléments

!!! abstract "Syntaxe"

    On utilise la méthode `.items()` pour parcourir la liste des éléments contenue dans le dictionnaire.

    ```python
    for élément in dico.items():
        bloc d'instruction
    ```


!!! note "Unpacking"

    Chaque élément étant un couple (clé, valeur), il est possible de dissocier chaque valeur dans deux itérateurs différents.

    ```python
    for clé, valeur in dico.items():
        bloc d'instruction
    ```

???+ question

    Nous disposons du dictionnaire : `mon_dictionnaire`.  
    Le parcours avec la boucle `for element in mon_dictionnaire` se fait :

    === "Cocher la ou les affirmations correctes"
        
        - [ ] sur les clés du dictionnaire.
        - [ ] sur les valeurs du dictionnaire.
        - [ ] sur les paires clé, valeur du dictionnaire.
        - [ ] sur les indices du dictionnaires

    === "Solution"
        
        - :white_check_mark: sur les clés du dictionnaire.
        - :x: ~~sur les valeurs du dictionnaire.~~
        - :x: ~~sur les paires clé, valeur du dictionnaire.~~
        - :x: ~~sur les indices du dictionnaires~~ Il n'y a pas d'"indices" pour des dictionnaires



???+ question "Gaston fait ses courses"

    Gaston va faire ses courses dans une boutique "prix ronds". Mais comme il est passionné de Python, il a créé un dictionnaire pour représenter ses achats appelé `achats` et un autre pour représenter le prix de chaque article appelé `prix`. 
    Exemple pour l'achat d'un kg de farine,d'une plaquette de beurre, et d'un kg de sucre.  
    Il connait les prix suivants :  

    |produit|1kg de farine|1 kg de sucre|1 l de lait|1 plaquette de beurre|
    |--|--|--|--|--|
    |prix|1|3|2|2|

    Voici par exemple les dictionnaires :

    ```python
    mes_achats = {"farine": 2, "beurre": 1, "sucre": 2}
    mes_prix = {"farine": 1, "sucre": 3, "lait": 2, "beurre": 2}  
    ```
    Aidez-le à compléter la fonction `facture` qui prend en paramètres les deux dictionnaires, et renvoie le montant qu'il devra payer pour ses achats. Les valeurs du dictionnaire `prix` sont des entiers.

    {{ IDE('scripts/achats_1') }}

??? success "Solution"

    ```python
    def facture(achats, prix) -> int:
        a_payer = 0
        for article in achats:
            a_payer = a_payer + achats[article] * prix[article]
        return a_payer
    ```


## Questions Flash


 <iframe 
        src="{{ page.canonical_url }}../activites/flash_dico.html"
        width="900" height="500"
        id="modele"
        title="Vocabulaire" 
        frameborder="0" 
        allow="autoplay; fullscreen; picture-in-picture">
    </iframe>
</div>

