---
author: Pierre Marquestaut, Gilles Lassus
title:  Pages dynamiques
---

!!! abstract "Définition"

    Contrairement à une page statique, la page web *dynamique* envoyée par le serveur est :

    1. différente selon le client.
    2. modifiable après réception sur l'ordinateur du client.

    Ce dynamisme peut se faire côté serveur (avec par exemple le langage PHP) ou coté client (avec Javascript).


## 1. Quand le client peut agir sur sa page : exemple avec JavaScript

![](images/dynclient.png){: .center}

Grâce au langage JavaScript, il est possible de fabriquer une page sur laquelle le client va pouvoir agir **localement**, sans avoir à redemander une nouvelle page au serveur.

Inventé en 1995 par [Brendan Eich](https://fr.wikipedia.org/wiki/Brendan_Eich){:target="_blank"} pour le navigateur Netscape, le langage JavaScript s'est imposé comme la norme auprès de tous les navigateurs pour apporter de l'interactivité aux pages web.

!!! abstract "Programmation évènementielle"
    Le langage Javascript permet de réaliser une programmation, c'est-à-dire d'associer une fonction à un évènement, de sorte que la fonction est appelée lors de la survenue de l'évènement.


!!! question "les évènements"
    <iframe id="vocabulaire"
    title="Vocabulaire sur les langages"
    width="100%"
    height="400"
    src="{{ page.canonical_url }}../activites/evt_js.html">

    </iframe>
!!! example "Exemple de couple ```html``` / ```javascript``` minimal"
    Notre fichier ```index.html``` fait référence, au sein d'une balise ```<script>```, à un fichier externe ```script.js``` qui contiendra notre code JavaScript.   

    - fichier ```index.html``` : 
    ```html
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title>un peu d'action</title>
        <link href="style.css" rel="stylesheet" type="text/css" />
      </head>
      <body>
        <script src="script.js"></script>
        <p>
        <h2>Une page web extrêmement dynamique</h2>
        </p>
        <div>

            <label>Changez la couleur d'arrière-plan:</label>

            <button type="button" onclick="choix('yellow');">jaune</button>

            <button type="button" onclick="choix('green');">vert</button>

            <button type="button" onclick="choix('purple');">violet</button> 

            <input onchange="choix(this.value)" type="color" />
            
        </div>
        <div>
          <p>
          En JavaScript, le nom de la couleur choisie est :
          </p>
          <p id="resultat"></p>
        </div>
      </body>
    </html>
    ```


    - fichier ```script.js``` :
    ```javascript
    function choix(color){
        document.body.style.background = color;
        document.getElementById("resultat").innerHTML=color;
    }
    ```


Le résultat de cette page peut être consulté [ici](http://glassus1.free.fr/page_test3.html){:target="_blank"}.

**Commentaires**

- Au sein du bouton déclaré par la balise ```button```, l'attribut  ```onclick``` reçoit le nom d'une fonction déclarée à l'intérieur du fichier ```script.js```, ici la fonction ```choix()```.
- Cette fonction nous permet de modifier à la fois l'aspect esthétique de la page (changement de la couleur de background) mais aussi le contenu de cette page, en faisant afficher le nom de la couleur.

La puissance du JavaScript permet de réaliser aujourd'hui des interfaces utilisateurs très complexes au sein d'un navigateur, équivalentes à celles produites par des logiciels externes (pensez à la version web de Discord ou aux Google Docs, par ex.). Bien sûr, dans ces cas complexes, le serveur est aussi sollicité pour modifier la page, comme nous le verrons en partie 3.





## 3. Quand la page est fabriquée à la demande pour le client : exemple avec PHP


![](data/dynserveur.png){: .center}

Rappelons que toutes les pages que nous avons créées jusqu'à présent sont uniformément envoyées par le serveur au client. Aucune «préparation» de la page en amont n'a lieu sur le serveur, aucun dialogue n'a lieu avec le serveur une fois que la page a été livrée. 
Évidemment, si le web était comme ceci, il ne serait qu'une gigantesque bibliothèque en consultation seule (ce fut le cas pendant longtemps, et ce qui n'était déjà pas si mal).

Les langages serveurs, parmi lesquels PHP (présent sur environ 80% des serveurs), Python (via le framework Django), Java, Ruby, C#, permettent de rajouter de l'interactivité côté serveur.

**Il convient de rappeler la différence fondamentale entre une page statique (côté serveur) et une page dynamique (côté serveur) :**

### 3.1 Page statique (côté serveur) : 
Lors d'une requête d'un client vers un serveur, si le client demande la page ```index.html```, une **copie exacte** du fichier ```index.html``` est transmise au client sur sa machine.  

**Exemple :** la page [http://glassus1.free.fr/interesting.html](http://glassus1.free.fr/interesting.html){:target="_blank"} que vous avez déjà consultée se trouve **telle quelle** sur le serveur mis à disposition par Free pour l'hébergement des pages personnelles :

![](data/extraitfree1.png){: .center}

Depuis votre navigateur, l'affichage du code-source (par Ctrl-U) vous donnera le fichier html tel qu'il était stocké sur le serveur.

### 3.2  Page dynamique (côté serveur) :
Lors d'une requête d'un client vers un serveur, si le client demande la page ```test.php```, un code html est généré à partir  du fichier ```test.php```  puis est transmise au client sur sa machine. Le fichier transmis ne contient plus de balises ```php```, il ne comporte que des balises ```html``` classiques.  

**Exemple :** la consultation de la page [http://glassus1.free.fr/test.php](http://glassus1.free.fr/test.php){:target="_blank"} va renvoyer la page suivante :

![](images/php1.png){: .center}

dont le code-source est :

![](images/php2.png){: .center}

Notez bien que ce code-source ne contient que du ```html```.

Allons regarder cette page **du côté du serveur** :

![](images/php3.png){: .center}
 
Le contenu de cette page est :

```php
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Quel jour sommes-nous</title>
    </head>
    <body>
	<p>
	<?php
	$date = date("d-m-Y");
	Print("Nous sommes le $date");
	?>
	</p>
</body>
</html>
```

On y repère la balise ```<?php>``` :
```php
	<?php
	$date = date("d-m-Y");
	Print("Nous sommes le $date");
	?>
```
Ce code ```php``` a donc généré, lors de l'appel au serveur, le code ```html``` :

```html
Nous sommes le 13-04-2020
```

Vous pouvez tester du code PHP et la page générée par exemple sur [ce site](https://www.w3schools.com/php/phptryit.asp?filename=tryphp_compiler){. target="_blank"}.

Voilà comment un serveur peut adapter la page qu'il renvoie, suivant l'utilisateur qui la demande. Nous verrons prochainement comment par des requêtes le client peut envoyer des paramètres au serveur, et comment celui-ci modifie sa réponse en conséquence.

**En savoir plus :** [https://www.php.net/manual/fr/tutorial.firstpage.php](https://www.php.net/manual/fr/tutorial.firstpage.php){:target="_blank"}
